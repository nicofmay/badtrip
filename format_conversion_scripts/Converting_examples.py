#!/usr/bin/env python

#Convert fasta to counts format. The (aligned) sequences in the fasta file are read in and the data is written to a counts format file.

import argparse
import os
import logging
import fasta as fa
import cf as cf  # noqa
import numpy.random

fileNames=["/Users/nicolademaio/Desktop/seq-gen_output_balance_1Ne_10sample_500genes_repl1.txt","/Users/nicolademaio/Desktop/seq-gen_output_balance_10Ne_10sample_500genes_repl1.txt","/Users/nicolademaio/Desktop/seq-gen_output_unbalance_1Ne_10sample_500genes_repl1.txt","/Users/nicolademaio/Desktop/seq-gen_output_unbalance_10Ne_10sample_500genes_repl1.txt"]

ind = { "A":0 , "C":1 , "G":2 , "T":3}
sErr=[0.001,0.01]

for i in range(len(fileNames)):
	
	#collect counts
	file=open(fileNames[i])
	counts=[]
	for s in range(8):
		counts.append([])
	line=file.readline()
	for g in range(500):
		while line!="	Matrix\n":
			line=file.readline()
		seqs={}
		for s in range(80):
			line=file.readline()
			num=int(line.split()[0])
			seq=line.split()[1]
			seqs[num-1]=seq
		for p in range(1000):
			for s in range(8):
				count=[0,0,0,0]
				for w in range(10):
					nuc=seqs[s*10+w][p]
					count[ind[nuc]]+=1
				counts[s].append(count)
	file.close()
				
	#seq errors
	for e in range(len(sErr)):
		countsErr=[]
		for s in range(8):
			countsErr.append([])
			for p in range(len(counts[s])):
				c=counts[s][p]
				newC=[0,0,0,0]
				for n in range(4):
					for i2 in range(c[n]):
						ra=numpy.random.random()
						if ra<sErr[e]:
							rN=numpy.random.randint(0,3)
							newC[rN]+=1
						else:
							newC[n]+=1
				countsErr[s].append(newC)
		#seq error to file
		fileO=open(fileNames[i].replace(".txt","_counts_seqErr"+str(sErr[e])+".txt"),"w")
		for s in range(8):
			fileO.write("	<sequence spec=\"SequencePoMo\" id=\"seq"+str(s+1)+"\" taxon=\"t"+str(s+1)+"\" value=\"")
			for p in range(len(countsErr[s])):
				fileO.write(str(countsErr[s][p][0])+"-"+str(countsErr[s][p][1])+"-"+str(countsErr[s][p][2])+"-"+str(countsErr[s][p][3])+",")
			fileO.write("\"/>\n")
		fileO.close()
	
	#no error to file
	fileO=open(fileNames[i].replace(".txt","_counts.txt"),"w")
	for s in range(8):
		fileO.write("	<sequence spec=\"SequencePoMo\" id=\"seq"+str(s+1)+"\" taxon=\"t"+str(s+1)+"\" value=\"")
		for p in range(len(counts[s])):
			fileO.write(str(counts[s][p][0])+"-"+str(counts[s][p][1])+"-"+str(counts[s][p][2])+"-"+str(counts[s][p][3])+",")
		fileO.write("\"/>\n")
	fileO.close()
		
exit()
	
