

\documentclass[10pt,letterpaper]{article}
\usepackage[top=0.85in,left=2.75in,footskip=0.75in]{geometry}
\usepackage{changepage}
\usepackage[utf8]{inputenc}
\usepackage{textcomp,marvosym}
\usepackage{fixltx2e}
\usepackage{amsmath,amssymb}
\usepackage{cite}
\usepackage{nameref,hyperref}
\usepackage[right]{lineno}
\usepackage{listings}
\usepackage{microtype}
\DisableLigatures[f]{encoding = *, family = * }
\usepackage{rotating}
\usepackage{bm}
\usepackage{color}
\raggedright
\setlength{\parindent}{0.5cm}
\textwidth 5.25in 
\textheight 8.75in
\usepackage[aboveskip=1pt,labelfont=bf,labelsep=period,justification=raggedright,singlelinecheck=off]{caption}

\makeatletter
\renewcommand{\@biblabel}[1]{\quad#1.}
\makeatother
\date{}

\begin{document}
\vspace*{0.35in}



{\Huge
\textbf\newline{BADTRIP Manual}
}
\newline

{\Large
\textbf\newline{Nicola De Maio}
}
\newline
{
\textbf\newline{\today}
}
\newline

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}
\lstset{frame=tb,
  language=Java,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3
}






\section{Installing BADTRIP}

BADTRIP is a BEAST2 package. 
To install it, first download and install BEAST2 if you have not already done so (\emph{http://beast2.org/}).
Then, from the BEAST2 distribution folder, run BEAUti (double-click on the BEAUti icon).
In BEAUti, click on "File" from the top bar menu, select "Manage packages" from the scroll-down menu, then select BADTRIP from the table of packages, and click on "Install/Upgrade".
Now BEAST2 will run BADTRIP xml files.
If BADTRIP is not yet available in the "Manage packages" list, you have to click on the "Package repositories" button. A window pops up where you can click "Add URL" and add "https://raw.githubusercontent.com/CompEvol/CBAN/master/packages-extra.xml".
After you have done this, BADTRIP will be available in the "Manage packages" list.

The source code of BADTRIP, examples, and this documentation, can be also found in  \emph{https://bitbucket.org/nicofmay/BADTRIP/}. It is alternatively possible to install BADTRIP from the source code 

If you want to install BADTRIP manually and run it from the command line, first download and unzip from https://bitbucket.org/nicofmay/badtrip/src/master/dist/ the latest version of BADTRIP. Then, if you are using a mac, place/rename this folder in /Library/Application Support/BEAST/2.7/BADTRIP Or if you are using Linux, place/rename it here /.beast/2.7/BADTRIP
Then follow the instructions in https://www.beast2.org/2019/09/26/command-line-tricks.html



\section{Citing BADTRIP}
The BADTRIP manuscript is https://doi.org/10.1371/journal.pcbi.1006117 .
You can also cite the PoMo model used in BADTRIP: \\
De Maio, Nicola, Christian Schloetterer, and Carolin Kosiol. Molecular biology and evolution 30.10 (2013): 2249-2262\\
De Maio, Nicola, Dominik Schrempf, and Carolin Kosiol. Systematic biology 64.6 (2015): 1018-1031\\
Schrempf, Dominik, et al. Journal of theoretical biology 407 (2016): 362-370\\






\section{Creating a working xml file}

A python script is included in this distribution (/scripts/create\_BADTRIP\_xml.py) to create an input xml file for BADTRIP.
This script requires three files in input, one with the epidemiological data, one with the sampling time data, and one with the sequencing data.
Example data files are given in the /scripts/ folder, and to run the script you can simply cd into the /scripts/ folder and use a terminal command like the following: \\
\emph{python create\_BADTRIP\_xml.py -a inputAlignment\_example.txt -e inputEpiData\_example.txt -s inputSamples\_example.txt -o BADTRIP\_xml\_example -E True}\\
The output will be named after the -o option, in this case BADTRIP\_xml\_example.xml .

Instead, to see the script options, simply run\\
\emph{python create\_BADTRIP\_xml.py -h}\\



If you want to use the create\_BADTRIP\_xml.py script as explained above, skip the rest of this manual until the section ``Running BADTRIP'' below.
Otherwise, it is also possible to create a working xml input file for BADTRIP by using the given xml template file (/examples/BADTRIP.xml) and manually adding/modifying parts as explained below. 
In the following, I will assume that the "BADTRIP.xml" example file is used as a template.




\subsection{Including the genetic alignment}

An example genetic alignment section:

\begin{lstlisting}
<alignmentPoMo spec="AlignmentPoMo" id="alignment" dataTypePoMo="PoModata">
	<sequence spec="SequencePoMo" id="sS1" taxon="tS1" value="10-0-0-0, 15-0-0-0, 0-0-8-3"/>
	<sequence spec="SequencePoMo" id="sS2" taxon="tS2" value="20-0-0-0, 0-23-0-0, 0-0-5-0"/>
	<sequence spec="SequencePoMo" id="sS3" taxon="tS3" value="0-0-0-0, 0-2-0-0, 0-0-0-1"/>
	<sequence spec="SequencePoMo" id="sS4" taxon="tS4" value="3-0-0-0, 0-5-0-0, 0-0-5-2"/>
</alignmentPoMo>
\end{lstlisting}

Data for each site and population is in the form "0-1-0-9", that is, 4 dash-separated integers. These four integers represent respectively the allele counts of As, Cs, Gs, and Ts at the given position and population.
It is important that all taxa have the same number of positions. An element "0-0-0-0" is equivalent to providing no data for the position, ideal in cases when a deletion removes part of the genome, for example.




\subsection{Including time and host information}

Information regarding hosts (start, ``HostDatesStartValue'',  and end, ``HostDatesEndValue'', times for their exposure intervals) and samples (host from which samples are taken, ``samplesHostsValue'', and times of sampling, ``samplesDatesValue'') are specified like this:

\begin{lstlisting}
	<TraitSetPoMo spec='TraitSetPoMo' id='traitSet' direction="forward" units="day"
        samplesHostsValue="tS1=HH1, tS2=HH1, tS3=HH2, tS4=HH2"
        samplesDatesValue="tS1=5, tS2=10, tS3=7, tS4=8"
        HostDatesStartValue="HH1=-5.0, HH2=2.0, HH3=10.0"
        HostDatesEndValue="HH1=15.0, HH2=9.0, HH3=18.0">
      <taxa spec='TaxonSet' alignment='@alignment'/>
   </TraitSetPoMo>
\end{lstlisting}
   
The numbers here can represent any time unit (days, years, hours, etc) and all other parameters (e.g., mutation rates) have to be interpreted in function of the time scale used.





\subsection{Defining the substitution model}

The following block defines the substitution model PoMo:

\begin{lstlisting}
    <siteModel spec="SiteModelPoMo" id="siteModel">
     <mutationRate spec='RealParameter' id="mutationRate" value="0.05" lower="0.0"/>
     <substModel spec="PoMoGeneral" virtualPop="15" estimateRootFreqs="false" useTheta="False" theta="0.01" id="PoMo.substModel">
       <fitness spec='RealParameter' id="PoMo.fitness" value="1.0 1.0 1.0 1.0" lower="0.8" upper="1.2"/>
       <rootNucFreqs spec='RealParameter' id="PoMo.rootFreqs" value="0.25 0.25 0.25 0.25"/>
       <mutModel spec='MutationModel' id="PoMo.mutModel">
       		<rateVector spec='RealParameter' id="PoMo.mutRates" value="0.01 0.01" upper="0.3"/>
       		<nucFreqs spec='RealParameter' id="PoMo.nucFreqs" value="0.25 0.25 0.25 0.25"/>
       </mutModel>
     </substModel>
   </siteModel>
\end{lstlisting}

In particular, this line defines many aspects of the model:
\begin{lstlisting}
     <substModel spec="PoMoGeneral" virtualPop="15" estimateRootFreqs="false" useTheta="False" theta="0.01" id="PoMo.substModel">
\end{lstlisting}

For example, "virtualPop" determines how many individuals are in the virtual population (here 15).
"estimateRootFreqs" determines if root nucleotide frequencies should be estimated or if equilibrium should be assumed.
Finally, "useTheta" specifies if a user-specified value of $\theta = 4N_e \mu$ for the root frequency of variant sites should be used, or if it should be estimated.
if "useTheta" is set to "true", then $\theta$ can be set with:

\begin{lstlisting}
     <substModel spec="PoMoGeneral" virtualPop="5" estimateRootFreqs="false" useTheta="true" theta="0.016" id="PoMo.substModel">
\end{lstlisting}

The mutation model has to be one of the following: HKY85, GTR, or general non-reversible. The mutation model is specified in the following line:
\begin{lstlisting}
       		<rateVector spec='RealParameter' id="PoMo.mutRates" value="0.01 0.01" upper="0.3"/>
\end{lstlisting}
Depending on how many values are given after "value", the mutation model is picked. the number of values mean the number of mutation rates in the model, excluding nucleotide frequencies: 1=F81, 2=HKY, 6=GTR, 12=general non-reversible




\subsection{Sequencing error}

Starting value for sequencing error is specified as:
\begin{lstlisting}
     <seqError spec='RealParameter' id="PoMo.seqError" value="0.0001"/>
\end{lstlisting}

A starting value of "0.0" as shown above, means that no sequencing error is assumed.
If the value is different from 0.0, sequencing error is taken into account, and if an operator (see next section) for the sequencing error is specified, the error rate is estimated.




\subsection{Operators}

Operators determine which parameters of the model are modified, and which are held constant.
For example, the following code 
\begin{lstlisting}
     <!--operator spec="ScaleOperator" id="errScaler"
 	      parameter="@PoMo.seqError"
	      scaleFactor="0.9" weight="1"/-->
\end{lstlisting}

is commented out (same as if it was not there).
By including the same operator,
\begin{lstlisting}
     <operator spec="ScaleOperator" id="errScaler"
 	      parameter="@PoMo.seqError"
	      scaleFactor="0.9" weight="1"/>
\end{lstlisting}
 we tell BEAST2 to alter the value, and therefore estimate, the considered parameter, in this case the sequencing error.
 
 Similarly, other operators can be included or removed/commented out, for example:
 
 \begin{lstlisting}
     <operator spec="ScaleOperator" id="fitScaler"
 	      parameter="@PoMo.fitness"
	      scaleFactor="0.8" weight="3"/>
\end{lstlisting}
for nucleotide fitnesses (selection or fixation bias of nucleotides);

 \begin{lstlisting}
     <operator spec="DeltaExchangeOperator" id="freqExchangerRoot"
	       parameter="@PoMo.rootFreqs"
	       delta="0.01" weight="0.5"/>
\end{lstlisting}
for root nucleotide frequencies (only to be used if we are estimating root nucleotide frequencies).

In the case of a nonreversible model, the following operator has to be removed:
 \begin{lstlisting}
     <operator spec="DeltaExchangeOperator" id="freqExchanger"
	       parameter="@PoMo.nucFreqs"
	       delta="0.01" weight="0.5"/>
\end{lstlisting}




\subsection{Specifying location and name of output files}

To specify the file names and location of the output, simply modify the two following lines at the end of the xml file

\begin{lstlisting}
	<logger logEvery="1000" fileName="pathToFile/fileName.log">
\end{lstlisting}

and

\begin{lstlisting}
	 <logger logEvery="10000" fileName="pathToFile/fileName.trees" mode="tree">
\end{lstlisting}

by including the desired location and file name for the output for the log file and the output trees file respectively.






\section{Running BADTRIP}

Just select the BADTRIP xml file you created, after you double-clicked on the BEAST2 icon in the BEAST2 folder.

%Alternatively, you can download the most recent version of the BADTRIP package as a zip file from \emph{ https://bitbucket.org/nicofmay/BADTRIP/src/master/dist/}.
%After installing BEAST2, and after unzipping BADTRIP, and locating the jar file in the lib folder, you can run the following command from terminal:

%\emph{java -cp /path\_to\_BADTRIP/BADTRIP.v0.1.0.jar:/path\_to\_BEAST/BEAST\ 2.4.0/lib/beast.jar beast.app.beastapp.BeastMain /path\_to\_xml\_file/file.xml}

If you want to install BADTRIP manually and run it from the command line, first download and unzip from https://bitbucket.org/nicofmay/badtrip/src/master/dist/ the latest version of BADTRIP. Then, if you are using a mac, place/rename this folder in /Library/Application Support/BEAST/2.7/BADTRIP Or if you are using Linux, place/rename it here /.beast/2.7/BADTRIP
Then follow the instructions in https://www.beast2.org/2019/09/26/command-line-tricks.html

If you get error, trying uninstalling PoMo from the BEAUti package manager.



\subsection{Analysing the output}

The output trees from BADTRIP can be used to get a summary of the inferred transmission events, graph-like plots of hosts and transmissions.
The trees themselves cannot be visualized in FigTree because they are not bifurcating.
To run these analysis, simply use the python script "Make\_transmission\_tree.py" which can be found within the BADTRIP package or at https://bitbucket.org/nicofmay/BADTRIP/src/ $\rightarrow$ BADTRIP.
 For a description of the formats and options, simply run from terminal "python Make\_transmission\_tree.py -h" from the BADTRIP/scripts/ folder.
 To run it, use the command format  "python Make\_transmission\_tree.py -i fileName.trees -o fileNameOutput".
 Sometimes the graphical output needs to be tuned, in which case you can run \emph{python Make\_transmission\_tree.py -h} to see the possible options. 
 Also, this script requires installation of \emph{graph\_tool}, which is a pain to install sometimes.
 If you are having problems with installing graph-tool (a dependency of the above script), you can run instead "Make\_transmission\_tree\_alternative.py", a less fancy, but quicker and easier to use version of the visualization script.
In case of any additional problem you can contact me: \emph{demaio@ebi.ac.uk}



\end{document}

