package badtrip.evolution.sitemodel;

import badtrip.evolution.alignment.AlignmentPoMo;
import badtrip.evolution.datatype.DataTypePoMo;
import badtrip.evolution.likelihood.TreeLikelihoodPoMo;
import badtrip.evolution.substitutionmodel.PoMoGeneral;
import beast.base.core.Description;
import beast.base.core.Input;
import beast.base.core.Input.Validate;
import beast.base.evolution.datatype.DataType;
import beast.base.evolution.sitemodel.SiteModelInterface;
import beast.base.evolution.substitutionmodel.SubstitutionModel;
import beast.base.evolution.tree.Node;
import beast.base.inference.CalculationNode;
import beast.base.inference.StateNode;

import java.util.ArrayList;
import java.util.List;




/**
 * SiteModel - Specifies how rates and substitution models vary across sites. Modified for PoMo
 *
 * @author Nicola De Maio
 */

public interface SiteModelInterfacePoMo extends SiteModelInterface {

    /**
     * set DataType so it can validate the Substitution model can handle it *
     * @param dataType
     */
    void setDataType(DataTypePoMo dataType);


    @Description(value = "Base implementation of a site model with substitution model and rate categories.", isInheritable = false)
    public abstract class Base extends CalculationNode implements SiteModelInterfacePoMo {
    	public Input<PoMoGeneral> substModelInput =
                new Input<PoMoGeneral>("substModel", "substitution model along branches in the beast.tree", null, Validate.REQUIRED);

    	/**
         * Specifies whether SiteModel should integrate over the different categories at
         * each site. If true, the SiteModel will calculate the likelihood of each site
         * for each category. If false it will assume that there is each site can have a
         * different category.
         *
         * @return the boolean
         */
        abstract public boolean integrateAcrossCategories();

        /**
         * @return the number of categories of substitution processes
         */
        abstract public int getCategoryCount();

        /**
         * Get the category of a particular site. If integrateAcrossCategories is true.
         * then throws an IllegalArgumentException.
         *
         * @param site the index of the site
         * @param node
         * @return the index of the category
         */
        abstract public int getCategoryOfSite(int site, Node node);

        /**
         * Get the rate for a particular category. This will include the 'mu' parameter, an overall
         * scaling of the siteModel.
         *
         * @param category the category number
         * @param node
         * @return the rate.
         */
        abstract public double getRateForCategory(int category, Node node);

        /**
         * Get an array of the rates for all categories.
         *
         * @param node
         * @return an array of rates.
         */
        abstract public double[] getCategoryRates(Node node);

        /**
         * Get the expected proportion of sites in this category.
         *
         * @param category the category number
         * @param node
         * @return the proportion.
         */
        abstract public double getProportionForCategory(int category, Node node);

        /**
         * Get an array of the expected proportion of sites for all categories.
         *
         * @param node
         * @return an array of proportions.
         */
        abstract public double[] getCategoryProportions(Node node);
        abstract public double[] getCategoryProportions();
    
        public boolean canSetSubstModel(Object o) {
            final PoMoGeneral substModel = (PoMoGeneral) o;
            if (m_dataType == null) {
            	// try to find out the data type from the data in a treelikelihood in an output
            	for (Object plugin : getOutputs()) {
            		if (plugin instanceof TreeLikelihoodPoMo) {
            			TreeLikelihoodPoMo likelihood = (TreeLikelihoodPoMo) plugin;
            			//m_dataType = (likelihood.dataInput.get()).getDataTypePoMo();
            			m_dataType = ((AlignmentPoMo)likelihood.dataInput.get()).getDataTypePoMo();
            			break;
            		}
            	}
            }
            if (m_dataType != null) {
                if (!substModel.canHandleDataType((DataType)m_dataType)) {
                    return false;
                    //throw new Exception("substitution model cannot handle data type");
                }
            }
            return true;
        }

        DataTypePoMo m_dataType;
        /**
         * Flag indicating proportional invariant is treated as a separate
         * category. If set to false, only gamma-categories are returned and
         * a TreeLikelihood has to deal with the proportional invariant category
         * separately -- and potentially much more efficiently.
         */
        public boolean hasPropInvariantCategory = true;

        public void setPropInvariantIsCategory(final boolean bPropInvariantIsCategory) {
            hasPropInvariantCategory = bPropInvariantIsCategory;
            refresh();
        }

        /**
         * set up categories, reserve appropriately sized memory *
         */
        protected void refresh() {
        }

        /**
         * Get this site model's substitution model
         *
         * @return the substitution model
         */
        public SubstitutionModel getSubstitutionModel() {
            return substModelInput.get();
        }


        /**
         * list of IDs onto which SiteModel is conditioned *
         */
        protected List<String> conditions = null;

        /**
         * return the list, useful for ... *
         * @return
         */
        public List<String> getConditions() {
            return conditions;
        }

        /**
         * add item to the list *
         * @param stateNode
         */
        public void addCondition(final Input<? extends StateNode> stateNode) {
            if (stateNode.get() == null) return;

            if (conditions == null) conditions = new ArrayList<String>();

            conditions.add(stateNode.get().getID());
        }

        @Override
        public void setDataType(final DataTypePoMo dataType) {
            m_dataType = dataType;
        }
        
        @Override
        public void setDataType(final DataType dataType) {
        }

        public double getProportionInvariant() {
            return 0;
        }

    } // class SiteModelInterface.Base

} // SiteModelInterface
