package badtrip.evolution.tree;

import beast.base.core.BEASTObject;
import beast.base.core.Description;
import beast.base.core.Input;
import beast.base.core.Input.Validate;
import beast.base.core.Log;
import beast.base.evolution.alignment.Taxon;
import beast.base.evolution.alignment.TaxonSet;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;


@Description("Like trait set in BEAST2, but with 4 different types: sample times, sample hosts, host earliest infection time, host latest infectivity time.")
public class TraitSetPoMo extends BEASTObject {

    public enum Units {
        year, month, day
    }

    final public Input<String> directionInput = new Input<>("direction", "direction of time in input values of samples and hosts, "+FORWARD+", "+BACKWARD+"'.", Validate.REQUIRED);
    final public Input<Units> unitsInput = new Input<>("units", "name of the units in which values are posed, " +
            "used for conversion to a real value. This can be " + Arrays.toString(Units.values()) + " (default 'day')", Units.day, Units.values());
    final public Input<String> samplesHostsInput = new Input<>("samplesHostsValue", "traits encoded as sample=host pairs separated by commas", Validate.REQUIRED);
    final public Input<String> samplesDatesInput = new Input<>("samplesDatesValue", "traits encoded as sample=date pairs separated by commas", Validate.REQUIRED);
    final public Input<String> HostDatesStartInput = new Input<>("HostDatesStartValue", "traits encoded as host=date pairs separated by commas", Validate.REQUIRED);
    final public Input<String> HostDatesEndInput = new Input<>("HostDatesEndValue", "traits encoded as host=date pairs separated by commas", Validate.REQUIRED);
    final public Input<TaxonSet> taxaInput = new Input<>("taxa", "contains list of taxa to map traits to", Validate.REQUIRED);

    final public Input<String> dateTimeFormatInput = new Input<>("dateFormat", "the date/time format to be parsed, (e.g., 'dd/M/yyyy')");
    
    final public static String FORWARD = "forward";
    final public static String BACKWARD = "backward";
    //final public static String SAMPLES_DATES_TRAIT = "samplesDates";
    //final public static String HOST_DATES_START_TRAIT = "HostDatesStart";
    //final public static String HOST_DATES_END_TRAIT = "HostDatesEnd";

    /**
     * String values of entities*
     */
    protected String[] taxonValues;
    protected String[] taxonValuesH;
    Map<String, String> hostValuesS;
    Map<String, String> hostValuesE;
    
    protected List<String> sampledHosts;
    protected List<String> nonSampledHosts;
    protected List<String> allHosts;
    
    //array of taxa
    Taxon[] taxa;
    
    /**
     * double representation of taxa value *
     */
    //double[] values;
    
    //sampling dates
    double[] samplesDatesValues;
    //String[] samplesHostsValues;
    //protected List<Double> HostDatesStartValues;
    //protected List<Double> HostDatesEndValues;
    double minValue;
    double maxValue;
    
    //map from taxa names to numbers 
    Map<String, Integer> map;
    
    //map from host names to start and end date
    Map<String, Double> mapStart;
    Map<String, Double> mapEnd;
    
    //labels of samples
    List<String> labels;

    /**
     * Whether or not values are ALL numeric.
     */
    //boolean numeric = true;
    
    @Override
    public void initAndValidate() {
        if (samplesHostsInput.get().matches("^\\s*$") || samplesDatesInput.get().matches("^\\s*$") || HostDatesStartInput.get().matches("^\\s*$") || HostDatesEndInput.get().matches("^\\s*$")) {
            return;
        }
        // first, determine taxon numbers associated with traits
        // The Taxon number is the index in the alignment, and
        // used as node number in a tree.
        map = new HashMap<>();
        mapStart = new HashMap<>();
        mapEnd = new HashMap<>();
        labels = taxaInput.get().asStringList();
        //System.out.println("Getting taxa for traitSet. Number of labels is "+labels.size());
        //taxa = new List<Taxon>;
        //System.out.println("Getting taxa for traitSet. Number of elements in taxon set is "+taxaInput.get().taxonList);
        //System.out.println("Getting taxa for traitSet. Number of elements in taxon set is "+taxaInput.get().getTaxonSet().size());
        //Object[] ta=taxaInput.get().getTaxonSet().toArray();
        
        
//        Object[] ta2=taxaInput.get().getTaxaNames().toArray();
//        taxa=new Taxon[ta2.length];
//        for (int i=0;i<ta2.length; i++){
//        	taxa[i]=new Taxon((String) (ta2[i]));
//        }
        taxa=new Taxon[labels.size()];
        for (int i=0;i<labels.size(); i++){
        	taxa[i]=new Taxon((String) (labels.get(i)));
        }
        //System.out.println("Getting taxa for traitSet. Number of taxa is "+taxa.length);
        //taxa = Arrays.copyOf(ta, ta.length, Taxon[].class);
        //taxa =  taxaInput.get().getTaxonSet().toArray(); //(Taxon[])
        String[] samplesHostsTraits = samplesHostsInput.get().split(",");
        String[] samplesDatesTraits = samplesDatesInput.get().split(",");
        String[] HostDatesStartTraits = HostDatesStartInput.get().split(",");
        String[] HostDatesEndTraits = HostDatesEndInput.get().split(",");
        taxonValues = new String[labels.size()];
        taxonValuesH = new String[labels.size()];
        hostValuesS = new HashMap<>();
        hostValuesE = new HashMap<>();
        samplesDatesValues = new double[labels.size()];
        //samplesHostsValues = new String[labels.size()];
        //HostDatesEndValues = new ArrayList<Double>();
        //HostDatesStartValues = new ArrayList<Double>();
        sampledHosts=new ArrayList<String>();
        nonSampledHosts=new ArrayList<String>();
        allHosts=new ArrayList<String>();
        
        //sample dates
        for (String trait : samplesDatesTraits) {
            trait = trait.replaceAll("\\s+", " ");
            String[] strs = trait.split("=");
            if (strs.length != 2) {
                throw new IllegalArgumentException("could not parse trait: " + trait);
            }
            String taxonID = normalize(strs[0]);
            int taxonNr = labels.indexOf(taxonID);
            if (taxonNr < 0) {
                throw new IllegalArgumentException("Trait (" + taxonID + ") is not a known taxon. Spelling error perhaps?");
            }
            taxonValues[taxonNr] = normalize(strs[1]);
            samplesDatesValues[taxonNr] = parseDouble(taxonValues[taxonNr]);
            map.put(taxonID, taxonNr);
            if (Double.isNaN(samplesDatesValues[taxonNr])) throw new IllegalArgumentException("date is not numeric: " + trait);
        }

        // sanity check: did we cover all taxa?
        for (int i = 0; i < labels.size(); i++) {
            if (taxonValues[i] == null) {
                Log.warning.println("WARNING: no date specified for " + labels.get(i));
            }
        }
        
        // find extremes
        minValue = samplesDatesValues[0];
        maxValue = samplesDatesValues[0];
        for (double value : samplesDatesValues) {
            minValue = Math.min(minValue, value);
            maxValue = Math.max(maxValue, value);
        }
        
        //sample hosts
        for (String trait : samplesHostsTraits) {
            trait = trait.replaceAll("\\s+", " ");
            //System.out.println("sample trait "+trait);
            String[] strs = trait.split("=");
            if (strs.length != 2) {
                throw new IllegalArgumentException("could not parse trait: " + trait);
            }
            String taxonID = normalize(strs[0]);
            int taxonNr = labels.indexOf(taxonID);
            if (taxonNr < 0) {
                throw new IllegalArgumentException("Trait (" + taxonID + ") is not a known taxon. Spelling error perhaps?");
            }
            taxonValuesH[taxonNr] = normalize(strs[1]);
            System.out.println("taxon "+taxonID+" associated with "+taxonValuesH[taxonNr]);
            //samplesHostsValues[taxonNr] = taxonValuesH[taxonNr];
            //map.put(taxonID, taxonNr);
            //if (Double.isNaN(samplesDatesValues[taxonNr])) throw new IllegalArgumentException("date is not numeric: " + trait);
        }
            
            //put hosts into List sampledHosts:
            for (String host : taxonValuesH){
            	//System.out.println("sample host "+host);
            	if ((! sampledHosts.contains(host))&&(host!=null)){
            		sampledHosts.add(host);
            		allHosts.add(host);
            	}
            }
        
        for (int i = 0; i < labels.size(); i++) {
            if (taxonValuesH[i] == null) {
                Log.warning.println("WARNING: no host specified for " + labels.get(i));
            }
        }
        
        for (String host : sampledHosts){
        	if (directionInput.get().equals(FORWARD)){
        		mapStart.put(host, Double.MIN_VALUE);
        		mapEnd.put(host, Double.MAX_VALUE);
        		hostValuesS.put(host, Double.toString(Double.MIN_VALUE));
        		hostValuesE.put(host, Double.toString(Double.MAX_VALUE));
        	} else if (directionInput.get().equals(BACKWARD)){
        		mapStart.put(host, Double.MAX_VALUE);
        		mapEnd.put(host, Double.MIN_VALUE);
        		hostValuesS.put(host, Double.toString(Double.MAX_VALUE));
        		hostValuesE.put(host, Double.toString(Double.MIN_VALUE));
        	}
        }
        
        //start dates
        for (String trait : HostDatesStartTraits) {
            trait = trait.replaceAll("\\s+", " ");
            String[] strs = trait.split("=");
            if (strs.length != 2) {
                throw new IllegalArgumentException("could not parse trait: " + trait);
            }
            String host = normalize(strs[0]);
            //int taxonNr = labels.indexOf(taxonID);
            if (!allHosts.contains(host)) {
            	System.out.println("Adding a non-sampled host with epi data: "+host);
                nonSampledHosts.add(host);
                allHosts.add(host);
                if (directionInput.get().equals(FORWARD)) {
                	mapEnd.put(host,Double.MAX_VALUE);
                	hostValuesE.put(host, Double.toString(Double.MAX_VALUE));
                }else if (directionInput.get().equals(BACKWARD)){
                	mapEnd.put(host,Double.MIN_VALUE);
                	hostValuesE.put(host, Double.toString(Double.MIN_VALUE));
                }
            } else {
            	if (mapStart.get(host)!=Double.MAX_VALUE && mapStart.get(host)!=Double.MIN_VALUE)
            		System.out.println("Multiple entries for host "+host+" start : "+trait);
            }
            mapStart.put(host, parseDouble(normalize(strs[1])));
        	hostValuesS.put(host, normalize(strs[1]));
            minValue = Math.min(minValue, parseDouble(normalize(strs[1])));
            maxValue = Math.max(maxValue, parseDouble(normalize(strs[1])));
        }
        
        //end dates
        for (String trait : HostDatesEndTraits) {
            trait = trait.replaceAll("\\s+", " ");
            String[] strs = trait.split("=");
            if (strs.length != 2) {
                throw new IllegalArgumentException("could not parse trait: " + trait);
            }
            String host = normalize(strs[0]);
            if (!allHosts.contains(host)) {
            	System.out.println("Adding a non-sampled host with epi data: "+host);
                nonSampledHosts.add(host);
                allHosts.add(host);
                if (!hostValuesS.containsKey(host)){
	                if (directionInput.get().equals(FORWARD)){
	                	mapStart.put(host,Double.MIN_VALUE);
	                	hostValuesS.put(host, Double.toString(Double.MIN_VALUE));
	                }else if (directionInput.get().equals(BACKWARD)){
	                	mapStart.put(host,Double.MAX_VALUE);
	                	hostValuesS.put(host, Double.toString(Double.MAX_VALUE));
	                }
                }
            } else {
            	if (mapEnd.get(host)!=Double.MAX_VALUE && mapEnd.get(host)!=Double.MIN_VALUE)
            		System.out.println("Multiple entries for host "+host+" end : "+trait);
            }
            mapEnd.put(host, parseDouble(normalize(strs[1])));
            hostValuesE.put(host, normalize(strs[1]));
            minValue = Math.min(minValue, parseDouble(normalize(strs[1])));
            maxValue = Math.max(maxValue, parseDouble(normalize(strs[1])));
        }
        
        //shift everything according to min value and max value
        if (directionInput.get().equals(FORWARD)){
        	for (int i = 0; i < samplesDatesValues.length ; i++) {
        		samplesDatesValues[i] = maxValue - samplesDatesValues[i];
            }
        	for (String host : mapStart.keySet()) {
        		mapStart.put(host, maxValue -mapStart.get(host));
        		mapEnd.put(host, maxValue -mapEnd.get(host));
            }
        }else if (directionInput.get().equals(BACKWARD)){
        	for (int i = 0; i < samplesDatesValues.length ; i++) {
        		samplesDatesValues[i] = samplesDatesValues[i] - minValue;
            }
        	for (String host : mapStart.keySet()) {
        		mapStart.put(host, mapStart.get(host) - minValue);
        		mapEnd.put(host, mapEnd.get(host) - minValue);
            }
        }
        
        for (int i = 0; i < labels.size(); i++) {
            Log.info.println(labels.get(i) + " = " + taxonValues[i] + " (" + (samplesDatesValues[i]) + ")");
        }
        for (String host : hostValuesE.keySet()) {
            Log.info.println(host + " = [" + hostValuesS.get(host) + " , " + hostValuesE.get(host) + "]    ( [" + (mapStart.get(host)) + " , " + mapEnd.get(host) + "] )");
        }
    } // initAndValidate

    /**
     * some getters and setters *
     */
//    public String getTraitName() {
//        return traitNameInput.get();
//    }
    
    public String getDirection() {
        return directionInput.get();
    }

//    @Deprecated // use getStringValue by name instead
//    public String getStringValue(int taxonNr) {
//        return taxonValues[taxonNr];
//    }
//
//    @Deprecated // use getValue by name instead
//    public double getValue(int taxonNr) {
//        if (values == null) {
//            return 0;
//        }
//        return values[taxonNr];
//    }

    public String getStringValue(String taxonName) {
        if (taxonValues == null || map == null || map.get(taxonName) == null) {
            return null;
        }
        return taxonValues[map.get(taxonName)];
    }

    @SuppressWarnings("null")
	public double getValue(String taxonName) {
        if (samplesDatesValues == null || map == null || map.get(taxonName) == null) {
                return ((Double) null);
        }
        return samplesDatesValues[map.get(taxonName)];
    }
    
    public String getHostValue(String taxonName) {
        if (taxonValuesH == null || map == null || map.get(taxonName) == null) {
            return null;
        }
        return taxonValuesH[map.get(taxonName)];

    }

    @SuppressWarnings("null")
	public double getStartValue(String host) {
        if (mapStart == null || mapStart.get(host) == null) {
                return (Double) null;
        }
        return mapStart.get(host);
    }
    
    public String getStartValueString(String host) {
        if (hostValuesS == null || hostValuesS.get(host) == null) {
                return null;
        }
        return hostValuesS.get(host);
    }
    
    @SuppressWarnings("null")
	public double getEndValue(String host) {
        if (mapEnd == null || mapEnd.get(host) == null) {
                return (Double) null;
        }
        return mapEnd.get(host);
    }
    
    public String getEndValueString(String host) {
        if (hostValuesE == null || hostValuesE.get(host) == null) {
                return null;
        }
        return hostValuesE.get(host);
    }

    /**
     * see if we can convert the string to a double value *
     */
    private double parseDouble(String str) {
        // default, try to interpret the string as a number
        try {
            return Double.parseDouble(str);
        } catch (NumberFormatException e) {
            // does not look like a number

                        try {
                            double year;
                            if (dateTimeFormatInput.get() == null) {
                                if (str.matches(".*[a-zA-Z].*")) {
                                        str = str.replace('/', '-');
                                }
                                // following is deprecated, but the best thing around at the moment
                                // see also comments in TipDatesInputEditor
                                long date = Date.parse(str);
                                year = 1970.0 + date / (60.0 * 60 * 24 * 365 * 1000);
                                Log.warning.println("No date/time format provided, using default parsing: '" + str + "' parsed as '" + year + "'");
                            } else {
                                DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateTimeFormatInput.get());
                                LocalDate date = LocalDate.parse(str, formatter);

                                Log.warning.println("Using format '" + dateTimeFormatInput.get() + "' to parse '" + str +
                                        "' as: " + (date.getYear() + (date.getDayOfYear()-1.0) / (date.isLeapYear() ? 366.0 : 365.0)));

                                year = date.getYear() + (date.getDayOfYear()-1.0) / (date.isLeapYear() ? 366.0 : 365.0);
                            }

                            switch (unitsInput.get()) {
                                case month:
                                    return year * 12.0;
                                case day:
                                    return year * 365;
                                default:
                                    return year;
                            }
                        } catch (DateTimeParseException e2) {
                            Log.err.println("Failed to parse date '" + str + "' using format '" + dateTimeFormatInput.get() + "'");
                            System.exit(1);
                        }
                    }
        //return 0;
        return Double.NaN;
    } // parseStrings

    /**
     * remove start and end spaces
     */
    String normalize(String str) {
        if (str.charAt(0) == ' ') {
            str = str.substring(1);
        }
        if (str.endsWith(" ")) {
            str = str.substring(0, str.length() - 1);
        }
        return str;
    }

    public double getDate(double height) {
        if (directionInput.get().equals(FORWARD)) {
            return maxValue - height;
        }

        if (directionInput.get().equals(BACKWARD)) {
            return minValue + height;
        }
        return height;
    }
    
    /**
     * Determines whether trait is recognised as specifying taxa dates.
     * @return true if this is a date trait.
     */
//    public boolean isDateTrait() {
//        return traitNameInput.get().equals(DATE_TRAIT)
//                || traitNameInput.get().equals(DATE_FORWARD_TRAIT)
//                || traitNameInput.get().equals(DATE_BACKWARD_TRAIT);
//    }

    /**
     * @return true if trait values are (all) numeric.
     */
//    public boolean isNumeric() {
//        return numeric;
//    }
} // class TraitSet
