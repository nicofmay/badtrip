package badtrip.evolution.tree;

import badtrip.evolution.datatype.Sample;
import beast.base.core.Description;
import beast.base.core.Input;
import beast.base.evolution.tree.Tree;
import beast.base.inference.Operator;
import beast.base.inference.StateNode;
import beast.base.inference.StateNodeInitialiser;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;
//import beast.base.evolution.tree.TreeParser;


@Description("TreePoMo, representing the transmission tree for PoMo, enriched with samples and transmission events.")
public class TreePoMo extends StateNode {
    final public Input<TreePoMo> m_initial = new Input<>("initial", "treePoMo to start with");
    final public Input<TraitSetPoMo> InputTraits = new Input<>("traits", "all trait information (samples, epi)", new TraitSetPoMo());

    /**
     * state of dirtiness of a node in the tree
     * DIRTY means a property on the node has changed, but not the topology. "property" includes the node height
     *       and that branch length to its parent.
     * FILTHY means the nodes' parent or child has changed.
     */
    public static final int IS_CLEAN = 0, IS_DIRTY = 1, IS_FILTHY = 2;

    /**
     * counters of number of nodes, nodeCount = internalNodeCount + leafNodeCount *
     */
    protected int nodeCount = -1;
    protected int internalNodeCount = -1;
    protected int leafNodeCount = -1;

    protected NodePoMo root;
    protected NodePoMo storedRoot;
    protected NodePoMo[] m_nodes = null;
    protected NodePoMo[] m_storedNodes = null;

    /**
     * Trait set which specifies all information.
     */
    protected TraitSetPoMo traitSet = null;
    protected boolean traitsProcessed = false;
    protected List<String> hosts;
    protected List<NodePoMo> extraList;
    //protected NodePoMo[] extraHosts;
    //protected NodePoMo[] storedExtraHosts;
    protected List<NodePoMo> storedExtraList;
    String[] taxa;
    
//    public Map<String, Double> startValues;
//    public Map<String, Double> endValues;
//    
//    
//    protected String[] taxonValues;
//    protected String[] sampleH;
//    protected List<String> sampledHosts;
//    protected List<String> nonSampledHosts;
//    protected List<String> allHosts;
//    double[] samplesDatesValues;
//    double minValue;
//    double maxValue;
//    Map<String, Integer> map;
//    
//    protected void processTraits(TraitSetPoMo traits) {
//    	taxonValues=traits.taxonValues;
//    	sampleH=traits.taxonValuesH;
//    	startValues.putAll(traits.mapStart);
//    	endValues.putAll(traits.mapEnd);
//    	map=traits.map;
//    }
    
    
    

    @Override
    public void initAndValidate() {
        if (m_initial.get() != null && !(this instanceof StateNodeInitialiser)) {
        	throw new RuntimeException("initial-input should be specified for tree that is not a StateNodeInitialiser");
        }
        
        traitSet=InputTraits.get();
        hosts=traitSet.allHosts;
        taxa = new String[traitSet.labels.size()];
        traitSet.labels.toArray(taxa);
        //processTraits(InputTraits.get());
        m_nodes=new NodePoMo[hosts.size()];
        m_storedNodes=new NodePoMo[hosts.size()];
        extraList=new ArrayList<NodePoMo>();
        storedExtraList=new ArrayList<NodePoMo>();
        //extraHosts=new ArrayList<NodePoMo>();
        //storedExtraHosts=new ArrayList<NodePoMo>();
        
        makeInitialTree(false);
        
        initArrays();
        
        System.out.println(root.height);
        
        //adjustTreeNodeHeights(root);
        
//        for (int i = 0; i < getNodeCount() && i < taxa.length; i++) {
//        	if (taxa[i] != null)
//        		m_nodes[i].setID(taxa[i]);
//        }
    }

    
    
    
    //NICOLA: create an initial meaningful TreePoMo ()
    public void makeInitialTree(final boolean finalize) {
    	//System.out.println("Creating tree");
        // make a caterpillar
    	//NodePoMo root=null;
    	double highest=Double.MIN_VALUE;
    	double lowest=Double.MAX_VALUE;
    	Integer[] connected=new Integer[hosts.size()];
    	int rootNr=-1;
    	for (int i = 0; i < hosts.size(); i++) {
    		connected[i]=0;
    		NodePoMo newN=new NodePoMo(hosts.get(i),hosts.get(i));
    		newN.setEnd(traitSet.mapEnd.get(hosts.get(i)));
    		newN.setStart(traitSet.mapStart.get(hosts.get(i)));
    		//for (int l = 0; l < traitSet.taxa.length; l++) {
    		//System.out.println("traitSet.taxa has "+traitSet.taxa.length+" elements");
    		//System.out.println("taxa has "+taxa.length+" elements");
    		for (int l = 0; l < traitSet.taxa.length; l++) {
    			//System.out.println("Creating tree, assigning samples");
    			//System.out.println(l+" "+traitSet.taxa[l].getID()+" "+hosts.get(i)+" "+traitSet.getHostValue(taxa[l]));
    			//System.out.println(hosts.get(i)+"\n");
    			//System.out.println(traitSet.getHostValue(taxa[l])+"\n");
//    			if (traitSet.getHostValue(traitSet.taxa[l].getID())==hosts.get(i)){
//    				System.out.println("Adding taxon "+traitSet.taxa[l].getID()+" to host "+hosts.get(i));
//    				Sample s = new Sample(traitSet.taxa[l], traitSet.getValue(traitSet.labels.get(l)));
//    				newN.addSample(s);
//    				System.out.println("New number of samples "+newN.getSamples().size()+" for host at height "+newN.getHeight());
//    			}
    			if (traitSet.getHostValue(taxa[l]).equals(hosts.get(i))){
    				System.out.println("Adding taxon "+taxa[l]+" to host "+hosts.get(i));
    				//Sample s = new Sample(traitSet.taxa[l], traitSet.getValue(traitSet.labels.get(l)));
    				Sample s = new Sample(traitSet.taxa[traitSet.map.get(taxa[l])], traitSet.getValue(taxa[l]));
    				newN.addSample(s);
    				//System.out.println("New number of samples "+newN.getSamples().size()+" for host at height "+newN.getHeight());
    			}
    		}
    		newN.sortSamples();
    		newN.isExtra=false;
    		m_nodes[i]=newN;
    		newN.setNr(i);
    		if (newN.getStart()>highest){
    			highest=newN.getStart();
    			root=newN;
    			rootNr=i;
    		}
    		if (newN.getEnd()<lowest){
    			lowest=newN.getEnd();
    		}
    	}
    	root.setParent(null);
    	root.setHeight(root.getStart()-10.0*EPSILON);
    	connected[rootNr]=1;
    	int numUnconnected=hosts.size()-1;
    	NodePoMo currentNode=root;
    	NodePoMo nextNode=null;
    	NodePoMo nextUnconnected=null;
    	double heighestNext=Double.MIN_VALUE;
    	while (numUnconnected>0){
    		for (int i = 0; i < hosts.size(); i++) {
    			if (connected[i]==0){
    				if (m_nodes[i].getStart()>currentNode.getEnd()){
    					numUnconnected-=1;
    					connected[i]=1;
    					m_nodes[i].setParent(currentNode);
    					currentNode.addChild(m_nodes[i]);
    					m_nodes[i].setHeight(m_nodes[i].getStart()-10.0*EPSILON);
    					if ((nextNode==null || m_nodes[i].getEnd()<nextNode.getEnd() ) && (m_nodes[i].getEnd()<currentNode.getEnd())){
    						nextNode=m_nodes[i];
    					}
    				}else{
    					if (m_nodes[i].getStart()>heighestNext){
    						heighestNext=m_nodes[i].getStart();
    						nextUnconnected=m_nodes[i];
    					}
    				}
    			}
    		}
    		if(numUnconnected>0 && nextNode==null && nextUnconnected!=null){
    			NodePoMo newN=new NodePoMo("unsampled"+(extraList.size()+1),"unsampled"+(extraList.size()+1));
    			newN.setEnd((nextUnconnected.getStart()-nextUnconnected.getEnd())/2.0);
        		newN.setStart((currentNode.getStart()-currentNode.getEnd())/2.0);
        		newN.setParent(currentNode);
        		newN.setHeight(newN.getStart()-10.0*EPSILON);
        		extraList.add(newN);
        		newN.setNr(extraList.indexOf(newN));
        		newN.isExtra=true;
        		currentNode.addChild(newN);
        		nextNode=newN;
    		}
    		currentNode.sortChildren();
    		nextUnconnected=null;
    		heighestNext=Double.MIN_VALUE;
    		currentNode=nextNode;
    		nextNode=null;
    	}
    	leafNodeCount=0;
    	for (int i = 1; i < hosts.size(); i++) {
    		if (m_nodes[i].children.size()==0){
    			leafNodeCount+=1;
    		}
    	}
        //leafNodeCount = taxa.size();
    	nodeCount=hosts.size()+extraList.size();
        //nodeCount = leafNodeCount * 2 - 1;
        //internalNodeCount = leafNodeCount - 1;
    	//extraHosts=new NodePoMo[extraList.size()];
    	//extraHosts=extraList.toArray(extraHosts);
    	//storedExtraHosts=new NodePoMo[extraList.size()];
    	//storedExtraHosts=extraList.toArray(storedExtraHosts);
    	//storedExtraList;
        internalNodeCount = nodeCount - leafNodeCount;

        if (finalize) {
            initArrays();
        }
        //System.out.println("Tree created");
    }

//    /**
//     * Process trait sets.
//     *
//     * @param traitList List of trait sets.
//     */
//    protected void processTraits(List<TraitSet> traitList) {
//        for (TraitSet traitSet : traitList) {
//            for (Node node : getExternalNodes()) {
//            	String id = node.getID();
//            	if (id != null) {
//                    node.setMetaData(traitSet.getTraitName(), traitSet.getValue(id));
//            	}
//            }
//            if (traitSet.isDateTrait())
//                timeTraitSet = traitSet;
//        }
//        traitsProcessed = true;
//    }


    protected void initArrays() {
        // initialise tree-as-array representation + its stored variant
        //m_nodes = new Node[nodeCount];
        listNodes(root, m_nodes, extraList);
        m_storedNodes = new NodePoMo[hosts.size()];
        //storedExtraHosts=new NodePoMo[extraHosts.length];
        storedExtraList=new ArrayList<NodePoMo>();
        for (int i =0;i<extraList.size();i++){
        	storedExtraList.add(null);
        }
        final NodePoMo copy = root.copy(storedExtraList);
//        for(int i=0;i<extraList.size();i++){
//        	storedExtraList.add(storedExt[i]);
//        }
        listNodes(copy, m_storedNodes, storedExtraList);
    }


    public TreePoMo() {
    }

    public TreePoMo(final NodePoMo rootNode) {
        setRoot(rootNode);
        initArrays();
    }

    /**
     * Construct a tree from newick string -- will not automatically adjust tips to zero.
     */
    public TreePoMo(final String newick) {
    	System.out.println("Method to read PoMo newick not yet implemented!");
        //this(new TreeParser(newick).getRoot());
    }

    /**
     * Ensure no negative branch lengths exist in tree.  This can occur if
     * leaf heights given as a trait are incompatible with the existing tree.
     */
    final static double EPSILON = 0.0000001;

    protected void adjustTreeNodeHeights(final NodePoMo node) {
        if (!node.isLeaf()) {
            for (final NodePoMo child : node.getChildren()) {
                adjustTreeNodeHeights(child);
            }
            for (final NodePoMo child : node.getChildren()) {
                final double minHeight = child.getHeight() + EPSILON;
                if (node.height < minHeight) {
                    node.height = minHeight;
                }
            }
            double minHeight = node.parent.getEnd() + EPSILON;
            if (node.height < minHeight) {
                node.height = minHeight;
            }
            minHeight = node.getEnd() + EPSILON;
            if (node.height < minHeight) {
                node.height = minHeight;
            }
            double maxHeight = node.parent.getHeight() - EPSILON;
            if (node.height > maxHeight) {
                node.height = maxHeight;
            }
            maxHeight = node.getStart() - EPSILON;
            if (node.height > maxHeight) {
                node.height = maxHeight;
            }
        }
    }


    /**
     * getters and setters
     *
     * @return the number of nodes in the beast.tree
     */
    //@Override
	public int getNodeCount() {
//        if (nodeCount < 0) {
//            nodeCount = this.root.getNodeCount();
//        }
        //System.out.println("nodeCount=" + nodeCount);
        return hosts.size() + extraList.size();
    }

    //@Override
	public int getInternalNodeCount() {
//        if (internalNodeCount < 0) {
//            internalNodeCount = root.getInternalNodeCount();
//        }
    	internalNodeCount = root.getInternalNodeCount();
    	//System.out.println("InternalNodeCount=" + internalNodeCount);
        return internalNodeCount;
    }

    //@Override
	public int getLeafNodeCount() {
//        if (leafNodeCount < 0) {
//            leafNodeCount = root.getLeafNodeCount();
//        }
        leafNodeCount = root.getLeafNodeCount();
        //System.out.println("LeafNodeCount=" + leafNodeCount+" root: "+root.getID());
        //System.exit(0);
        return leafNodeCount;
    }

    /**
     * @return a list of external (leaf) nodes contained in this tree
     */
    //@Override
	public List<NodePoMo> getExternalNodes() {
        final ArrayList<NodePoMo> externalNodes = new ArrayList<>();
        for (int i = 0; i < getNodeCount(); i++) {
            final NodePoMo node = getNode(i);
            if (node.isLeaf()) externalNodes.add(node);
        }
        return externalNodes;
    }

    /**
     * @return a list of internal (ancestral) nodes contained in this tree, including the root node
     */
    //@Override
	public List<NodePoMo> getInternalNodes() {
        final ArrayList<NodePoMo> internalNodes = new ArrayList<>();
        for (int i = 0; i < getNodeCount(); i++) {
            final NodePoMo node = getNode(i);
            if (!node.isLeaf()) internalNodes.add(node);
        }
        return internalNodes;
    }


	public NodePoMo getRootPoMo() {
        return root;
    }

    public void setRoot(final NodePoMo root) {
        this.root = root;
        nodeCount = this.root.getNodeCount();
        // ensure root is the last node
//        if (m_nodes != null && root.labelNr != m_nodes.length - 1) {
//            final int rootPos = m_nodes.length - 1;
//            Node tmp = m_nodes[rootPos];
//            m_nodes[rootPos] = root;
//            m_nodes[root.labelNr] = tmp;
//            tmp.labelNr = root.labelNr;
//            m_nodes[rootPos].labelNr = rootPos;
//        }
    }

    /**
     * Sets root without recalculating nodeCount or ensuring that root is the last node in the internal array.
     * Currently only used by sampled ancestor tree operators. Use carefully!
     *
     * @param root the new root node
     */
    public void setRootOnly(final NodePoMo root) {
        this.root = root;
    }

    //@Override
	public NodePoMo getNode(final int nodeNr) {
        return m_nodes[nodeNr];
        //return getNode(nodeNr, root);
    }

    /**
     * @returns an array of taxon names in order of their node numbers.
     * Note that in general no special order of taxa is assumed, just the order
     * assumed in this tree. Consider using tree.m_taxonset.get().asStringList()
     * instead.
     */
    public String [] getTaxaNames() {
//    	if (taxa == null || (taxa.length == 1 && taxa[0] == null) || taxa.length == 0) {
//         //if (m_sTaxaNames == null || (m_sTaxaNames.length == 1 && m_sTaxaNames[0] == null) || m_sTaxaNames.length == 0) {
//            final TaxonSet taxonSet = m_taxonset.get();
//            if (taxonSet != null) {
//                final List<String> txs = taxonSet.asStringList();
//                m_sTaxaNames = txs.toArray(new String[txs.size()]);
//            } else {
//                m_sTaxaNames = new String[getNodeCount()];
//                collectTaxaNames(getRoot());
//                List<String> taxaNames = new ArrayList<>();
//                for (String name : m_sTaxaNames) {
//                	if (name != null) {
//                		taxaNames.add(name);
//                	}
//                }
//                m_sTaxaNames = taxaNames.toArray(new String[]{});
//                
//            }
//        }
//
//        // sanity check
//        if (m_sTaxaNames.length == 1 && m_sTaxaNames[0] == null) {
//            Log.warning.println("WARNING: tree interrogated for taxa, but the tree was not initialised properly. To fix this, specify the taxonset input");
//        }
        return taxa;
    }

//    void collectTaxaNames(final NodePoMo node) {
//        if (node.getID() != null) {
//            m_sTaxaNames[node.getNr()] = node.getID();
//        }
//        if (node.isLeaf()) {
//            if (node.getID() == null) {
//            	node.setID("node" + node.getNr());
//            }
//        } else {
//        	for (Node child : node.getChildren()) {
//        		collectTaxaNames(child);
//        	}
//        }
//    }


    /**
     * copy meta data matching pattern to double array
     *
     * @param node     the node
     * @param t       the double array to be filled with meta data
     * @param pattern the name of the meta data
     */
    //@Override
	public void getMetaData(final NodePoMo node, final Double[] t, final String pattern) {
        t[Math.abs(node.getNr())] = (Double) node.getMetaData(pattern);
        for (int i=0; i<node.children.size();i++){
        	getMetaData(node.getChild(i), t, pattern);
        }
    }

    /**
     * copy meta data matching pattern to double array
     *
     * @param node     the node
     * @param t       the integer array to be filled with meta data
     * @param pattern the name of the meta data
     */
    public void getMetaData(final NodePoMo node, final Integer[] t, final String pattern) {
        t[Math.abs(node.getNr())] = (Integer) node.getMetaData(pattern);
        for (int i=0; i<node.children.size();i++){
        	getMetaData(node.getChild(i), t, pattern);
        }
    }


    /**
     * traverse tree and assign meta-data values in t to nodes in the
     * tree to the meta-data field represented by the given pattern.
     * This only has an effect when setMetadata() in a subclass
     * of Node know how to process such value.
     */
    //@Override
	public void setMetaData(final NodePoMo node, final Double[] t, final String pattern) {
        node.setMetaData(pattern, t[Math.abs(node.getNr())]);
        for (int i=0; i<node.children.size();i++){
        	setMetaData(node.getChild(i), t, pattern);
        }
    }


    /**
     * convert tree to array representation *
     */
    void listNodes(final NodePoMo node, final NodePoMo[] nodes, final List<NodePoMo> extraList) {
    	if (!node.isExtra){
    		nodes[node.getNr()] = node;
    	}else{
    		extraList.set(node.getNr(), node);
    	}
        node.m_tree = this;  //(JH) I don't understand this code
        // (JH) why not  node.children, we don't keep it around??
        for (final NodePoMo child : node.getChildren()) {
            listNodes(child, nodes, extraList);
        }
    }

//    private int
//    getNodesPostOrder(final Node node, final Node[] nodes, int pos) {
//        node.m_tree = this;
//        for (final Node child : node.children) {
//            pos = getNodesPostOrder(child, nodes, pos);
//        }
//        nodes[pos] = node;
//        return pos + 1;
//    }

//    /**
//     * @param node  top of tree/sub tree (null defaults to whole tree)
//     * @param nodes array to fill (null will result in creating a new one)
//     * @return tree nodes in post-order, children before parents
//     */
//    public Node[] listNodesPostOrder(Node node, Node[] nodes) {
//        if (node == null) {
//            node = root;
//        }
//        if (nodes == null) {
//            final int n = (node == root) ? nodeCount : node.getNodeCount();
//            nodes = new Node[n];
//        }
//        getNodesPostOrder(node, nodes, 0);
//        return nodes;
//    }

//    protected NodePoMo[] postCache = null;
//    //@Override
//	public NodePoMo[] listNodesPostOrder(NodePoMo node, NodePoMo[] nodes) {
//        if( node != null ) {
//            return TreeInterface.super.listNodesPostOrder(node, nodes);
//        }
//        if( postCache == null ) {
//            postCache = TreeInterface.super.listNodesPostOrder(node, nodes);
//        }
//        return postCache;
//    }

    /**
     * @return list of nodes in array format.
     *         *
     */
    //@Override
	public NodePoMo[] getNodesAsArray() {
        return m_nodes;
    }
	
//	public NodePoMo[] getExtraNodes() {
//        return extraHosts;
//    }
	
	public List<NodePoMo> getExtraList() {
        return extraList;
    }

    /**
     * deep copy, returns a completely new tree
     *
     * @return a deep copy of this beast.tree.
     */
    @Override
    public TreePoMo copy() {
        TreePoMo tree = new TreePoMo();
        tree.setID(getID());
        tree.index = index;
        tree.extraList = new ArrayList<NodePoMo>();
        //tree.extraHosts = new NodePoMo[extraList.size()];
        tree.root = root.copy(tree.extraList);
//        for(int i=0;i<tree.extraHosts.length;i++){
//        	tree.extraList.add(tree.extraHosts[i]);
//        }
        tree.nodeCount = nodeCount;
        tree.internalNodeCount = internalNodeCount;
        tree.leafNodeCount = leafNodeCount;
        tree.taxa=taxa.clone();
        tree.hosts= new ArrayList<String>();
        for (String s : hosts){
        	tree.hosts.add(s);
        }
        //protected TraitSetPoMo traitSet = null;
        //protected boolean traitsProcessed = false;
        return tree;
    }

    /**
     * copy of all values into existing tree *
     */
    @Override
    public void assignTo(final StateNode other) {
        final TreePoMo tree = (TreePoMo) other;
        final NodePoMo[] nodes = new NodePoMo[m_nodes.length];
        //final NodePoMo[] extraNodes = new NodePoMo[extraList.size()];
        final List<NodePoMo> extraList2 = new ArrayList<NodePoMo>();
        for(int i=0;i<this.extraList.size();i++){
        	extraList2.add(null);
        }
        listNodes(tree.root, nodes, extraList2);
        tree.setID(getID());
        //tree.index = index;
        root.assignTo(nodes, extraList2);
        if (!root.isExtra){
        	tree.root = nodes[root.getNr()];
        }else{
        	tree.root = extraList2.get(root.getNr());
        }
        //tree.root = nodes[root.getNr()];
        tree.nodeCount = nodeCount;
        tree.internalNodeCount = internalNodeCount;
        tree.leafNodeCount = leafNodeCount;
    }
    

    /**
     * copy of all values from existing tree *
     */
    @Override
    public void assignFrom(final StateNode other) {
        final TreePoMo tree = (TreePoMo) other;
        final NodePoMo[] nodes = new NodePoMo[tree.m_nodes.length];//tree.getNodesAsArray();
        final List<NodePoMo> extraList2 = new ArrayList<NodePoMo>();
        for (int i = 0; i < tree.m_nodes.length; i++) {
            nodes[i] = new NodePoMo();
        }
        for (int i = 0; i < tree.extraList.size(); i++) {
        	extraList2.add(new NodePoMo());
        }
        setID(tree.getID());
        //index = tree.index;
        if(tree.root.isExtra){
        	root = extraList2.get(tree.root.getNr());
        }else{
        	root = nodes[tree.root.getNr()];
        }
        //root = nodes[tree.root.getNr()];
        root.assignFrom(nodes,extraList2,  tree.root);
        root.parent = null;
        root.isExtra=tree.root.isExtra;
        nodeCount = tree.nodeCount;
        internalNodeCount = tree.internalNodeCount;
        leafNodeCount = tree.leafNodeCount;
        initArrays();
    }

    /**
     * as assignFrom, but only copy tree structure *
     */
    @Override
    public void assignFromFragile(final StateNode other) {
        final TreePoMo tree = (TreePoMo) other;
        if (m_nodes == null) {
            initArrays();
        }
        if (tree.root.isExtra){
        	root = extraList.get(tree.root.getNr());
        }else{
        	root = m_nodes[tree.root.getNr()];
        }
        final NodePoMo[] otherNodes = tree.m_nodes;
        final int rootNr = root.getNr();
        assignFrom(0, rootNr, otherNodes, tree.extraList);
        root.isExtra=tree.root.isExtra;
        root.setNr(tree.root.getNr());
        root.start=tree.root.start;
        root.end=tree.root.end;
        root.height = otherNodes[rootNr].height;
        root.parent = null;
        for(int i=0;i<tree.root.samples.size();i++){
        	//root.addSample(tree.root.samples.get(i),tree.root.sampleTimes.get(i));
        	root.addSample(tree.root.samples.get(i));
        }
        for (int i=0; i<otherNodes[rootNr].children.size();i++){
        	if(otherNodes[rootNr].getChild(i).isExtra){
        		root.addChild(extraList.get(otherNodes[rootNr].getChild(i).getNr()));
        	}else{
        		root.addChild(m_nodes[otherNodes[rootNr].getChild(i).getNr()]);
        	}
        }
//        if (otherNodes[rootNr].getLeft() != null) {
//            root.setLeft(m_nodes[otherNodes[rootNr].getLeft().getNr()]);
//        } else {
//            root.setLeft(null);
//        }
//        if (otherNodes[rootNr].getRight() != null) {
//            root.setRight(m_nodes[otherNodes[rootNr].getRight().getNr()]);
//        } else {
//            root.setRight(null);
//        }
        assignFrom(rootNr + 1, m_nodes.length+extraList.size(), otherNodes, tree.extraList);
    }

    /**
     * helper to assignFromFragile *
     */
    private void assignFrom(final int start, final int end, final NodePoMo[] otherNodes, List<NodePoMo> extra) {
        for (int i = start; i < end; i++) {
            NodePoMo sink = m_nodes[i];
            NodePoMo src = otherNodes[i];
            sink.height = src.height;
            if (sink.isExtra){
            	sink.parent = extraList.get(src.parent.getNr());
            }else{
            	sink.parent = m_nodes[src.parent.getNr()];
            }
            sink.isExtra=src.isExtra;
            sink.setNr(src.getNr());
            sink.start=src.start;
            sink.end=src.end;
            for(int s=0;s<src.samples.size();s++){
            	//sink.addSample(src.samples.get(s),src.sampleTimes.get(s));
            	sink.addSample(src.samples.get(s));
            }
            for (int c=0; c<src.children.size();c++){
            	if(src.getChild(c).isExtra){
            		sink.addChild(extraList.get(src.getChild(c).getNr()));
            	}else{
            		sink.addChild(m_nodes[src.getChild(c).getNr()]);
            	}
            }
//            if (src.getLeft() != null) {
//                sink.setLeft(m_nodes[src.getLeft().getNr()]);
//                if (src.getRight() != null) {
//                    sink.setRight(m_nodes[src.getRight().getNr()]);
//                } else {
//                    sink.setRight(null);
//                }
//            }
        }
    }


    @Override
	public String toString() {
        return root.toString();
    }


    /**
     * StateNode implementation
     */
    @Override
    public void setEverythingDirty(final boolean isDirty) {
        setSomethingIsDirty(isDirty);
        if (!isDirty) {
            for( NodePoMo n : m_nodes ) {
                n.isDirty = IS_CLEAN;
            }
            for( NodePoMo n : extraList ) {
                n.isDirty = IS_CLEAN;
            }
          //  root.makeAllDirty(IS_CLEAN);
        } else {
            for( NodePoMo n : m_nodes ) {
                n.isDirty = IS_FILTHY;
            }
            for( NodePoMo n : extraList ) {
                n.isDirty = IS_FILTHY;
            }
        //    root.makeAllDirty(IS_FILTHY);
        }
    }

    @Override
    public int scale(final double scale) {
        int s=root.scale(scale);
        if (s==0) return 0;
        else return s;
        //return getInternalNodeCount()- getDirectAncestorNodeCount();
    }


//    /**
//     * The same as scale but with option to scale all sampled nodes
//     * @param scale
//     * @param scaleSNodes if true all sampled nodes are scaled. Note, the most recent node is considered to
//     *                    have height 0.
//     * @return
//     */
//    public int scale(double scale, boolean scaleSNodes) {
//        ((ZeroBranchSANode)root).scale(scale, scaleSNodes);
//        if (scaleSNodes) {
//            return getNodeCount() - 1 - getDirectAncestorNodeCount();
//        } else {
//            return getInternalNodeCount() - getDirectAncestorNodeCount();
//        }
//    }

    /** Loggable interface implementation follows **/

    /**
     * print translate block for NEXUS beast.tree file
     */
    public static void printTranslate(final NodePoMo node, final PrintStream out, final int nodeCount) {
        final List<String> translateLines = new ArrayList<>();
        printTranslate(node, translateLines, nodeCount);
        Collections.sort(translateLines);
        for (final String line : translateLines) {
            out.println(line);
        }
    }

    static public int taxaTranslationOffset = 1;

    /**
     * need this helper so that we can sort list of entries *
     */
    static void printTranslate(NodePoMo node, List<String> translateLines, int nodeCount) {
    	final String nr = (node.getNr() + taxaTranslationOffset) + "";
        String line = "\t\t" + "    ".substring(nr.length()) + nr + " " + node.getID();
        if (node.getNr() < nodeCount) {
            line += ",";
        }
        translateLines.add(line);
        if (!(node.isLeaf())) {
        	for(int i=0;i<node.children.size();i++){
        		 printTranslate(node.getChild(i), translateLines, nodeCount);
        	}
//            printTranslate(node.getLeft(), translateLines, nodeCount);
//            if (node.getRight() != null) {
//                printTranslate(node.getRight(), translateLines, nodeCount);
//            }
        }
    }

    public static void printTaxa(final NodePoMo node, final PrintStream out, final int nodeCount) {
        final List<String> translateLines = new ArrayList<>();
        printTranslate(node, translateLines, nodeCount);
        Collections.sort(translateLines);
        for (String line : translateLines) {
            line = line.split("\\s+")[2];
            out.println("\t\t\t" + line.replace(',', ' '));
        }
    }
    
    //@Override
	public NodePoMo getRoot() {
        return root;
    }

    @Override
	public void init(PrintStream out) {
        NodePoMo node = getRoot();
        out.println("#NEXUS\n");
        out.println("Begin taxa;");
        //int leaves=getLeafNodeCount();
        int leaves=nodeCount;
        out.println("\tDimensions ntax=" + leaves + ";");
        out.println("\t\tTaxlabels");
        printTaxa(node, out, leaves);
        out.println("\t\t\t;");
        out.println("End;");

        out.println("Begin trees;");
        out.println("\tTranslate");
        printTranslate(node, out, leaves);
        out.print(";");
    }

    @Override
	public void log(long sample, PrintStream out) {
        TreePoMo tree = (TreePoMo) getCurrent();
        out.print("tree STATE_" + sample + " = ");
        // Don't sort, this can confuse CalculationNodes relying on the tree
        //tree.getRoot().sort();
        final int[] dummy = new int[1];
        final String newick = tree.getRoot().toSortedNewick(dummy);
        out.print(newick);
        out.print(";");
    }

    /**
     * @see beast.base.core.Loggable *
     */
    @Override
	public void close(PrintStream out) {
        out.print("End;");
    }

    /**
     * reconstruct tree from XML fragment in the form of a DOM node *
     */
    @Override
    public void fromXML(final org.w3c.dom.Node node) {
//        final String newick = node.getTextContent();
//        final TreeParser parser = new TreeParser();
//        try {
//            parser.thresholdInput.setValue(1e-10, parser);
//        } catch (Exception e1) {
//            e1.printStackTrace();
//        }
//        try {
//            parser.offsetInput.setValue(0, parser);
//            setRoot(parser.parseNewick(newick));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        System.out.println("function to read from PoMo newick tree not yet implemented!");
        System.exit(0);
        initArrays();
    }

    /**
     * Valuable implementation *
     */
    @Override
	public int getDimension() {
        return getNodeCount();
    }

    //@Override
	public double getArrayValue() {
        return root.height;
    }

    //@Override
	public double getArrayValue(int value, boolean isE) {
		if (isE){
			return extraList.get(value).height;
		}else{
			return m_nodes[value].height;
		}
    }
	
	public double getArrayValue(int value) {
		return m_nodes[value].height;
    }
	
	public double getArrayValueExtra(int value) {
		return extraList.get(value).height;
    }

    /**
     * StateNode implementation *
     */
    @Override
    protected void store() {
    	//System.out.println("Storing tree start ");
    	//this should not matter as the number of observed nodes is not modifiable
    	if (m_storedNodes.length != m_nodes.length) {
            final NodePoMo[] tmp = new NodePoMo[m_nodes.length];
            System.arraycopy(m_storedNodes, 0, tmp, 0, m_storedNodes.length - 1);
            if (m_nodes.length > m_storedNodes.length) {
                tmp[m_storedNodes.length - 1] = m_storedNodes[m_storedNodes.length - 1];
                tmp[m_nodes.length - 1] = new NodePoMo();
                tmp[m_nodes.length - 1].setNr(m_nodes.length - 1);
            }
            m_storedNodes = tmp;
        }
    	//fix size of stored extra node list
    	if (extraList.size() > storedExtraList.size()) {
        	for (int i=storedExtraList.size();i<extraList.size();i++){
        		storedExtraList.add(new NodePoMo());
        		storedExtraList.get(i).setNr(i);
        		storedExtraList.get(i).isExtra=true;
        	}
        }else if (extraList.size() < storedExtraList.size()) {
        	for (int i=extraList.size();i<storedExtraList.size();i++){
        		storedExtraList.remove(storedExtraList.size()-1);
        	}
        }

        storeNodes(0, m_nodes.length);
        storeNodesExtra(0, extraList.size());
        if (root.isExtra){
        	storedRoot = storedExtraList.get(root.getNr());
        }else{
        	storedRoot = m_storedNodes[root.getNr()];
        }
        //System.out.println("Storing tree end ");
    }


    /**
     * Stores nodes with index i, for start <= i < end
     * (i.e. including start but not including end)
     *
     * @param start the first index to be stored
     * @param end   nodes are stored up to but not including this index
     */
    private void storeNodes(final int start, final int end) {
        // Use direct members for speed (we are talking 5-7% or more from total time for large trees :)
        for (int i = start; i < end; i++) {
            final NodePoMo sink = m_storedNodes[i];
            final NodePoMo src = m_nodes[i];
            storeNode(sink, src, i);
        }
    }
    
    private void storeNodesExtra(final int start, final int end) {
        // Use direct members for speed (we are talking 5-7% or more from total time for large trees :)
        for (int i = start; i < end; i++) {
            final NodePoMo sink = storedExtraList.get(i);
            final NodePoMo src = extraList.get(i);
            storeNode(sink, src, i);
        }
    }
    
    private void storeNode(NodePoMo sink, NodePoMo src, final int i) {
    	//System.out.println("Storing node start ");
        // Use direct members for speed (we are talking 5-7% or more from total time for large trees :)
            sink.height = src.height;
            sink.start = src.start;
            sink.end = src.end;
            sink.labelNr=i;
            sink.isExtra=src.isExtra;
            sink.setName(src.hostName);
            sink.metaDataString = src.metaDataString;
            sink.metaData = new TreeMap<>(src.metaData);
            sink.setID(src.getID());
            //sink.samples=new ArrayList<Taxon>();
            sink.samples=new ArrayList<Sample>();
            for(int j=0;j<src.samples.size();j++){
            	//sink.addSample(src.samples.get(j),src.sampleTimes.get(j));
            	sink.addSample(src.samples.get(j));
            }

            if ( src.parent != null ) {
            	if (src.parent.isExtra){
            		sink.parent = storedExtraList.get(src.parent.getNr());
            	}else{
            		sink.parent = m_storedNodes[src.parent.getNr()];
            	}
            } else {
                sink.parent = null;
            }

            final List<NodePoMo> children = sink.children;
            final List<NodePoMo> srcChildren = src.children;

            if( children.size() == srcChildren.size() ) {
               // save some more time by avoiding list clear and add
               for (int k = 0; k < children.size(); ++k) {
                   final NodePoMo srcChild = srcChildren.get(k);
                   final NodePoMo c;
                   if (srcChild.isExtra){
                	   c = storedExtraList.get(srcChild.getNr());
                   }else{
                	   c = m_storedNodes[srcChild.getNr()];
                   }
                   c.parent = sink;
                   children.set(k, c);
               }
            } else {
                children.clear();
                for (final NodePoMo srcChild : srcChildren) {
                	final NodePoMo c;
                	if (srcChild.isExtra){
                 	   c = storedExtraList.get(srcChild.getNr());
                    }else{
                 	   c = m_storedNodes[srcChild.getNr()];
                    }
                    c.parent = sink;
                    children.add(c);
                }
            }
            
            //System.out.println("Storing node end ");
    }
    
    protected NodePoMo[] postCache = null;
    //@Override
//	public NodePoMo[] listNodesPostOrder(NodePoMo node, NodePoMo[] nodes) {
//        if( node != null ) {
//            return TreeInterface.super.listNodesPostOrder(node, nodes);
//        }
//        if( postCache == null ) {
//            postCache = TreeInterface.super.listNodesPostOrder(node, nodes);
//        }
//        return postCache;
//    }

    @Override
    public void startEditing(final Operator operator) {
        super.startEditing(operator);
        postCache = null;
    }

    @Override
    public void restore() {
    	
    	//System.out.println("Restoring tree start ");

        // necessary for sampled ancestor trees
        nodeCount = m_storedNodes.length + storedExtraList.size();

        final NodePoMo[] tmp = m_storedNodes;
        m_storedNodes = m_nodes;
        m_nodes = tmp;
        final List<NodePoMo> tmp2 = storedExtraList;
        storedExtraList = extraList;
        extraList = tmp2;
        if (storedRoot.isExtra){
        	root = extraList.get(storedRoot.getNr());
        }else{
        	root = m_nodes[storedRoot.getNr()];
        }
        leafNodeCount = 0;
        for( NodePoMo n : m_nodes ) {
            leafNodeCount += n.isLeaf() ? 1 : 0;
        }
        for( NodePoMo n : extraList ) {
            leafNodeCount += n.isLeaf() ? 1 : 0;
        }
        hasStartedEditing = false;
        for( NodePoMo n : m_nodes ) {
            n.isDirty = Tree.IS_CLEAN;
        }
        for( NodePoMo n : extraList ) {
            n.isDirty = Tree.IS_CLEAN;
        }
        postCache = null;
        //System.out.println("Restoring tree end ");
    }

//    /**
//     * @return Date trait set if available, null otherwise.
//     */
//    public TraitSet getDateTrait() {
//        if (!traitsProcessed)
//            processTraits(m_traitList.get());
//
//        return timeTraitSet;
//    }

//    /**
//     * Determine whether tree has a date/time trait set associated with it.
//     *
//     * @return true if so
//     */
//    public boolean hasDateTrait() {
//        return getDateTrait() != null;
//    }

//    /**
//     * Specifically set the date trait set for this tree. A null value simply
//     * removes the existing trait set.
//     *
//     * @param traitSet
//     */
//    public void setDateTrait(TraitSet traitSet) {
//        if (hasDateTrait()) {
//            m_traitList.get().remove(timeTraitSet);
//        }
//
//        if (traitSet != null)
//            m_traitList.get().add(traitSet);
//
//        timeTraitSet = traitSet;
//    }

    /**
     * Convert age/height to the date time scale given by a trait set,
     * if one exists.  Otherwise just return the unconverted height.
     *
     * @param height
     * @return date specified by height
     */
    public double getDate(final double height) {
    	return traitSet.getDate(height);
//        if (hasDateTrait()) {
//            return timeTraitSet.getDate(height);
//        } else
//            return height;
    }

//    /**
//     * This method allows the retrieval of the taxon label of a node without using the node number.
//     *
//     * @param node
//     * @return the name of the given node, or null if the node is unlabelled
//     */
//    public String getTaxonId(final Node node) {
//        return getTaxaNames()[node.getNr()];  //To change body of created methods use File | Settings | File Templates.
//    }

    /**
     * Removes the i'th node in the tree. Results in a renumbering of the remaining nodes so that their numbers
     * faithfully describe their new position in the array. nodeCount and leafNodeCount are recalculated.
     * Use with care!
     *
     * @param i the index of the node to be removed.
     */
    public void removeNode(final int i, boolean isExtra) {
    	if (isExtra){
    		extraList.remove(i);
    		for (int j = i; j < extraList.size(); j++) {
                extraList.get(j).setNr(j);
            }
//    		final List<NodePoMo> tmp = new ArrayList<NodePoMo>();
//    		for (int j = 0; j < i; j++) {
//    			tmp.add(extraList.get(j));
//    		}
//            for (int j = i; j < extraList.size() - 1; j++) {
//            	tmp.add(extraList.get(j+1));
//                tmp.get(j).setNr(j);
//            }
//            extraList = tmp;
    	}else{
    		final NodePoMo[] tmp = new NodePoMo[m_nodes.length - 1];
            System.arraycopy(m_nodes, 0, tmp, 0, i);
            for (int j = i; j < m_nodes.length - 1; j++) {
                tmp[j] = m_nodes[j + 1];
                tmp[j].setNr(j);
            }
            m_nodes = tmp;
    	}
        nodeCount--;
        //leafNodeCount--;
    }

    /**
     * Adds a node to the end of the node array. nodeCount and leafNodeCount are recalculated.
     * Use with care!
     */
    public void addNode(final NodePoMo newNode) {
    	if (newNode.isExtra){
    		extraList.add(newNode);
    		newNode.setNr(extraList.size()-1);
    	}else{
    		final NodePoMo[] tmp = new NodePoMo[m_nodes.length + 1];
            System.arraycopy(m_nodes, 0, tmp, 0, m_nodes.length);
            tmp[m_nodes.length] = newNode;
            newNode.setNr(m_nodes.length);
            m_nodes = tmp;
    	}
        nodeCount++;
//        if (newNode.isLeaf()){
//        	leafNodeCount++;
//        }
    }

    public int getDirectAncestorNodeCount() {
        int directAncestorNodeCount = 0;
        for (int i = 0; i < leafNodeCount; i++) {
            if (this.getNode(i).isDirectAncestor()) {
                directAncestorNodeCount += 1;
            }
        }
        return directAncestorNodeCount;
    }




//	@Override
//	public double getArrayValue(int dim) {
//		return 0;
//	}


//    @Override
//    public TaxonSet getTaxonset() {
//        return m_taxonset.get();
//    }
} // class Tree