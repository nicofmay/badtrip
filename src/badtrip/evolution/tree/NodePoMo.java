/*
* File Node.java
*
* Copyright (C) 2010 Remco Bouckaert remco@cs.auckland.ac.nz
*
* This file is part of BEAST2.
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership and licensing.
*
* BEAST is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
*  BEAST is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with BEAST; if not, write to the
* Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
* Boston, MA  02110-1301  USA
*/
package badtrip.evolution.tree;


import badtrip.evolution.datatype.Sample;
import beast.base.core.BEASTObject;
import beast.base.core.Description;
import beast.base.evolution.tree.TraitSet;
import beast.base.evolution.tree.Tree;

import java.util.*;
//import beast.base.util.HeapSort;
//import beast.base.util.HeapSort;



@Description("Nodes in building treePoMo data structure (transmission trees).")
//public class Node extends BEASTObject {
public class NodePoMo extends BEASTObject {
	
	/**
     * label nr of node, used mostly when this is a leaf.
     */
    protected int labelNr;

    /**
     * height of this node (infection time of this host).
     */
    protected double height = Double.MAX_VALUE;

    /**
     * Arbitrarily labeled metadata on this node. Not currently implemented as part of state!
     */
    protected Map<String, Object> metaData = new TreeMap<>();

    /**
     * list of children of this node *
     * Don't use m_left and m_right directly
     * Use getChildCount() and getChild(x) or getChildren() instead
     */
    List<NodePoMo> children = new ArrayList<>();

    /**
     * values of this node.
     * start is the earliest time a host can be infected
     * end is the last time.
     * infectionT is the time in which the host was infected.
     * hostNmae is the name of the host that the node represents.
     */
    protected double start = Double.MAX_VALUE;
    protected double end = Double.MIN_VALUE;
    public String hostName;
    
    
    /**
     * list of samples *
     */
    //List<Double> sampleTimes = new ArrayList<>();
    List<Sample> samples = new ArrayList<>();

    /**
     * parent node (infector) in the beast.tree, null if root *
     */
    NodePoMo parent = null;

    /**
     * status of this node after an operation is performed on the state *
     */
    int isDirty = TreePoMo.IS_CLEAN;
    
    /**
     * meta-data contained in square brackets in Newick *
     */
    public String metaDataString;

    /**
     * The TreePoMo (transmission tree) that this node is a part of.
     * This allows e.g. access to the State containing the Tree *
     */
    protected TreePoMo m_tree;
    
    protected boolean isExtra=false;

    public NodePoMo() {
    }

    public NodePoMo(final String id) {
        setID(id);
        initAndValidate();
    }
    
    public NodePoMo(final String id, final String id2) {
        setID(id);
        setName(id2);
        initAndValidate();
    }
    
    @Override
    public void initAndValidate() {
        // do nothing
    }
    
    public void setName(final String ID) {
        this.hostName = ID;
    }
    
    public TreePoMo getTree() {
        return m_tree;
    }
    
    /**
     * @return number uniquely identifying the node in the tree.
     *         This is a number between 0 and the total number of nodes in the tree
     *         Leaf nodes are number 0 to #leaf nodes -1
     *         Internal nodes are numbered  #leaf nodes  up to #nodes-1
     *         The root node is always numbered #nodes-1
     */
    public int getNr() {
        return labelNr;
    }

    public void setNr(final int labelIndex) {
        labelNr = labelIndex;
    }

    public double getHeight() {
        return height;
    }

    public double getDate() {
        return m_tree.getDate(height);
    }
    
    public boolean getIfExtra() {
        return isExtra;
    }

    public void setHeight(final double height) {
        startEditing();
        this.height = height;
        isDirty |= Tree.IS_DIRTY;
        if (!isLeaf()) {
        	for (int i=0;i<children.size();i++){
        		children.get(i).isDirty |= Tree.IS_DIRTY;
        	}
        }
    }
    
    /**
     * @return length of branch between this node and its parent in the beast.tree
     */
    public final double getLength() {
        if (isRoot()) {
            return 0;
        } else {
            return getParent().height - height;
        }
    }

    
    public void setStart(Double t) {
        this.start = t;
    }
    
    public void setEnd(Double t) {
        this.end = t;
    }
    
    public void setSamples(List<Sample> s) {
        this.samples=s;
    }
    
//    public void setSamples(List<Taxon> s) {
//        this.samples=s;
//    }
//    
//    public void setSampleTimes(List<Double> l) {
//        this.sampleTimes=l;
//    }
    
    //NICOLA TODO : Check is descending order is the right order. Use this function.
    public void sortChildren() {
    	Collections.sort(this.children,Comparator.comparing(item -> -item.height));
    	//this.children.sort(Comparator.comparing());
        //this.children
    }
    
    //NICOLA TODO : Check is descending order is the right order. Use this function.
    public void sortSamples() {
    	Collections.sort(this.samples,Comparator.comparing(item -> -item.time));
    }
    
    //public void addSample(Taxon t) {
    //    this.samples.add(t);
    //}
    
    //public void addSampleTime(double t) {
    //    this.sampleTimes.add(t);
    //}
    
    public void setChildrenImmediate(List<NodePoMo> s) {
        this.children=s;
    }
    
    public void addChildImmediate(NodePoMo t) {
        this.children.add(t);
    }
    
    
    public String getName() {
        return this.hostName;
    }
    
    public double getEnd() {
        return end;
    }
    
    public double getStart() {
        return start;
    }
    
    /**
     * methods for accessing the dirtiness state of the Node.
     * A Node is Tree.IS_DIRTY if its value (like height) has changed
     * A Node Tree.IS_if FILTHY if its parent or child has changed.
     * Otherwise the node is Tree.IS_CLEAN *
     */
    public int isDirty() {
        return isDirty;
    }

    public void makeDirty(final int dirty) {
        isDirty |= dirty;
    }
    
    public void makeAllDirty(final int dirty) {
        isDirty = dirty;
        if (children.size()>0) {
        	for (int i=0; i<children.size(); i++){
        		children.get(i).makeAllDirty(dirty);
        	}
        }
    }
    
    public void setIfExtra(final boolean ex) {
        isExtra=ex;
    }


    /**
     * @return parent node, or null if this is root *
     */
    public NodePoMo getParent() {
        return parent;
    }

    /**
     * Calls setParent(parent, true)
     *
     * @param parent the new parent to be set, must be called from within an operator.
     */
    public void setParent(final NodePoMo parent) {
        setParent(parent, true);
    }
    
    /**
     * Sets the parent of this node
     *
     * @param parent     the node to become parent
     * @param inOperator if true, then startEditing() is called and setting the parent will make tree "filthy"
     */
    void setParent(final NodePoMo parent, final boolean inOperator) {
        if (inOperator) startEditing();
        if (this.parent != parent) {
        	this.parent = parent;
            if (inOperator) isDirty = Tree.IS_FILTHY;
        }
    }
    

    /**
     * Sets the parent of this node. No overhead, no side effects like setting dirty flags etc.
     *
     * @param parent     the node to become parent
     */
    void setParentImmediate(final NodePoMo parent) {
        this.parent = parent;
    }

    /**
     * @return unmodifiable list of children of this node
     */
    public List<NodePoMo> getChildren() {
        return Collections.unmodifiableList(children);
    }
    
    public List<Sample> getSamples() {
        return Collections.unmodifiableList(this.samples);
    }
    
//    public List<Taxon> getSamples() {
//        return Collections.unmodifiableList(this.samples);
//    }
    
//    public List<Double> getSampleTimes() {
//        return Collections.unmodifiableList(this.sampleTimes);
//    }
    
    public Sample getSample(int i) {
        return this.samples.get(i);
    }
    
//    public Taxon getSample(int i) {
//        return this.samples.get(i);
//    }
//    
//    public double getSampleTime(int i) {
//        return this.sampleTimes.get(i);
//    }
    
    /**
     * get all child node under this node, if this node is leaf then list.size() = 0.
     *
     * @return
     */
    public List<NodePoMo> getAllChildNodes() {
        final List<NodePoMo> childNodes = new ArrayList<>();
        if (!this.isLeaf()) getAllChildNodes(childNodes);
        return childNodes;
    }

    // recursive
    public void getAllChildNodes(final List<NodePoMo> childNodes) {
        childNodes.add(this);
        for (NodePoMo child : children)
            child.getAllChildNodes(childNodes);
    }
    
    /**
     * get all leaf node under this node, if this node is leaf then list.size() = 0.
     *
     * @return
     */
    public List<NodePoMo> getAllLeafNodes() {
        final List<NodePoMo> leafNodes = new ArrayList<>();
        if (!this.isLeaf()) getAllLeafNodes(leafNodes);
        return leafNodes;
    }

    // recursive
    public void getAllLeafNodes(final List<NodePoMo> leafNodes) {
        if (this.isLeaf()) {
            leafNodes.add(this);
        }

        for (NodePoMo child : children)
            child.getAllLeafNodes(leafNodes);
    }
    
    /**
     * @return true if current node is root node *
     */
    public boolean isRoot() {
        return parent == null;
    }

    /**
     * @return true if current node is a leaf node *
     */
    public boolean isLeaf() {
    	//System.out.println("Host "+this.getName()+" has "+children.size()+" children");
        return children.size() == 0;
        //return getLeft() == null && getRight() == null;
    }

    public void removeChild(final NodePoMo child) {
        startEditing();
        children.remove(child);
    }

    /**
     * Removes all children from this node.
     *
     * @param inOperator if true then startEditing() is called. For operator uses, called removeAllChildren(true), otherwise
     *                   use set to false.
     */
    public void removeAllChildren(final boolean inOperator) {
        if (inOperator) startEditing();
        children.clear();
    }
    
    //NICOLA TODO: make sure that list is sorted after adding a child using the function sortChildren
    public void addChild(final NodePoMo child) {
        child.setParent(this);
        children.add(child);
        //sortChildren();
    }
    
    /**
     * @return count number of nodes in beast.tree, starting with current node *
     */
    public int getNodeCount() {
        int nodes = 1;
        for (final NodePoMo child : children) {
            nodes += child.getNodeCount();
        }
        return nodes;
    }

    public int getLeafNodeCount() {
        if (isLeaf()) {
        	//System.out.println(this.getID()+" is leaf and returns 1");
            return 1;
        }
        int nodes = 0;
        for (final NodePoMo child : children) {
            nodes += child.getLeafNodeCount();
        }
        //System.out.println(this.getID()+" is not leaf and returns "+nodes);
        return nodes;
    }

    public int getInternalNodeCount() {
        if (isLeaf()) {
            return 0;
        }
        int nodes = 1;
        for (final NodePoMo child : children) {
            nodes += child.getInternalNodeCount();
        }
        return nodes;
    }
    

    /**
     * @return beast.tree in Newick format, with length and meta data
     *         information. Unlike toNewick(), here Nodes are numbered, instead of
     *         using the node labels.
     *         If there are internal nodes with non-null IDs then their numbers are also printed.
     *         Also, all internal nodes are labelled if printInternalNodeNumbers
     *         is set true. This is useful for example when storing a State to file
     *         so that it can be restored.
     */
    public String toShortNewick(final boolean printInternalNodeNumbers) {
        final StringBuilder buf = new StringBuilder();
        if (children.size()>0) {
        	buf.append("(");
        	for (int i=0; i<children.size();i++){
        		buf.append(children.get(i).toShortNewick(printInternalNodeNumbers));
        		if (i<children.size()-1) buf.append(',');
        	}
        	buf.append(")");
        }
        if (getID() != null) {
            buf.append(getNr());
        } else if (printInternalNodeNumbers) {
            buf.append(getNr());
        }
        buf.append(":").append(getLength());
        return buf.toString();
    }

    /**
     * prints newick string where it orders by highest leaf number
     * in a clade. Print node numbers (m_iLabel) incremented by 1
     * for leaves and internal nodes with non-null IDs.
     */
    public String toSortedNewick(final int[] maxNodeInClade) {
        return toSortedNewick(maxNodeInClade, false);
    }

    public String toSortedNewick(int[] maxNodeInClade, boolean printMetaData) {
    	//System.out.println("Start to string newick");
        StringBuilder buf = new StringBuilder();
        if (children.size() != 0) {
            buf.append("(");
            List<String> strings = new ArrayList<>();
            List<Integer> inds = new ArrayList<>();
            for (int i=0;i<children.size();i++){
            	String child = children.get(i).toSortedNewick(maxNodeInClade, printMetaData);
            	int childIndex = maxNodeInClade[0];
            	int ind=0;
            	boolean found=false;
            	//System.out.println("Start while");
            	while(ind<inds.size()){
            		if (inds.get(ind)>childIndex){
            			found=true;
            			inds.add(ind, childIndex);
            			strings.add(ind, child);
            			break;
            		}
            		ind+=1;
            	}
            	//System.out.println("End while");
            	if(found==false){
            		inds.add(childIndex);
        			strings.add(child);
            	}
            }
            for (int i=0;i<children.size();i++){
            	buf.append(strings.get(i));
            	if(i<(children.size()-1)){
            		buf.append(",");
            	}
            }
            maxNodeInClade[0]=inds.get(0);
            buf.append(")");
            //if (getID() != null) {
            //    buf.append(labelNr+1);
            //}
        } else {
            maxNodeInClade[0] = labelNr;
            buf.append(labelNr + 1);
        }
        if (this.isExtra) buf.append("[&host=Unsampled,height="+this.height+"]");
        else buf.append("[&host="+this.hostName+",height="+this.height+"]");

        if (printMetaData) {
            buf.append(getNewickMetaData());
        }
        buf.append(":").append(getLength());
        //System.out.println("End to string newick: "+buf.toString());
        return buf.toString();
    }

    /**
     *
     * @param onlyTopology  if true, only print topology
     * @return
     */
    public String toNewick(boolean onlyTopology) {
        final StringBuilder buf = new StringBuilder();
        if (children.size()>0) {
        	buf.append("(");
        	for (int i=0; i<children.size();i++){
        		buf.append(children.get(i).toNewick(onlyTopology));
        		if (i<children.size()-1) buf.append(',');
        	}
        	buf.append(")");
        	if (getID() != null) {
                buf.append(getID());
            }
        } else {
            if (getID() == null) {
                buf.append(labelNr);
            } else {
                buf.append(getID());
            }
        }
        if (!onlyTopology) {
            //buf.append(getNewickMetaData());
            buf.append(":").append(getLength());
        }
        return buf.toString();
    }


    /**
     * @return beast.tree in Newick format with taxon labels for labelled tip nodes
     * and labeled (having non-null ID) internal nodes.
     * If a tip node doesn't have an ID (taxon label) then node number (m_iLabel) is printed.
     */
    public String toNewick() {
        return toNewick(false);
    }
    
    public String getNewickMetaData() {
        if (metaDataString != null) {
            return "[&" + metaDataString + ']';
        }
        return "";
    }


    /**
     * @param labels
     * @return beast.tree in long Newick format, with all length and meta data
     *         information, but with leafs labelled with their names
     */
    public String toString() {
        final StringBuilder buf = new StringBuilder();
        if (children.size()>0) {
        	buf.append("(");
        	for (int i=0; i<children.size();i++){
        		buf.append(children.get(i).toString());
        		if (i<children.size()-1) buf.append(',');
        	}
        	buf.append(")");
        }
        if (getName() != null) {
            buf.append(getName());
        }
        if (metaDataString != null) {
            buf.append('[');
            buf.append(metaDataString);
            buf.append(']');
        }
        buf.append(":").append(getLength());
        return buf.toString();
    }
    
    /**
     * sorts nodes according to their label number
     *
     * @return
     */
    public int sort() {
    	if (isExtra){
    		return m_tree.hosts.size() + labelNr;
    	}else{
    		return labelNr;
    	}

//        if (isLeaf()) {
//            return labelNr;
//        }
//        final int childCount = getChildCount();
//
//        if (childCount == 1) return getChild(0).sort();
//
//        final List<Integer> lowest = new ArrayList<>();
//        final int[] indices = new int[childCount];
//
//        // relies on this being a copy of children list
//        final List<NodePoMo> children = new ArrayList<>(getChildren());
//
//        for (final NodePoMo child : children) {
//            lowest.add(child.sort());
//        }
//        HeapSort.sort(lowest, indices);
//        for (int i = 0; i < childCount; i++) {
//            setChild(i, children.get(indices[i]));
//        }
//        return lowest.get(indices[0]);
    } // sort

    protected void startEditing() {
        if (m_tree != null && m_tree.getState() != null) {
            m_tree.startEditing(null);
        }
    }

    /**
     * during parsing, leaf nodes are numbered 0...m_nNrOfLabels-1
     * but internal nodes are left to zero. After labeling internal
     * nodes, m_iLabel uniquely identifies a node in a beast.tree.
     *
     * @param labelIndex
     * @return
     */
    public int labelInternalNodes(int labelIndex) {
        if (isLeaf()) {
            return labelIndex;
        } else {
        	for(int i=0;i<children.size();i++){
        		labelIndex = children.get(i).labelInternalNodes(labelIndex);
        	}
        	labelNr = labelIndex++;
        }
        return labelIndex;
    } // labelInternalNodes

    /**
     * @return (deep) copy of node
     */
    public NodePoMo copy() {
        final NodePoMo node = new NodePoMo();
        node.height = height;
        node.start = start;
        node.end = end;
        node.hostName=hostName;
        node.labelNr = labelNr;
        node.isExtra=isExtra;
        node.metaDataString = metaDataString;
        node.metaData = new TreeMap<>(metaData);
        node.parent = null;
        node.setID(getID());
        for (final NodePoMo child : getChildren()) {
            node.addChild(child.copy());
        }
        for(int i=0;i<samples.size();i++){
        	//node.addSample(samples.get(i),sampleTimes.get(i));
        	node.addSample(samples.get(i));
        }
        return node;
    } // copy
    
    public NodePoMo copy(List<NodePoMo> extraCopy) {
        final NodePoMo node = new NodePoMo();
        node.height = height;
        node.start = start;
        node.end = end;
        node.hostName=hostName;
        node.labelNr = labelNr;
        node.isExtra=isExtra;
        node.metaDataString = metaDataString;
        node.metaData = new TreeMap<>(metaData);
        node.parent = null;
        node.setNr(this.getNr());
        node.setID(getID());
        if (isExtra){
        	extraCopy.set(labelNr, node);
        	//extraCopy.add(node);
        }
        for (final NodePoMo child : getChildren()) {
            node.addChild(child.copy());
        }
        for(int i=0;i<samples.size();i++){
        	//node.addSample(samples.get(i),sampleTimes.get(i));
        	node.addSample(samples.get(i));
        }
        return node;
    } // copy
    
//    public void addSample(Taxon sample, double time){
//    	samples.add(sample);
//    	sampleTimes.add(time);
//    }
    
    public void addSample(Sample sample){
    	samples.add(sample);
    }

    /**
     * assign values to a tree in array representation *
     */
    public void assignTo(final NodePoMo[] nodes, final List<NodePoMo> extraCopy) {
    	final NodePoMo node;
    	if(!isExtra){
    		node = nodes[getNr()];
    	}else{
    		node = extraCopy.get(getNr());
    	}
        node.start = start;
        node.height = height;
        node.end = end;
        node.labelNr = labelNr;
        node.isExtra=isExtra;
        node.setName(hostName);
        node.metaDataString = metaDataString;
        node.metaData = new TreeMap<>(metaData);
        node.parent = null;
        node.setID(getID());
        for(int i=0;i<samples.size();i++){
        	//node.addSample(samples.get(i),sampleTimes.get(i));
        	node.addSample(samples.get(i));
        }
        for(int i=0;i<children.size();i++){
        	if (children.get(i).isExtra){
        		node.addChild(extraCopy.get(children.get(i).getNr()));
        	}else{
        		node.addChild(nodes[children.get(i).getNr()]);
        	}
        	children.get(i).assignTo(nodes, extraCopy);
        	node.getChildren().get(i).setParent(node);
        }
    }

    /**
     * assign values from a tree in array representation *
     */
    public void assignFrom(final NodePoMo[] nodes, final List<NodePoMo> extra, final NodePoMo node) {
        height = node.height;
        start = node.start;
        end = node.end;
        hostName = node.getName();
        labelNr = node.labelNr;
        isExtra = node.isExtra;
        metaDataString = node.metaDataString;
        metaData = new TreeMap<>(node.metaData);
        parent = null;
        setNr(node.getNr());
        setID(node.getID());
        for(int i=0;i<node.samples.size();i++){
        	//addSample(node.samples.get(i),node.sampleTimes.get(i));
        	addSample(node.samples.get(i));
        }
        for(int i=0;i<node.children.size();i++){
        	if (node.children.get(i).isExtra){
        		addChild(extra.get(node.children.get(i).getNr()));
        	}else{
        		addChild(nodes[node.children.get(i).getNr()]);
        	}
        	children.get(i).assignFrom(nodes, extra, node.children.get(i));
        	children.get(i).setParent(this);
        }
    }

    /**
     * set meta-data according to pattern.
     * Only heights are recognised, but derived classes could deal with
     * richer meta data pattersn.
     */
    public void setMetaData(final String pattern, final Object value) {
        startEditing();
        if (pattern.equals(TraitSet.DATE_TRAIT) ||
                pattern.equals(TraitSet.DATE_FORWARD_TRAIT) ||
                pattern.equals(TraitSet.DATE_BACKWARD_TRAIT)) {
            height = (Double) value;
            isDirty |= Tree.IS_DIRTY;
        } else {
            metaData.put(pattern, value);
        }

    }

    public Object getMetaData(final String pattern) {
        if (pattern.equals(TraitSet.DATE_TRAIT) ||
                pattern.equals(TraitSet.DATE_FORWARD_TRAIT) ||
                pattern.equals(TraitSet.DATE_BACKWARD_TRAIT)) {
            return height;
        } else {
            final Object d = metaData.get(pattern);
            if (d != null) return d;
        }
        return 0;
    }

    public Set<String> getMetaDataNames() {
        return metaData.keySet();
    }




    /**
     * scale height of this node and all its descendants
     *
     * @param scale scale factor
     */
    //NICOLA TODO : Check that times back in time are in fact higher
    public int scale(final double scale) {
        startEditing();
        isDirty |= Tree.IS_DIRTY;
        //System.out.println("Scaling node at height "+height);
        height *= scale;
        //System.out.println("New height "+height);
        if (samples.size()>0 && samples.get(0).getTime()>height) return 0;
        if (children.size()>0 && children.get(0).getHeight()>height) return 0;
        if (end>height) return 0;
        if (start<height) return 0;
        int scaled=1;
        //System.out.println("accepted, number of children: "+children.size());
        for (int i=0;i<children.size();i++){
        	int s=children.get(i).scale(scale);
        	if (s==0) return 0;
        	//if (children.get(i).getHeight()>height) System.out.println("Child time became before infection time");
        	if (children.get(i).getHeight()>height) throw new IllegalArgumentException("Child time became before infection time");
        	scaled+=s;
        }
        //System.out.println("New scaled after children: "+scaled);
        sortChildren();
        return scaled;
        //if (height<end || height>start) throw new IllegalArgumentException("Infection time out of epi interval");
    }

    /**
     * some methods that are useful for porting from BEAST 1 *
     */
    public int getChildCount() {
        return children.size();
    }

    public NodePoMo getChild(final int childIndex) {
        return children.get(childIndex);
    }
    
    //NICOLA TODO: after using this make sure list of children is sorted
    public void setChild(final int childIndex, final NodePoMo node) {
    	//System.out.println("Set child");
        while (children.size() < childIndex) {
            children.add(null);
        }
        children.set(childIndex,  node);
        //System.out.println("End Set child");
    }

    //NICOLA TODO: after using this make sure list of children is sorted
    public static NodePoMo connect(final List<NodePoMo> c, final double h) {
        final NodePoMo n = new NodePoMo();
        n.setHeight(h);
        n.setChildrenImmediate(c);
        for(int i=0;i<c.size();i++){
        	c.get(i).parent=n;
        }
        return n;
    }

    /**
     * @return true if this leaf actually represent a direct ancestor
     * (i.e. is on the end of a zero-length branch)
     */
    public boolean isDirectAncestor() {
        return (isLeaf() && !isRoot() && this.getParent().getHeight() == this.getHeight());
    }

    /**
     * @return true if this is a "fake" internal node (i.e. one of its children is a direct ancestor)
     */
    public boolean isFake() {
        if (this.isLeaf())
            return false;
        boolean dirAnc=false;
        for(int i=0; i<children.size();i++){
        	if (children.get(i).isDirectAncestor()){
        		dirAnc=true;
        		break;
        	}
        }
        return dirAnc;
    }

    public NodePoMo getDirectAncestorChild() {
        if (!this.isFake()) {
            return null;
        }
        for(int i=0; i<children.size();i++){
        	if (children.get(i).isDirectAncestor()){
        		return children.get(i);
        	}
        }
        return null;
    }

    public NodePoMo getNonDirectAncestorChild(){
        if (!this.isFake()) {
            return null;
        }
        for(int i=0; i<children.size();i++){
        	if (!(children.get(i).isDirectAncestor())){
        		return children.get(i);
        	}
        }
        return null;
    }

    public NodePoMo getFakeChild(){
    	for(int i=0; i<children.size();i++){
        	if (!(children.get(i).isFake())){
        		return children.get(i);
        	}
        }
        return null;
    }
    
    //NICOLA TODO : check if in fact older dates are bigger
    public boolean isConsistent(){
    	for(int i=0; i<children.size();i++){
        	if ((children.get(i).getHeight()>height)||(children.get(i).getHeight()>start)||(children.get(i).getHeight()<end) ){
        		return false;
        	}
        }
    	for(int i=0; i<samples.size();i++){
        	if ((samples.get(i).getTime()>height)||(samples.get(i).getTime()>start)||(samples.get(i).getTime()<end)){
        		return false;
        	}
        }
    	if ((height>start)||(height<end)){
    		return false;
    	}
        return true;
    }

	
} // class NodePoMo
