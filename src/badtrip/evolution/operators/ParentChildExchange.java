/*
* File SubtreeSlide.java
*
* Copyright (C) 2010 Remco Bouckaert remco@cs.auckland.ac.nz
*
* This file is part of BEAST2.
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership and licensing.
*
* BEAST is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
*  BEAST is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with BEAST; if not, write to the
* Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
* Boston, MA  02110-1301  USA
*/
/*
 * SubtreeSlideOperator.java
 *
 * Copyright (C) 2002-2006 Alexei Drummond and Andrew Rambaut
 *
 * This file is part of BEAST.
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership and licensing.
 *
 * BEAST is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 *  BEAST is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with BEAST; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */

package badtrip.evolution.operators;


//import java.text.DecimalFormat;
//import java.util.ArrayList;

import badtrip.evolution.tree.NodePoMo;
import badtrip.evolution.tree.TreePoMo;
import beast.base.core.Description;
import beast.base.inference.util.InputUtil;
import beast.base.util.Randomizer;

import java.util.List;


/**
 * Implements the subtree slide move.
 */
@Description("Exchange a parent with its first child (child becomes parent and vice-versa, infection times are also exchanged).")
public class ParentChildExchange extends TreeOperatorTransmission {

    @Override
    public void initAndValidate() {
        //size = sizeInput.get();
        //limit = limitInput.get();
    }

    /**
     *
     * @return log of Hastings Ratio, or Double.NEGATIVE_INFINITY if proposal should not be accepted *
     */
    @Override
    public double proposal() {
    	//System.out.println("Start Operator ParentChildExchange ");
    	//System.out.println("Starting parent-child exchange");
        final TreePoMo tree = (TreePoMo) InputUtil.get(treeInput, this);
        //final int leafNodeCount = tree.getInternalNodeCount();
        final int nodeCount = tree.getNodeCount();
        NodePoMo node;
        if (nodeCount<2)return Double.NEGATIVE_INFINITY;
        
        //sample random node
        int nodeNr = Randomizer.nextInt(nodeCount);
        List<NodePoMo> extras=tree.getExtraList();
        if (nodeNr>=extras.size()){
        	node=tree.getNodesAsArray()[nodeNr-extras.size()];
        }else{
        	node=extras.get(nodeNr);
        }
        //get a non-leaf random node
        while (node.isLeaf()){
        	nodeNr = Randomizer.nextInt(nodeCount);
            if (nodeNr>=extras.size()) node=tree.getNodesAsArray()[nodeNr-extras.size()];
            else node=extras.get(nodeNr);
        }
        //System.out.println("Chosen node "+node.getID());
        double value=node.getHeight();
        NodePoMo child=node.getChildren().get(0);
        double valueC=child.getHeight();
        if ((node.getSamples().size()==0 || (node.getSamples().size()>0 && node.getSamples().get(0).getTime()<valueC)) && value<child.getStart()){
        	//System.out.println("picking parent");
        	NodePoMo parent=node.getParent();
        	node.setHeight(valueC);
        	node.removeChild(child);
        	child.setParent(parent);
        	child.setHeight(value);
        	child.addChild(node);
        	if (!(parent==null)) {
        		//System.out.println("Exchanging non-root "+parent.getID());
				parent.removeChild(node);
				parent.addChild(child);
				parent.sortChildren();
			}else{
				//System.out.println("Exchanging root");
				tree.setRoot(child);
			}
			child.sortChildren();
			node.setParent(child);
			//System.out.println("Done exchange");
			//System.out.println("Operator ParentChildExchange 0 "+node.getID()+" "+child.getID());//+" "+child.getParent().getID()+" "+parent.getID()
			return 0.0;
        }
        //System.out.println("Ending parent-child exchange");
        //System.out.println("Operator ParentChildExchange -infty");
		return Double.NEGATIVE_INFINITY;
       
    }





}
