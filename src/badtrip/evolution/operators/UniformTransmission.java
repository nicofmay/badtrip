/*
* File Uniform.java
*
* Copyright (C) 2010 Remco Bouckaert remco@cs.auckland.ac.nz
*
* This file is part of BEAST2.
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership and licensing.
*
* BEAST is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
*  BEAST is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with BEAST; if not, write to the
* Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
* Boston, MA  02110-1301  USA
*/
/*
 * UniformOperator.java
 *
 * Copyright (C) 2002-2006 Alexei Drummond and Andrew Rambaut
 *
 * This file is part of BEAST.
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership and licensing.
 *
 * BEAST is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 *  BEAST is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with BEAST; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */

package badtrip.evolution.operators;

import badtrip.evolution.tree.NodePoMo;
import badtrip.evolution.tree.TreePoMo;
import beast.base.core.Description;
import beast.base.inference.util.InputUtil;
import beast.base.util.Randomizer;

import java.util.List;


@Description("Randomly selects host and moves infection time uniformly in available interval restricted by parent, children, epi data, and samples.")
public class UniformTransmission extends TreeOperatorTransmission {

    // empty constructor to facilitate construction by XML + initAndValidate
    public UniformTransmission() {
    }

    public UniformTransmission(TreePoMo tree) {
        try {
            initByName(treeInput.getName(), tree);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            throw new RuntimeException("Failed to construct Uniform Tree Operator.");
        }
    }

    @Override
    public void initAndValidate() {
    }

    /**
     * change the parameter and return the hastings ratio.
     *
     * @return log of Hastings Ratio, or Double.NEGATIVE_INFINITY if proposal should not be accepted *
     */
    @Override
    public double proposal() {
    	//System.out.println("Start Operator UniformTransmission ");
        final TreePoMo tree = (TreePoMo) InputUtil.get(treeInput, this);

        // randomly select internal node
        final int nodeCount = tree.getNodeCount();
        
        // Abort if no non-root internal nodes
        //if (tree.getInternalNodeCount()==1)
        //    return Double.NEGATIVE_INFINITY;
        
        NodePoMo node;
        if (nodeCount==0)System.out.println("What? there are not hosts in the tree!");
        final int nodeNr = Randomizer.nextInt(nodeCount);
        List<NodePoMo> extras=tree.getExtraList();
        if (nodeNr>=extras.size()){
        	node=tree.getNodesAsArray()[nodeNr-extras.size()];
        }else{
        	node=extras.get(nodeNr);
        }
        double upper;
        //double oldValue=node.getHeight();
        if (node.equals(tree.getRoot())) upper = node.getStart();
        else upper = Math.min(node.getStart(),node.getParent().getHeight());
        //double upper = Math.min(node.getStart(),node.getParent().getHeight());
        double lower = node.getEnd();
        if (node.getChildren().size()>0) lower=Math.max(lower,node.getChildren().get(0).getHeight());
        if (node.getSamples().size()>0) lower=Math.max(lower,node.getSamples().get(0).getTime());
        if (!(node.equals(tree.getRoot()))) lower = Math.max(lower,node.getParent().getEnd());
        
        final double newValue = (Randomizer.nextDouble() * (upper - lower)) + lower;
        node.setHeight(newValue);
        if(!node.isRoot())node.getParent().sortChildren();
        //System.out.println("Operator UniformTransmission "+node.getID()+" "+oldValue+" "+newValue);
        return 0.0;
    }

}
