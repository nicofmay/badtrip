/*
* File Exchange.java
*
* Copyright (C) 2010 Remco Bouckaert remco@cs.auckland.ac.nz
*
* This file is part of BEAST2.
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership and licensing.
*
* BEAST is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
*  BEAST is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with BEAST; if not, write to the
* Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
* Boston, MA  02110-1301  USA
*/
/*
 * ExchangeOperator.java
 *
 * Copyright (C) 2002-2006 Alexei Drummond and Andrew Rambaut
 *
 * This file is part of BEAST.
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership and licensing.
 *
 * BEAST is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 *  BEAST is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with BEAST; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */

package badtrip.evolution.operators;

import badtrip.evolution.tree.NodePoMo;
import badtrip.evolution.tree.TreePoMo;
import beast.base.core.Description;
import beast.base.core.Input;
import beast.base.inference.util.InputUtil;
import beast.base.util.Randomizer;

import java.util.ArrayList;
import java.util.List;


/*
 * KNOWN BUGS: WIDE operator cannot be used on trees with 4 or less tips!
 */

@Description("Implements branch exchange operations. There is a NARROW and WIDE variety. " +
        "The narrow exchange is very similar to a rooted-beast.tree nearest-neighbour " +
        "interchange but with the restriction that node height must remain consistent.")
public class ExchangeTransmission extends TreeOperatorTransmission {
    final public Input<Boolean> isNarrowInput = new Input<>("isNarrow", "if true (default) a narrow exchange is performed, otherwise a wide exchange", true);

    @Override
    public void initAndValidate() {
    }

    /**
     * override this for proposals,
     *
     * @return log of Hastings Ratio, or Double.NEGATIVE_INFINITY if proposal should not be accepted *
     */
    @Override
    public double proposal() {
        final TreePoMo tree = (TreePoMo) InputUtil.get(treeInput, this);

        double logHastingsRatio = 0;

        if (isNarrowInput.get()) {
            logHastingsRatio = narrow(tree);
        } else {
            logHastingsRatio = wide(tree);
        }

        return logHastingsRatio;
    }

    private int isg(final NodePoMo n) {
      //return (n.getLeft().isLeaf() && n.getRight().isLeaf()) ? 0 : 1;
      boolean grandpa=false;
  		for (NodePoMo c:n.getChildren()){
  			if (!c.isLeaf()) {
  				grandpa=true;
  				break;
  			}
  		}
  		return (grandpa && (n.getChildren().size()>1))? 0 : 1;
  		
    }

//    private int sisg(final NodePoMo n) {
//        return n.isLeaf() ? 0 : isg(n);
//    }
    
    List<NodePoMo> getViableIterative(NodePoMo n){
    	boolean grandpa=false;
    	List<NodePoMo> viables= new ArrayList<NodePoMo>();
    	List<NodePoMo> newViables= new ArrayList<NodePoMo>();
    	for (NodePoMo c:n.getChildren()){
    		if (!c.isLeaf()) {
    			grandpa=true;
    			newViables=getViableIterative(c);
    			viables.addAll(newViables);
    		}
    	}
    	if (grandpa && n.getChildren().size()>1) viables.add(n);
    	return viables;
    }
    
    List<NodePoMo> getViable(NodePoMo n){
    	List<NodePoMo> viables= new ArrayList<NodePoMo>();
    	for (NodePoMo c:n.getChildren()){
    		if (!c.isLeaf()) {
    			viables.add(c);
    		}
    	}
    	return viables;
    }

    /**
     * WARNING: Assumes strictly bifurcating beast.tree.
     */
    public double narrow(final TreePoMo tree) {
    		System.out.println("Starting narrow");
        //if( true ) {
            //        Alternative implementation that has less risk of rejection due to
            //    	  selecting an invalid initial node
            //
            final int internalNodes = tree.getInternalNodeCount();
            //final int leafNodes = tree.getLeafNodeCount();
            if (internalNodes <= 1) {
            	System.out.println("Ending narrow, no internals");
                return Double.NEGATIVE_INFINITY;
            }
            List<NodePoMo> grandpas= new ArrayList<NodePoMo>();
            grandpas =getViableIterative(tree.getRoot());
            if (grandpas.size() < 1) {
            	System.out.println("Ending narrow, no grandpas");
                return Double.NEGATIVE_INFINITY;
            }
            //final int[] dummy = new int[1];
            //final String newick = tree.getRoot().toSortedNewick(new int[1]);
            System.out.println(tree.getRoot().toSortedNewick(new int[1]));
            System.out.println("grandpas size "+grandpas.size()+ " internal nodes "+internalNodes);
            int vIndex=Randomizer.nextInt(grandpas.size());
            NodePoMo grandParent = grandpas.get(vIndex);
            List<NodePoMo> viables= new ArrayList<NodePoMo>();
            viables= getViable(grandParent);
            System.out.println("viables size "+viables.size());
            vIndex=Randomizer.nextInt(viables.size());
            NodePoMo parentIndex = viables.get(vIndex);
            System.out.println("Parent "+parentIndex.getID());
            int vIndex2=Randomizer.nextInt(grandParent.getChildren().size());
            NodePoMo uncle = grandParent.getChildren().get(vIndex2);
            System.out.println("Uncle "+uncle.getID());
            while (uncle.getID()==parentIndex.getID()) {
            	vIndex2=Randomizer.nextInt(grandParent.getChildren().size());
            	uncle = grandParent.getChildren().get(vIndex2);
            	System.out.println("New uncle "+uncle.getID());
            }
            //if (vIndex2>=vIndex) vIndex2+=1;
            //NodePoMo uncle = grandParent.getChildren().get(vIndex2);
            if (parentIndex.getHeight() < uncle.getHeight()) {
            	if(viables.contains(uncle)){
            		parentIndex = grandParent.getChildren().get(vIndex2);
                    uncle = viables.get(vIndex);
            	}else{
            		System.out.println("Ending narrow (parent is younger than uncle)");
            		return Double.NEGATIVE_INFINITY;
            	}
            }
            int validGP = grandpas.size();
            final int c2 = isg(parentIndex) + isg(uncle);
            int ind = Randomizer.nextInt(parentIndex.getChildren().size());
            final NodePoMo i = parentIndex.getChildren().get(ind);
            boolean accepted=exchangeNodesDoable(i, uncle, parentIndex, grandParent);
            final int validGPafter = validGP - c2 + isg(parentIndex) + isg(uncle);
            grandParent.makeDirty(TreePoMo.IS_FILTHY);
            System.out.println("Toward End narrow with exchange accepted "+accepted);
            System.exit(0);
            if (!accepted) return Double.NEGATIVE_INFINITY;
            System.out.println("Ending narrow with exchange done!");
            System.exit(0);
            return Math.log((float)validGP/validGPafter);
            

            //NodePoMo grandParent = tree.getNode(internalNodes + 1 + Randomizer.nextInt(internalNodes));
            //while (grandParent.getLeft().isLeaf() && grandParent.getRight().isLeaf()) {
            //    grandParent = tree.getNode(internalNodes + 1 + Randomizer.nextInt(internalNodes));
            //}

//            NodePoMo parentIndex = grandParent.getLeft();
//            NodePoMo uncle = grandParent.getRight();
//            if (parentIndex.getHeight() < uncle.getHeight()) {
//                parentIndex = grandParent.getRight();
//                uncle = grandParent.getLeft();
//            }

//            if( parentIndex.isLeaf() ) {
//                // tree with dated tips
//                return Double.NEGATIVE_INFINITY;
//            }

//            int validGP = 0;
//            {
//                for(int i = internalNodes + 1; i < 1 + 2*internalNodes; ++i) {
//                    validGP += isg(tree.getNode(i));
//                }
//            }

            //final int c2 = sisg(parentIndex) + sisg(uncle);

            //final NodePoMo i = (Randomizer.nextBoolean() ? parentIndex.getLeft() : parentIndex.getRight());
            //exchangeNodes(i, uncle, parentIndex, grandParent);

//            final int validGPafter = validGP - c2 + sisg(parentIndex) + sisg(uncle);
//
//            return Math.log((float)validGP/validGPafter);
//        } else {
//
//            final int nodeCount = tree.getNodeCount();
//
//            NodePoMo i = tree.getRoot();
//
//            while (i.isRoot() || i.getParent().isRoot()) {
//                i = tree.getNode(Randomizer.nextInt(nodeCount));
//            }
//
//            final NodePoMo parentIndex = i.getParent();
//            final NodePoMo grandParent = parentIndex.getParent();
//            NodePoMo uncle = grandParent.getLeft();
//            if (uncle.getNr() == parentIndex.getNr()) {
//                uncle = grandParent.getRight();
//                assert (uncle.getNr() != parentIndex.getNr());
//            }
//            assert uncle == getOtherChild(grandParent, parentIndex);
//
//            assert i.getHeight() <= grandParent.getHeight();
//
//            if (//i.getHeight() < uncle.getHeight() &&
//                    uncle.getHeight() < parentIndex.getHeight()) {
//                exchangeNodes(i, uncle, parentIndex, grandParent);
//                return 0;
//            } else {
//                // Couldn't find valid narrow move on this beast.tree!!
//                return Double.NEGATIVE_INFINITY;
//            }
//        }
    }

    /**
     * WARNING: Assumes strictly bifurcating beast.tree.
     * @param tree
     */
    public double wide(final TreePoMo tree) {
    	System.out.println("Starting wide");
        final int nodeCount = tree.getNodeCount();

        NodePoMo i = tree.getRoot();

        while (i.isRoot()) {
            i = tree.getNode(Randomizer.nextInt(nodeCount));
        }

        NodePoMo j = i;
        while (j.getNr() == i.getNr() || j.isRoot()) {
            j = tree.getNode(Randomizer.nextInt(nodeCount));
        }

        final NodePoMo p = i.getParent();
        final NodePoMo jP = j.getParent();

        if ((p != jP) && (i != jP) && (j != p)
                && (j.getHeight() < p.getHeight())
                && (i.getHeight() < jP.getHeight())
//                && ((p.getHeight() < jP.getHeight() && i.getHeight() < j.getHeight()) ||
//                (p.getHeight() > jP.getHeight() && i.getHeight() > j.getHeight()))
                ) {
        	boolean done=exchangeNodesDoable(i, j, p, jP);
        	System.out.println("Ending wide");
        	if (!done) return Double.NEGATIVE_INFINITY;
        	else{
            

	            // All the nodes on the path from i/j to the common ancestor of i/j parents had a topology change,
	            // so they need to be marked FILTHY.
	            if( markCladesInput.get() ) {
	                NodePoMo iup = p;
	                NodePoMo jup = jP;
	                while (iup != jup) {
	                    if( iup.getHeight() < jup.getHeight() ) {
	                        assert !iup.isRoot();
	                        iup = iup.getParent();
	                        iup.makeDirty(TreePoMo.IS_FILTHY);
	                    } else {
	                        assert !jup.isRoot();
	                        jup = jup.getParent();
	                        jup.makeDirty(TreePoMo.IS_FILTHY);
	                    }
	                }
	            }
	            System.out.println("Ending wide");
	            return 0;
        	}
        }

        // Randomly selected nodes i and j are not valid candidates for a wide exchange.
        // reject instead of counting (like we do for narrow).
        System.out.println("Ending wide");
        return Double.NEGATIVE_INFINITY;
    }


    /* exchange sub-trees whose root are i and j */

    protected void exchangeNodes(NodePoMo i, NodePoMo j,
                                 NodePoMo p, NodePoMo jP) {
        // precondition p -> i & jP -> j
        replace(p, i, j);
        replace(jP, j, i);
        // postcondition p -> j & p -> i
    }
    
    protected boolean exchangeNodesDoable(NodePoMo child1, NodePoMo child2, NodePoMo parent1, NodePoMo parent2) {
    	if (child1.getHeight()<parent2.getEnd()) return false;
    	if (child1.getHeight()>parent2.getHeight()) return false;
    	if (child2.getHeight()<parent1.getEnd()) return false;
    	if (child2.getHeight()>parent1.getHeight()) return false;
    	// precondition p -> i & jP -> j
    	replace(parent1, child1, child2);
    	replace(parent2, child2, child1);
    	parent1.sortChildren();
    	parent2.sortChildren();
    	// postcondition p -> j & p -> i
    	return true;
    }
}
