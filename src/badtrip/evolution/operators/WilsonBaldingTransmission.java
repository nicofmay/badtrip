/*
* File WilsonBalding.java
*
* Copyright (C) 2010 Remco Bouckaert remco@cs.auckland.ac.nz
*
* This file is part of BEAST2.
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership and licensing.
*
* BEAST is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
*  BEAST is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with BEAST; if not, write to the
* Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
* Boston, MA  02110-1301  USA
*/
/*
 * WilsonBalding.java
 *
 * Copyright (C) 2002-2006 Alexei Drummond and Andrew Rambaut
 *
 * This file is part of BEAST.
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership and licensing.
 *
 * BEAST is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 *  BEAST is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with BEAST; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */

package badtrip.evolution.operators;

import badtrip.evolution.tree.NodePoMo;
import badtrip.evolution.tree.TreePoMo;
import beast.base.core.Description;
import beast.base.inference.util.InputUtil;
import beast.base.util.Randomizer;

import java.util.List;

/**
 * WILSON, I. J. and D. J. BALDING, 1998  Genealogical inference from microsatellite data.
 * Genetics 150:499-51
 * http://www.genetics.org/cgi/ijlink?linkType=ABST&journalCode=genetics&resid=150/1/499
 */
@Description("Implements a move similar to the one proposed by WILSON and BALDING 1998, just picking a random non-root node and assigning another compatible random parent.")
public class WilsonBaldingTransmission extends TreeOperatorTransmission {

    @Override
    public void initAndValidate() {
    }

    /**
     * override this for proposals,
     *
     * @return log of Hastings Ratio, or Double.NEGATIVE_INFINITY if proposal should not be accepted *
     */
    @Override
    public double proposal() {
    	//System.out.println("Start Operator WilsonBaldingTransmission ");
        TreePoMo tree = (TreePoMo) InputUtil.get(treeInput, this);

        // choose a random node avoiding root
        final int nodeCount = tree.getNodeCount();
        NodePoMo i;
        int nodeNr;
        List<NodePoMo> extras=tree.getExtraList();
        do {
        	//sample random node
        	nodeNr = Randomizer.nextInt(nodeCount);
            if (nodeNr>=extras.size()){
            	i=tree.getNodesAsArray()[nodeNr-extras.size()];
            }else{
            	i=extras.get(nodeNr);
            }
        } while (i.isRoot());
        final NodePoMo p = i.getParent();

        // choose another random node to insert i above
        NodePoMo j;
        //NodePoMo jP;

        // make sure that the target j is a possible parent
        do {
        	//sample random node
        	nodeNr = Randomizer.nextInt(nodeCount);
            if (nodeNr>=extras.size()){
            	j=tree.getNodesAsArray()[nodeNr-extras.size()];
            }else{
            	j=extras.get(nodeNr);
            }
            //j = tree.getNode(Randomizer.nextInt(nodeCount));
            //jP = j.getParent();
        } while (j.equals(i)||j.getHeight()<i.getHeight()||j.getEnd()>i.getHeight());
        
        p.removeChild(i);
        i.setParent(j);
        j.addChild(i);
        j.sortChildren();
        //System.out.println("Operator WilsonBaldingTransmission "+i.getID()+" "+j.getID()+" "+p.getID());
        return 0.0;
        
    }


} // class WilsonBalding
