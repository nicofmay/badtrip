/*
* File WilsonBalding.java
*
* Copyright (C) 2010 Remco Bouckaert remco@cs.auckland.ac.nz
*
* This file is part of BEAST2.
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership and licensing.
*
* BEAST is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
*  BEAST is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with BEAST; if not, write to the
* Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
* Boston, MA  02110-1301  USA
*/
/*
 * WilsonBalding.java
 *
 * Copyright (C) 2002-2006 Alexei Drummond and Andrew Rambaut
 *
 * This file is part of BEAST.
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership and licensing.
 *
 * BEAST is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 *  BEAST is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with BEAST; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */

package badtrip.evolution.operators;

import badtrip.evolution.tree.NodePoMo;
import badtrip.evolution.tree.TreePoMo;
import beast.base.core.Description;
import beast.base.inference.util.InputUtil;
import beast.base.util.Randomizer;

import java.util.ArrayList;
import java.util.List;

/**
 * WILSON, I. J. and D. J. BALDING, 1998  Genealogical inference from microsatellite data.
 * Genetics 150:499-51
 * http://www.genetics.org/cgi/ijlink?linkType=ABST&journalCode=genetics&resid=150/1/499
 */
@Description("Implements a move is similar to one proposed by WILSON and BALDING 1998, just picking a random non-root node and assigning another compatible random parent, but this time also select a new random infection time.")
public class WilsonBaldingNewHeight extends TreeOperatorTransmission {

    @Override
    public void initAndValidate() {
    }
    
    List<NodePoMo> getParentables(NodePoMo n, double height, String s){
    	List<NodePoMo> parentables=new ArrayList<NodePoMo>();
    	if(height<n.getHeight() && height>n.getEnd() && n.getID()!=s) parentables.add(n);
    	for (NodePoMo c :n.getChildren()){
    		parentables.addAll(getParentables(c, height, s));
    	}
    	return parentables;
    }

    /**
     * override this for proposals,
     *
     * @return log of Hastings Ratio, or Double.NEGATIVE_INFINITY if proposal should not be accepted *
     */
    @Override
    public double proposal() {
    	//System.out.println("Start Operator WilsonBaldingNewHeight ");
        TreePoMo tree = (TreePoMo) InputUtil.get(treeInput, this);
        //System.out.println("Starting Wilson Balding with new infection time ");
        // choose a random node avoiding root
        final int nodeCount = tree.getNodeCount();
        NodePoMo i;
        int nodeNr;
        List<NodePoMo> extras=tree.getExtraList();
        do {
        	//sample random node
        	nodeNr = Randomizer.nextInt(nodeCount);
            if (nodeNr>=extras.size()){
            	i=tree.getNodesAsArray()[nodeNr-extras.size()];
            }else{
            	i=extras.get(nodeNr);
            }
        } while (i.isRoot());
        final NodePoMo p = i.getParent();
        
        //sample new height
        double upper=i.getStart();
        upper = Math.min(i.getStart(),tree.getRoot().getHeight());
        //double upper = Math.min(node.getStart(),node.getParent().getHeight());
        double lower = i.getEnd();
        if (i.getChildren().size()>0) lower=Math.max(lower,i.getChildren().get(0).getHeight());
        if (i.getSamples().size()>0) lower=Math.max(lower,i.getSamples().get(0).getTime());
        List<NodePoMo> oldParentables=getParentables(tree.getRoot(), i.getHeight(), i.getID());
        
        final double newValue = (Randomizer.nextDouble() * (upper - lower)) + lower;
        
        List<NodePoMo> newParentables=getParentables(tree.getRoot(), newValue, i.getID());
        
        final int c2=oldParentables.size();
        final int c1=newParentables.size();
		if (c1<1) {
			//System.out.println("Ending proposal slide for lack of parentables ");
			//System.out.println("Operator WilsonBaldingNewHeight -infty");
			return Double.NEGATIVE_INFINITY;
		}

        // choose another random node to insert i above
        NodePoMo j=newParentables.get(Randomizer.nextInt(c1));
        
        i.setHeight(newValue);
        p.removeChild(i);
        i.setParent(j);
        j.addChild(i);
        j.sortChildren();
        //System.out.println("Set new parent ");
        //System.out.println("Operator WilsonBaldingNewHeight "+i.getID()+" "+j.getID()+" "+p.getID());
        return Math.log((((float)c1)/(float)(c2)));
        
    }


} // class WilsonBalding
