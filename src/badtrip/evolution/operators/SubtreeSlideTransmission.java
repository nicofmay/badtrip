/*
* File SubtreeSlide.java
*
* Copyright (C) 2010 Remco Bouckaert remco@cs.auckland.ac.nz
*
* This file is part of BEAST2.
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership and licensing.
*
* BEAST is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
*  BEAST is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with BEAST; if not, write to the
* Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
* Boston, MA  02110-1301  USA
*/
/*
 * SubtreeSlideOperator.java
 *
 * Copyright (C) 2002-2006 Alexei Drummond and Andrew Rambaut
 *
 * This file is part of BEAST.
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership and licensing.
 *
 * BEAST is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 *  BEAST is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with BEAST; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */

package badtrip.evolution.operators;


//import java.text.DecimalFormat;

import badtrip.evolution.tree.NodePoMo;
import badtrip.evolution.tree.TreePoMo;
import beast.base.core.Description;
import beast.base.inference.util.InputUtil;
import beast.base.util.Randomizer;

import java.util.ArrayList;
import java.util.List;


/**
 * Implements the subtree slide move.
 */
@Description("Moves the height of an internal node along the branch. A new parent is assigned in the neighborhood (granparent, parent, uncles, and siblings).")
public class SubtreeSlideTransmission extends TreeOperatorTransmission {

    @Override
    public void initAndValidate() {
        //size = sizeInput.get();
        //limit = limitInput.get();
    }

    /**
     * Do a probabilistic subtree slide move.
     *
     * @return log of Hastings Ratio, or Double.NEGATIVE_INFINITY if proposal should not be accepted *
     */
    @Override
    public double proposal() {
    	//System.out.println("Start Operator SubtreeSlideTransmission, ");
    	//System.out.println("Starting proposal slide");
        final TreePoMo tree = (TreePoMo) InputUtil.get(treeInput, this);
        final int nodeCount = tree.getNodeCount();
        NodePoMo node;
        if (nodeCount==0)System.out.println("What? there are no hosts in the tree!");
        
        //sample random node
        int nodeNr = Randomizer.nextInt(nodeCount);
        List<NodePoMo> extras=tree.getExtraList();
        if (nodeNr>=extras.size()){
        	node=tree.getNodesAsArray()[nodeNr-extras.size()];
        }else{
        	node=extras.get(nodeNr);
        }
        //System.out.println("Chosen node "+node.getID());
        final double value=node.getHeight();
        
        
        
        //define lists of relatives
        double upper=node.getStart();
        NodePoMo parent=null;
        NodePoMo gParent=null;
        List<NodePoMo> siblings= new ArrayList<NodePoMo>();
        List<NodePoMo> uncles= new ArrayList<NodePoMo>();
        boolean pIsRoot=false;
        boolean nIsRoot=node.isRoot();
        if (!nIsRoot){
        	parent=node.getParent();
        	pIsRoot=parent.isRoot();
        	if (!pIsRoot){
        		gParent=parent.getParent();
        		//upper=Math.min(upper,gParent.getHeight());
        		for (NodePoMo c : gParent.getChildren()){
            		if (!c.equals(parent)){
            			uncles.add(c);
            		}
            	}
        	}
        	for (NodePoMo c : parent.getChildren()){
        		if (!c.equals(node)){
        			siblings.add(c);
        		}
        	}
        }
        
        //improve lower and upper bounds
        double lower=node.getEnd();
        if (node.getSamples().size()>0) lower=Math.max(lower,node.getSamples().get(0).getTime());
        if (node.getChildren().size()>0) lower=Math.max(lower,node.getChildren().get(0).getHeight());
        final double interval1=upper-lower;
        
        
        final double newValue = (Randomizer.nextDouble() * (interval1)) + lower;
        node.setHeight(newValue);
        
        //System.out.println("new value "+newValue);
        
        //get parentables at new height
        List<NodePoMo> parentables= new ArrayList<NodePoMo>();
        if (nIsRoot) parentables.add(null);
        else{
	        if(newValue<parent.getHeight() && newValue>parent.getEnd()) parentables.add(parent);
	        //System.out.println("Got parent "+parent.getID());
	        if(!pIsRoot && newValue<gParent.getHeight() && newValue>gParent.getEnd()) parentables.add(gParent);
	        //System.out.println("Got gparent ");
			for (NodePoMo n : siblings){
				if(newValue<n.getHeight() && newValue>n.getEnd()) parentables.add(n);
			}
			//System.out.println("Got sibs ");
			for (NodePoMo n : uncles){
				if(newValue<n.getHeight() && newValue>n.getEnd()) parentables.add(n);
			}
			//System.out.println("Got uncles ");
        }
		final int c1=parentables.size();
		if (c1<1) {
			//System.out.println("Ending proposal slide for lack of parentables ");
			//System.out.println("Operator SubtreeSlideTransmission -infty");
			return Double.NEGATIVE_INFINITY;
		}
		
		//get new parent and make change
		nodeNr = Randomizer.nextInt(c1);
		NodePoMo newP = parentables.get(nodeNr);
		//if (newP!=null) System.out.println("Got new parent "+newP.getID());
		//else System.out.println("Remaining root ");
		if (newP!=null && !newP.equals(parent)){
			//System.out.println("Setting new parent");
			newP.addChild(node);
			parent.removeChild(node);
			node.setParent(newP);
		}
		if (newP!=null){
			newP.sortChildren();
		}
		//System.out.println("Set new parent ");
		
		//get new interval and number of parentables for Hastings ratio
        upper=node.getStart();
        siblings= new ArrayList<NodePoMo>();
        uncles= new ArrayList<NodePoMo>();
        pIsRoot=false;
        nIsRoot=node.isRoot();
        if (!nIsRoot){
        	parent=node.getParent();
        	pIsRoot=parent.isRoot();
        	if (!pIsRoot){
        		gParent=parent.getParent();
        		upper=Math.min(upper,gParent.getHeight());
        		for (NodePoMo c : gParent.getChildren()){
            		if (!c.equals(parent)){
            			uncles.add(c);
            		}
            	}
        	}
        	for (NodePoMo c : parent.getChildren()){
        		if (!c.equals(node)){
        			siblings.add(c);
        		}
        	}
        }
        final double interval2=upper-lower;
        //System.out.println("Got new interval and relatives ");
        
        
        //get parentables at old height
        parentables= new ArrayList<NodePoMo>();
        if (nIsRoot) parentables.add(null);
        else{
	        if(value<parent.getHeight() && value>parent.getEnd()) parentables.add(parent);
	        //System.out.println("Got parent "+parent.getID());
	        if(!pIsRoot && value<gParent.getHeight() && value>gParent.getEnd()) parentables.add(gParent);
	        //System.out.println("Got gparent ");
			for (NodePoMo n : siblings){
				if(value<n.getHeight() && value>n.getEnd()) parentables.add(n);
			}
			//System.out.println("Got sibs ");
			for (NodePoMo n : uncles){
				if(value<n.getHeight() && value>n.getEnd()) parentables.add(n);
			}
			//System.out.println("Got uncles ");
        }
		final int c2=parentables.size();
		
		//System.out.println("Ending proposal slide"+Math.log((float)(1.0/interval2)*(1.0/(float)(c2))*((float)c1)*interval1));
		//System.out.println("Operator SubtreeSlideTransmission, finite "+node.getID());
		//if (newP!=null) System.out.println(newP.getID());
		return Math.log((float)(1.0/interval2)*(1.0/(float)(c2))*((float)c1)*interval1);
        
        
//        //improve lower and upper bounds
//        double lower=node.getEnd();
//        if (node.getSamples().size()>0) lower=Math.max(lower,node.getSamples().get(0).getTime());
//        if (node.getChildren().size()>0) {
//        	child=node.getChildren().get(0);
//        	lower=Math.max(lower,child.getEnd());
//        	if (child.getSamples().size()>0) lower=Math.max(lower,child.getSamples().get(0).getTime());
//        	if (child.getChildren().size()>0) lower=Math.max(lower,child.getChildren().get(0).getHeight());
//        }
//        if (node.getChildren().size()>1) {
//        	lower=Math.max(lower,node.getChildren().get(1).getHeight());
//        }
//        final double newValue = (Randomizer.nextDouble() * (upper - lower)) + lower;
//        node.setHeight(newValue);
        
        
//        //get current number of parentables
//        List<NodePoMo> parentables= new ArrayList<NodePoMo>();
//        final int c1=1;
//		//if(child.getHeight()>parent.getEnd())parentables.add(parent);
//		for (NodePoMo n : siblings){
//			if(value<n.getHeight() && value>n.getEnd()) c1+=1;
//		}
//		for (NodePoMo n : uncles){
//			if(value<n.getHeight() && value>n.getEnd()) parentables.add(n);
//		}
//        
//        //case in which height goes down below child's height (and so become child's child)
//        if (node.getChildren().size()>0 && newValue<child.getHeight()){
//        	if (newValue<child.getEnd()) return Double.NEGATIVE_INFINITY;
//        	node.removeChild(child);
//        	if (node.isRoot()){
//        		child.setParent(parent);
//        	}else{
//        		List<NodePoMo> parentables= new ArrayList<NodePoMo>();
//        		if(child.getHeight()>parent.getEnd())parentables.add(parent);
//        		for (NodePoMo n : siblings){
//        			if(child.getHeight()<n.getHeight() && child.getHeight()>n.getEnd()) parentables.add(n);
//        		}
//        		for (NodePoMo n : uncles){
//        			if(child.getHeight()<n.getHeight() && child.getHeight()>n.getEnd()) parentables.add(n);
//        		}
//        		if (parentables.size()==0) return Double.NEGATIVE_INFINITY;
//        		nodeNr = Randomizer.nextInt(parentables.size());
//        		NodePoMo newParent=parentables.get(nodeNr);
//        		child.setParent(newParent);
//        		newParent.addChild(child);
//        		newParent.sortChildren();
//        		parent.removeChild(node);
//            	
//        	}
//        	child.addChild(node);
//        	child.sortChildren();
//        	node.setParent(child);
//        }
//        
//      //case in which height goes up above parent's height (and so become parent's parent)
//        if (!node.isRoot() && newValue>parent.getHeight()){
//        	if (parent.isRoot()){
//        		node.setParent(null);
//        	}else{
//        		List<NodePoMo> parentables= new ArrayList<NodePoMo>();
//        		parentables.add(gParent);
//        		for (NodePoMo n : uncles){
//        			if(newValue<n.getHeight() && newValue>n.getEnd()) parentables.add(n);
//        		}
//        		if (parentables.size()==0) return Double.NEGATIVE_INFINITY;
//        		nodeNr = Randomizer.nextInt(parentables.size());
//        		NodePoMo newParent=parentables.get(nodeNr);
//        		node.setParent(newParent);
//        		newParent.addChild(node);
//        		newParent.sortChildren();
//        	}
//        	node.addChild(parent);
//        	node.sortChildren();
//        	parent.removeChild(node);
//        	parent.setParent(node);
//        }
        
        
       
    }





}
