/*
* File TreeLikelihood.java
*
* Copyright (C) 2010 Remco Bouckaert remco@cs.auckland.ac.nz
*
* This file is part of BEAST2.
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership and licensing.
*
* BEAST is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
*  BEAST is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with BEAST; if not, write to the
* Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
* Boston, MA  02110-1301  USA
*/


package badtrip.evolution.likelihood;

import badtrip.evolution.alignment.AlignmentPoMo;
import badtrip.evolution.datatype.DataTypePoMo;
import badtrip.evolution.datatype.Sample;
import badtrip.evolution.sitemodel.SiteModelInterfacePoMo;
import badtrip.evolution.sitemodel.SiteModelPoMo;
import badtrip.evolution.substitutionmodel.PoMoGeneral;
import badtrip.evolution.tree.NodePoMo;
import badtrip.evolution.tree.TreePoMo;
import beast.base.core.Description;
import beast.base.core.Input;
import beast.base.core.Input.Validate;
import beast.base.core.Log;
import beast.base.evolution.sitemodel.SiteModelInterface;
import beast.base.evolution.substitutionmodel.DefaultEigenSystem;
import beast.base.evolution.substitutionmodel.EigenDecomposition;
import beast.base.evolution.substitutionmodel.EigenSystem;
import beast.base.inference.Distribution;
import beast.base.inference.State;
import beast.base.inference.parameter.RealParameter;
import beast.base.util.Randomizer;

import java.lang.reflect.Constructor;
import java.util.*;


@Description("Calculates the probability of deep sequencing data on a PoMo transmission tree given a site and PoMo substitution model. This slow version always recalculates everything at each step.")
public class TreeLikelihoodPoMoTransmissionSlow extends Distribution {
    
    final public Input<AlignmentPoMo> dataInput = new Input<>("data", "sequence PoMo data", Validate.REQUIRED);

    final public Input<TreePoMo> treePoMoInput = new Input<>("treePoMo", "phylogenetic transmission PoMo tree with sequence data in the leafs and internal nodes", Validate.REQUIRED);

    final public Input<SiteModelInterface> siteModelInput = new Input<>("siteModel", "site model for leafs in the beast.tree", Validate.REQUIRED);
    
    public Input<String> eigenSystemClass = new Input<String>("eigenSystem", "Name of the class used for creating an EigenSystem", DefaultEigenSystem.class.getName());

    public Input<Boolean> m_useAmbiguities = new Input<Boolean>("useAmbiguities", "flag to indicate leafs that sites containing ambigue states should be handled instead of ignored (the default)", false);
    
    public Input<RealParameter> seqErrorInput = new Input<RealParameter>("seqError", " starting sequencing error rate, 0 by default", Validate.REQUIRED);
    
	public Input<RealParameter> bottleneckInput = new Input<RealParameter>("bottleneck", "parameter describing how many Ne generiations the bottleneck corresponds to in terms of drift", Validate.REQUIRED);
    
    enum Scaling {none, always, _default};
    public Input<Scaling> scaling = new Input<TreeLikelihoodPoMoTransmissionSlow.Scaling>("scaling", "type of scaling to use, one of " + Arrays.toString(Scaling.values()) + ". If not specified, the -beagle_scaling flag is used.", Scaling._default, Scaling.values());

    /**
     * calculation engine *
     */
    protected BeerLikelihoodCorePoMo likelihoodCore;
    BeagleTreeLikelihoodPoMo beagle;
    
    RealParameter seqErr, bottle;
    double sErrStored, sErr;
    double bottleneck,bottleneckStored;
    protected double[][] partialsTaxa, partialsTaxaStored;
    AlignmentPoMo alignment;
    int nStateCount, nPatterns;

    /**
     * Plugin associated with inputs. Since none of the inputs are StateNodes, it
     * is safe to link to them only once, during initAndValidate.
     */
    PoMoGeneral substitutionModel;
    protected SiteModelPoMo.Base m_siteModel;
    protected EigenDecomposition eigenDecompositionBottleneck;
    //private EigenDecomposition storedEigenDecompositionBottleneck;
    protected EigenSystem eigenSystemBottleneck;
    protected double[][] rateMatrixBottleneck;
    
    /**
     * create an EigenSystem of the class indicated by the eigenSystemClass input *
     */
    protected EigenSystem createEigenSystem() throws Exception {
        Constructor<?>[] ctors = Class.forName(eigenSystemClass.get()).getDeclaredConstructors();
        Constructor<?> ctor = null;
        for (int i = 0; i < ctors.length; i++) {
            ctor = ctors[i];
            if (ctor.getGenericParameterTypes().length == 1)
                break;
        }
        ctor.setAccessible(true);
        return (EigenSystem) ctor.newInstance(nStateCount);
    }

    /**
     * flag to indicate the
     * // when CLEAN=0, nothing needs to be recalculated for the node
     * // when DIRTY=1 indicates a node partial needs to be recalculated
     * // when FILTHY=2 indicates the indices for the node need to be recalculated
     * // (often not necessary while node partial recalculation is required)
     */
    //protected int hasDirt;

    /**
     * Lengths of the branches in the tree associated with each of the nodes
     * in the tree through their node  numbers. By comparing whether the
     * current branch length differs from stored branch lengths, it is tested
     * whether a node is dirty and needs to be recomputed (there may be other
     * reasons as well...).
     * These lengths take branch rate models in account.
     */
    //protected double[][] m_branchLengths;
    //protected double[][] storedBranchLengths;

    /**
     * memory allocation for likelihoods for each of the patterns *
     */
    double[] patternLogLikelihoods;
    /**
     * memory allocation for the root partials *
     */
    double[] m_fRootPartials;
    /**
     * memory allocation for probability tables obtained from the SiteModel *
     */
    double[] probabilities;
    //transition probabilities for bottleneck: just one set for the whole tree
    double[] probabilitiesBottleneck, probabilitiesBottleneckStored;

    int matrixSize;

    /**
     * flag to indicate ascertainment correction should be applied *
     */
    boolean useAscertainedSitePatterns = false;

    /**
     * dealing with proportion of site being invariant *
     */
    double proportionInvariant = 0;
    List<Integer> constantPattern = null;
    

    @Override
    public void initAndValidate() throws IllegalArgumentException {
    	
    	seqErr = seqErrorInput.get();
    	bottle=bottleneckInput.get();
    	bottleneck=bottle.getValue();
    	bottleneckStored=bottle.getValue();
		//NICOLA TODO : try to see if you can use beagle.
        beagle = null;
//        beagle = new BeagleTreeLikelihoodPoMoTransmission();
//        try {
//	        beagle.initByName(
//                    "data", dataInput.get(), "tree", treeInput.get(), "siteModel", ((SiteModelPoMo) siteModelInput.get()),
//                    "branchRateModel", null , "useAmbiguities", m_useAmbiguities.get(),
//                    "scaling", scaling.get().toString());//branchRateModelInput.get()
//	        if (beagle.beagle != null) {
//	            //a Beagle instance was found, so we use it
//	        	System.out.println("Used beagle\n");
//	            return;
//	        }
//        } catch (Exception e) {
//        	System.out.println("Not using Beagle\n");
//			// ignore
//		}
//        // No Beagle instance was found, so we use the good old java likelihood core
//        beagle = null;
        
        //int nodeCount = treePoMoInput.get().getNodeCount();
        if (!((siteModelInput.get()) instanceof SiteModelPoMo.Base)) {
        	throw new IllegalArgumentException("siteModel input should be of type SiteModel.Base");
        }
        m_siteModel = (SiteModelInterfacePoMo.Base) siteModelInput.get();
        m_siteModel.setDataType((DataTypePoMo)(dataInput.get().getDataType()));
        substitutionModel = m_siteModel.substModelInput.get();
        nStateCount=substitutionModel.getStateCount();
        
        rateMatrixBottleneck = new double[nStateCount][nStateCount];
	    rateMatrixBottleneck = getRatesBottleneck(substitutionModel.getPopSize());
	    
	    try {
			eigenSystemBottleneck = createEigenSystem();
		} catch (Exception e) {
			e.printStackTrace();
		}
	    
	    eigenDecompositionBottleneck = eigenSystemBottleneck.decomposeMatrix(rateMatrixBottleneck);

        //m_branchLengths = new double[nodeCount][];
        //storedBranchLengths = new double[nodeCount][];

        //nStateCount = dataInput.get().getMaxStateCount();
        //nStateCount=substitutionModel.getStateCount();
        nPatterns = dataInput.get().getPatternCount();
        likelihoodCore = new BeerLikelihoodCorePoMo(nStateCount);

        String className = getClass().getSimpleName();

        alignment = (AlignmentPoMo) dataInput.get();

        Log.info.println(className + "(" + getID() + ") uses " + likelihoodCore.getClass().getSimpleName());
        Log.info.println("  " + alignment.toString(true));

        proportionInvariant = m_siteModel.getProportionInvariant();
        m_siteModel.setPropInvariantIsCategory(false);
        if (proportionInvariant > 0) {
            calcConstantPatternIndices(nPatterns, nStateCount);
        }

        patternLogLikelihoods = new double[nPatterns];
        m_fRootPartials = new double[nPatterns * nStateCount];
        matrixSize = (nStateCount ) * (nStateCount );
        probabilities = new double[(nStateCount ) * (nStateCount )];
        Arrays.fill(probabilities, 1.0);
        probabilitiesBottleneck = new double[(nStateCount ) * (nStateCount )];
        Arrays.fill(probabilitiesBottleneck, 1.0);
        getTransitionProbabilitiesBottleneck(probabilitiesBottleneck,bottleneck);
        probabilitiesBottleneckStored=probabilitiesBottleneck.clone();
        
        
        sErr=seqErr.getValue();
        sErrStored=seqErr.getValue();
        int nTaxa=alignment.getTaxonCount();
        partialsTaxa = new double[nTaxa][nPatterns * nStateCount];
        partialsTaxaStored = new double[nTaxa][nPatterns * nStateCount];
        double[] currPartials;//= new double[nStateCount];
        List<String> tNames = alignment.getTaxaNames();
        for (int t=0; t<nTaxa; t++){
        	currPartials=getPartials(alignment.getTaxonIndex(tNames.get(t)), nPatterns);
        	//System.out.println(nStateCount+" "+nPatterns+" "+partialsTaxa[t].length+" "+partialsTaxaStored[t].length+" "+currPartials.length);
        	System.arraycopy(currPartials, 0, partialsTaxa[t], 0, currPartials.length);
    		System.arraycopy(currPartials, 0, partialsTaxaStored[t], 0, currPartials.length);
        }
        

        if (dataInput.get().isAscertained) {
            useAscertainedSitePatterns = true;
        }
        
        initCore();
        
    }


    /**
     * Determine indices of m_fRootProbabilities that need to be updates
     * // due to sites being invariant. If none of the sites are invariant,
     * // the 'site invariant' category does not contribute anything to the
     * // root probability. If the site IS invariant for a certain character,
     * // taking ambiguities in account, there is a contribution of 1 from
     * // the 'site invariant' category.
     */
    void calcConstantPatternIndices(final int nPatterns, final int nStateCount) {
        constantPattern = new ArrayList<Integer>();
        for (int i = 0; i < nPatterns; i++) {
           // final int[][] pattern = dataInput.get().getPatternPoMo(i);
            final boolean[] bIsInvariant = new boolean[nStateCount];
            Arrays.fill(bIsInvariant, true);
            for (int k = 0; k < nStateCount; k++) {
                if (bIsInvariant[k]) {
                    constantPattern.add(i * nStateCount + k);
                }
            }
        }
    }

    void initCore() {
    	//System.out.println("InitCore() ");
    	TreePoMo treePoMo=treePoMoInput.get();
    	//TODO: move to later (calculateLogP) to account for new nodes added to the tree
        final int nodeCount = treePoMo.getNodeCount();
        likelihoodCore.initialize(
                nodeCount,
                dataInput.get().getPatternCount(),
                m_siteModel.getCategoryCount(),
                true, m_useAmbiguities.get()
        );

        //final int extNodeCount = treePoMo.getLeafNodeCount();
        //final int intNodeCount = treePoMo.getInternalNodeCount();
        
        //System.out.println("Setting node Matrices ");
        //setNodeMatrices(treePoMo.getRoot());
        
        //System.out.println("Creating node partials ");
        //for (int i = 0; i < intNodeCount+extNodeCount; i++) {
        //    likelihoodCore.createNodePartials(i);//extNodeCount +
        //}
        
        //System.out.println("Setting node Partials");
        //setPartials(treePoMo.getRoot(), dataInput.get().getPatternCount());
        
        //hasDirt = TreePoMo.IS_FILTHY;
        //System.out.println("End InitCore() ");
    }

    /**
     * This method samples the sequences based on the tree and site model.
     */
    public void sample(State state, Random random) {
        throw new UnsupportedOperationException("Can't sample a fixed alignment!");
    }

    /**
     * set leaf states in likelihood core *
     */
    void setStates(NodePoMo node, int patternCount) {
    	System.out.println("Warning, setting states in PoMo, no reason to.");
    }

    protected int getTaxonNr(NodePoMo node, int sampleIndex, AlignmentPoMo data) {
        int iTaxon = data.getTaxonIndex(node.getSample(sampleIndex).getTaxon().getID());
        if (iTaxon == -1) {
        	if (node.getID().startsWith("'") || node.getID().startsWith("\"")) {
        		iTaxon = data.getTaxonIndex(node.getSample(sampleIndex).getTaxon().getID().substring(1,node.getID().length()-1));
        	}
            if (iTaxon == -1) {
            	throw new RuntimeException("Could not find sequence " + node.getSample(sampleIndex).getTaxon().getID() + " in the alignment");
            }
        }
        return iTaxon;
	}

//	/**
//     * set leaf partials in likelihood core *
//     */
//    //most important modified bit - Nicola
//    void setPartials(NodePoMo node, int patternCount) {
//    	final ArrayList<Integer> childNums = new ArrayList<Integer>();
//    	for (NodePoMo c : node.getChildren()){
//    		setPartials(c, patternCount);
//    		childNums.add(c.getNr());
//    	}
//        if (node.isLeaf()) {
//        	double[] partials=getPartialsList(node.getSamples(), node.getNr(), node.getHeight(), patternCount);
//            likelihoodCore.setNodePartials(node.getNr(), partials);
//        } else {
//        	ArrayList<double[]> partials = new ArrayList<double[]>();
//            for (int i=0;i<childNums.size();i++){
//            	double[] partial = new double[likelihoodCore.partialsSize];
//            	likelihoodCore.getPartials(childNums.get(i), partial);
//            	partials.add(partial);
//            }
//        	double[] newPartials=getPartialsInternal(node, node.getSamples(), partials, node.getNr(), patternCount);
//        	likelihoodCore.setNodePartials(node.getNr(), newPartials);
//        }
//    }
    
    /**
     * calculate partials *
     */
    double[] getPartialsIterative(NodePoMo node, int patternCount) {
        	ArrayList<double[]> partials = new ArrayList<double[]>();
        	double[] partial;
            for (NodePoMo c : node.getChildren()){
            	partial =getPartialsIterative(c, patternCount);
            	partials.add(partial);
            }
            //partial=getPartialsNode(node, partials, patternCount);//, node.getNr(), node.getSamples()
            //System.out.println("getPartialsIterative, host "+node.hostName+" partials ");
            //for(int i=0;i<likelihoodCore.partialsSize;i++){// /patternCount
            //	System.out.println(partial[i]);
            //}
        	return getPartialsNode(node, partials, patternCount); //partial;
    }
    
    double[] getPartialsNode(NodePoMo node, ArrayList<double[]> partials, int patternCount){//, int nodeIndex, List<Sample> samples
    	List<Sample> samples=node.getSamples();
    	int nSamples=samples.size();
        int nChildren=node.getChildren().size();
    	//System.out.println("Start getPartialsNode ");
    	//final int iNode = node.getNr();
        int intervals=nChildren+nSamples;
        if (intervals==0){
        	double[] partial1=new double[likelihoodCore.partialsSize];
        	Arrays.fill(partial1, 1.0);
        	return partial1;
        }
        double[] branchTimes=new double[intervals];
        double currentTime=Double.MAX_VALUE;
        int is=0,ic=0;
        if (nChildren>0 &&(nSamples==0 || (node.getChild(nChildren-1).getHeight()<node.getSample(nSamples-1).getTime()))){
        	currentTime=node.getChild(nChildren-1).getHeight();
        	ic+=1;
        } else if (nSamples>0){
        	currentTime=node.getSample(nSamples-1).getTime();
        	is+=1;
        }
        for (;(ic+is)<intervals;){
        	if (ic<nChildren && (is>=nSamples || node.getChild(nChildren-(ic+1)).getHeight()<node.getSample(nSamples-(1+is)).getTime())){
        		branchTimes[ic+is-1]=node.getChild(nChildren-(ic+1)).getHeight()-currentTime;
        		currentTime=node.getChild(nChildren-(ic+1)).getHeight();
        		ic+=1;
        	}else if (is<nSamples){
        		branchTimes[ic+is-1]=node.getSample(nSamples-(1+is)).getTime() -currentTime;
        		currentTime=node.getSample(nSamples-(1+is)).getTime();
        		is+=1;
        	}
        }
        if(intervals>0){
        	branchTimes[intervals-1]=node.getHeight()-currentTime;
        }
    	double[] partial1=new double[likelihoodCore.partialsSize];
    	double[] matrix = new double[likelihoodCore.matrixSize];
    	double[] newPartials=new double[likelihoodCore.partialsSize];
    	//System.out.println("Got branch lengths ");
    	//double[][] matrices= new double[likelihoodCore.nrOfMatrices][likelihoodCore.matrixSize];
    	//double[] ps;
    	is=0;
    	ic=0;
    	double sum;
    	//int intervals=partials.size()+samples.size();
        if (nSamples==0 || (partials.size()>0 && (node.getChild(partials.size()-1).getHeight()<samples.get(nSamples-1).getTime()))){
        	partial1=partials.get(partials.size()-1);
        	//System.arraycopy( ps, 0, partial1, 0, ps.length );
        	sum=0.0;
        	for (int i=0; i<partial1.length;i++) sum+=partial1[i];
        	if (sum==0.0){
        		System.out.println("Child height "+node.getChild(partials.size()-1).getHeight());
        		System.out.println(this.treePoMoInput.get().getRoot().toSortedNewick(new int[1]));
        		System.out.println("getPartialsNode(), got zero sum partials at node "+node.getID()+" after child "+ic+", first step");
        		//for (int i=0; i<partial1.length;i++)System.out.println(partial1[i]);
        		//System.exit(0);
        	}
        	ic+=1;
        } else if (nSamples>0){
        	System.arraycopy(partialsTaxa[alignment.getTaxonIndex(samples.get(nSamples-1).getTaxon().getID())], 0, partial1, 0, patternCount*nStateCount);
        	//partial1=getPartials(alignment.getTaxonIndex(samples.get(samples.size()-1).getTaxon().getID()), patternCount);//, samples.size()-1
        	//System.arraycopy( ps, 0, partial1, 0, ps.length );
        	sum=0.0;
        	for (int i=0; i<partial1.length;i++) sum+=partial1[i];
        	if (sum==0.0){
        		System.out.println(this.treePoMoInput.get().getRoot().toSortedNewick(new int[1]));
        		System.out.println("getPartialsNode(), got zero sum partials at node "+node.getID()+" after sample "+is+", first step");
        		//for (int i=0; i<partial1.length;i++)System.out.println(partial1[i]);
        		//System.exit(0);
        	}
        	is+=1;
        }else{
        	System.out.println("Got node without samples or children, but escaped first test, what is the problem? ");
        	System.exit(0);
        }
        if (m_siteModel.getCategoryCount()>1) {
        	System.out.println("no multiple categories supported!");
        	System.exit(0);
        }
        //System.out.println("Got first step ");
        final double jointBranchRate = m_siteModel.getRateForCategory(0, null);
        for (;(ic+is)<intervals;){
        	//for (int i2 = 0; i2 < m_siteModel.getCategoryCount(); i2++) {	
            substitutionModel.getTransitionProbabilities(branchTimes[ic+is-1], jointBranchRate, matrix);
                //System.out.println("Substitution probabilities, category "+i2+" interval "+i+" node "+iNode+" length "+probabilities.length);
                //for (int i3=0; i3<probabilities.length;i3++) System.out.println(probabilities[i3]); //////////////////////////////////////////
                //likelihoodCore.setNodeMatrix(iNode, i2, i, intervals, probabilities);
            //}
        	//likelihoodCore.getNodeMatrices(nodeIndex, ic+is-1, matrix);
        	if (ic<partials.size() && (is>=nSamples || node.getChild(ic).getHeight()<samples.get(is).getTime())){
        		likelihoodCore.calculatePartialsFixedPruning(partial1, matrix, partials.get(partials.size()-(ic+1)), newPartials);
        		System.arraycopy( newPartials, 0, partial1, 0, newPartials.length );
        		sum=0.0;
            	for (int i=0; i<newPartials.length;i++) sum+=newPartials[i];
            	if (sum==0.0){
            		System.out.println(this.treePoMoInput.get().getRoot().toSortedNewick(new int[1]));
            		System.out.println("Child height "+node.getChild(partials.size()-(ic+1)).getHeight()+" child "+node.getChild(partials.size()-(ic+1)).getID());
            		//System.out.println("Transition matrix ");
            		//for (int i=0; i<matrix.length;i++)System.out.println(matrix[i]);
            		System.out.println("Branch length "+branchTimes[ic+is-1]);
            		System.out.println("getPartialsNode(), got zero sum partials at node "+node.getID()+" after child "+ic);
            		//for (int i=0; i<newPartials.length;i++)System.out.println(newPartials[i]);
            		//System.exit(0);
            	}
        		ic+=1;
        	}else if (is<nSamples){
        		double[] part = new double[patternCount*nStateCount];
        		System.arraycopy(partialsTaxa[alignment.getTaxonIndex(samples.get(nSamples-(1+is)).getTaxon().getID())], 0, part, 0, patternCount*nStateCount);
        		likelihoodCore.calculatePartialsFixedPruning(partial1, matrix, part, newPartials);//, samples.size()-(1+is)  
        		System.arraycopy( newPartials, 0, partial1, 0, newPartials.length );
        		sum=0.0;
            	for (int i=0; i<newPartials.length;i++) sum+=newPartials[i];
            	if (sum==0.0){
            		System.out.println(this.treePoMoInput.get().getRoot().toSortedNewick(new int[1]));
            		System.out.println("Sample height "+node.getSample(nSamples-(1+is)).getTime());
            		//System.out.println("Transition matrix ");
            		//for (int i=0; i<matrix.length;i++)System.out.println(matrix[i]);
            		System.out.println("Branch length "+branchTimes[ic+is-1]);
            		System.out.println("getPartialsNode(), got zero sum partials at node "+node.getID()+" after sample "+is);
            		//for (int i=0; i<newPartials.length;i++)System.out.println(newPartials[i]);
            		//System.exit(0);
            	}
        		is+=1;
        	}
        }
        //System.out.println("Got for ");
        substitutionModel.getTransitionProbabilities(branchTimes[intervals-1], jointBranchRate, matrix);
        //likelihoodCore.getNodeMatrices(nodeIndex, intervals-1, matrix);
    	likelihoodCore.calculatePartialsPruning(partial1, matrix, newPartials);
    	System.arraycopy( newPartials, 0, partial1, 0, newPartials.length );
    	sum=0.0;
    	for (int i=0; i<newPartials.length;i++) sum+=newPartials[i];
    	if (sum==0.0){
    		System.out.println(this.treePoMoInput.get().getRoot().toSortedNewick(new int[1]));
    		System.out.println("getPartialsNode(), got zero sum partials at node "+node.getID()+" right before bottleneck ");
    		//System.exit(0);
    	}
    	likelihoodCore.calculatePartialsPruning(partial1, probabilitiesBottleneck ,newPartials);
    	sum=0.0;
    	for (int i=0; i<newPartials.length;i++) sum+=newPartials[i];
    	if (sum==0.0){
    		System.out.println(this.treePoMoInput.get().getRoot().toSortedNewick(new int[1]));
    		System.out.println("End getPartialsNode(), got zero sum partials at node "+node.getID()+" end ");
    		//System.exit(0);
    	}
    	//System.out.println("End getPartialsNode() ");
    	return newPartials;
    }
    

    
//    /**
//     * set leaf partials in likelihood core *
//     */
//    boolean setCurrentPartials(NodePoMo node, int patternCount) {
//    	final ArrayList<Integer> childNums = new ArrayList<Integer>();
//    	boolean clean=true;
//    	for (NodePoMo c : node.getChildren()){
//    		if (!setCurrentPartials(c, patternCount)) clean=false;
//    		childNums.add(c.getNr());
//    	}
//    	if (node.isDirty()!=TreePoMo.IS_CLEAN||hasDirt!=TreePoMo.IS_CLEAN) clean=false;
//        if (node.isLeaf() && (!clean)) {
//        	likelihoodCore.setNodePartialsForUpdate(node.getNr());
//            double[] partials=getPartialsList(node.getSamples(), node.getNr(), node.getHeight(), patternCount);
//            likelihoodCore.setCurrentNodePartials(node.getNr(), partials);
//        } else if ((!node.isLeaf()) && (!clean)) {
//        	likelihoodCore.setNodePartialsForUpdate(node.getNr());
//        	ArrayList<double[]> partials = new ArrayList<double[]>();
//            for (int i=0;i<childNums.size();i++){
//            	double[] partial = new double[likelihoodCore.partialsSize];
//            	likelihoodCore.getPartials(childNums.get(i), partial);
//            	partials.add(partial);
//            }
//        	double[] newPartials=getPartialsInternal(node, node.getSamples(), partials, node.getNr(), patternCount);
//        	likelihoodCore.setCurrentNodePartials(node.getNr(), newPartials);
//        }
//        return clean;
//    }

    //travel through samples from a leaf node to get to the partial at its colonization time.
//    double[] getPartialsList(List<Sample> samples, int nodeIndex, double height, int patternCount){
//    	double[] partials=new double[likelihoodCore.partialsSize];
//    	double[] ps;
//    	if (samples.size()>0){
//    		ps=getPartials(samples.get(samples.size()-1).getTaxon(), samples.size()-1, patternCount);
//    		System.arraycopy( ps, 0, partials, 0, ps.length );
//    	}else{
//    		Arrays.fill(partials, 1.0);
//    		return partials;
//    	}
//    	double[] newPartials=new double[likelihoodCore.partialsSize];
//    	double[] matrix = new double[likelihoodCore.matrixSize];
//    	for (int i=0;i<samples.size()-1;i++){
//    		likelihoodCore.getNodeMatrices(nodeIndex, i, matrix);
//    		for (int i2 = 0; i2 < m_siteModel.getCategoryCount(); i2++) {	
//                final double jointBranchRate = m_siteModel.getRateForCategory(i2, null) * branchRate;
//                substitutionModel.getTransitionProbabilities(branchTimes[i], jointBranchRate, probabilities);
//                //System.out.println("Substitution probabilities, category "+i2+" interval "+i+" node "+iNode+" length "+probabilities.length);
//                //for (int i3=0; i3<probabilities.length;i3++) System.out.println(probabilities[i3]); //////////////////////////////////////////
//                likelihoodCore.setNodeMatrix(iNode, i2, i, intervals, probabilities);
//            }
//    		likelihoodCore.calculatePartialsFixedPruning(partials, matrix, getPartials(samples.get(samples.size()-(2+i)).getTaxon(), samples.size()-(2+i), patternCount), newPartials);
//    		System.arraycopy( newPartials, 0, partials, 0, newPartials.length );
//    	}
//    	likelihoodCore.getNodeMatrices(nodeIndex, samples.size()-1, matrix);
//    	likelihoodCore.calculatePartialsPruning(partials, matrix, newPartials);
//    	System.arraycopy( newPartials, 0, partials, 0, newPartials.length );
//    	likelihoodCore.calculatePartialsPruning(partials, probabilitiesBottleneck ,newPartials);
//    	return newPartials;
//    }
    
    
  //travel through samples from an internal node to get to the final partial at its colonization time.
//    double[] getPartialsInternal(NodePoMo node, List<Sample> samples, ArrayList<double[]> partials, int nodeIndex, int patternCount){
//    	double[] partial1=new double[likelihoodCore.partialsSize];
//    	double[] matrix = new double[likelihoodCore.matrixSize];
//    	double[] newPartials=new double[likelihoodCore.partialsSize];
//    	double[] ps;
//    	int is=0,ic=0;
//    	int intervals=partials.size()+samples.size();
//        if (samples.size()==0 || (node.getChild(partials.size()-1).getHeight()<samples.get(samples.size()-1).getTime())){
//        	ps=partials.get(partials.size()-1);
//        	System.arraycopy( ps, 0, partial1, 0, ps.length );
//        	ic+=1;
//        } else if (node.getSamples().size()>0){
//        	ps=getPartials(samples.get(samples.size()-1).getTaxon(), samples.size()-1, patternCount);
//        	System.arraycopy( ps, 0, partial1, 0, ps.length );
//        	is+=1;
//        }
//        for (;(ic+is)<intervals;){
//        	likelihoodCore.getNodeMatrices(nodeIndex, ic+is-1, matrix);
//        	if (ic<partials.size() && (is>=samples.size() || node.getChild(ic).getHeight()<samples.get(is).getTime())){
//        		likelihoodCore.calculatePartialsFixedPruning(partial1, matrix, partials.get(partials.size()-(ic+1)), newPartials);
//        		System.arraycopy( newPartials, 0, partial1, 0, newPartials.length );
//        		ic+=1;
//        	}else if (is<node.getSamples().size()){
//        		likelihoodCore.calculatePartialsFixedPruning(partial1, matrix, getPartials(samples.get(samples.size()-(1+is)).getTaxon(), samples.size()-(1+is), patternCount), newPartials);
//        		System.arraycopy( newPartials, 0, partial1, 0, newPartials.length );
//        		is+=1;
//        	}
//        }
//        likelihoodCore.getNodeMatrices(nodeIndex, intervals-1, matrix);
//    	likelihoodCore.calculatePartialsPruning(partial1, matrix, newPartials);
//    	System.arraycopy( newPartials, 0, partial1, 0, newPartials.length );
//    	likelihoodCore.calculatePartialsPruning(partial1, probabilitiesBottleneck ,newPartials);
//    	return partial1;
//    }
    
    
    
    double[] getPartials(int iTaxon, int patternCount){//, int taxonIndex, Taxon t
    	
        AlignmentPoMo data = (AlignmentPoMo) dataInput.get();
        int nStates = substitutionModel.getStateCount();
        int vPop=((nStates-4)/6)+1;
        double[] partials = new double[patternCount * nStates];

        int k = 0;
        //int iTaxon = taxonIndex;
        //int iTaxon = data.getTaxonIndex(t.getID());
        //int iTaxon = getTaxonNr(t, data);
        int[] nState, maxNucs, maxVals;
        for (int iPattern = 0; iPattern < patternCount; iPattern++) {
            nState = data.getPatternPoMo(iTaxon, iPattern);
            maxNucs = new int[]{-1, -1};
            maxVals = new int[]{0, 0};
            for (int iNuc = 0; iNuc < 4; iNuc++) {
            	if (nState[iNuc]>maxVals[0]){
            		maxNucs[1]=maxNucs[0];
            		maxVals[1]=maxVals[0];
            		maxNucs[0]=iNuc;
            		maxVals[0]=nState[iNuc];
            	}else if(nState[iNuc]>maxVals[1]){
            		maxNucs[1]=iNuc;
            		maxVals[1]=nState[iNuc];
            	}
            }
            if(maxVals[1]>0){
            	int numSeconds=0;
            	boolean equalFirst=false;
            	if (maxVals[1]==maxVals[0])equalFirst=true;
            	for (int iNuc = 0; iNuc < 4; iNuc++) {
            		if(nState[iNuc]==maxVals[1]) numSeconds+=1;
            	}
            	if(numSeconds>1){ //if there are multiple nucleotides sharing the second place of most abundant, than pick one at random as second most abundant.
            		int newSecond=Randomizer.nextInt(numSeconds);
            		int secondCount=0;
            		for (int iNuc = 0; iNuc < 4; iNuc++) {
                		if(nState[iNuc]==maxVals[1] && secondCount==newSecond) maxNucs[1]=iNuc;
                		else secondCount+=1;
                	}
            		if(equalFirst){
            			int newFirst=-1;
            			if(numSeconds==2){ newFirst=1-newSecond;
            			}else{
            				newFirst=Randomizer.nextInt(numSeconds-1);
            				if (newFirst>=newSecond) newFirst+=1;
            				
            			}
            			int firstCount=0;
                		for (int iNuc = 0; iNuc < 4; iNuc++) {
                    		if(nState[iNuc]==maxVals[1] && firstCount==newFirst) maxNucs[0]=iNuc;
                    		else firstCount+=1;
                    	}
            		}
            	}
            }
            if(maxNucs[0]==-1){
            	for (int iState = 0; iState < nStates; iState++) {
                    partials[k++] = 1.0;
                }
            }else {
            	//fixed states
            	//double sErr=seqErr.getValue();
            	for (int iState = 0; iState < 4; iState++) {
            		if (iState==maxNucs[0]){
            			partials[k++] = sampProb(1.0-sErr,maxVals[0],sErr/3.0,maxVals[1]);
            		} else if (iState==maxNucs[1]){
            			partials[k++] = sampProb(sErr/3.0,maxVals[0],1.0-sErr,maxVals[1]);
            		} else{
            			partials[k++] = sampProb(sErr/3.0,maxVals[0]+maxVals[1],1.0-sErr,0);
            		}
            	}
            	//groups of polymorphisms
            	for (int iGroup = 0; iGroup < 6; iGroup++) {
            		int[] ind=PoMoGeneral.indeces(iGroup);
            		for (int iState = 0; iState < vPop-1; iState++) {
                		double[] probs=new double[2];
                		double freq;
                		if (maxNucs[0]==ind[0]) {
                			freq=(iState+1.0)/((double)(vPop));
                			//probs[0]=freq*(1.0-sErr) + (1.0-freq)*sErr/3.0;
                		}else if(maxNucs[0]==ind[1]) {
                			freq=(vPop - (iState+1.0))/((double)(vPop));
                			//probs[0]=(vPop - (iState+1.0))/((double)(vPop));
                		}else{
                			freq=0.0;
                			//probs[0]=sErr;
                		}
                		probs[0]=freq*(1.0-sErr) + (1.0-freq)*sErr/3.0;
                		if (maxNucs[1]==ind[0]) {
                			freq=(iState+1.0)/((double)(vPop));
                			//probs[1]=(iState+1.0)/((double)(vPop));
                		}else if(maxNucs[1]==ind[1]) {
                			freq=(vPop - (iState+1.0))/((double)(vPop));
                			//probs[1]=(vPop - (iState+1.0))/((double)(vPop));
                		}else{
                			freq=0.0;
                			//probs[1]=sErr;
                		}
                		probs[1]=freq*(1.0-sErr) + (1.0-freq)*sErr/3.0;
                		partials[k++] = sampProb(probs[0],maxVals[0],probs[1],maxVals[1]);
            		}
            	}
            }
//            if (iPattern==0){
//            	String s="Pattern: "+maxNucs[0]+"-"+maxVals[0]+", "+maxNucs[1]+"-"+maxVals[1]+" \n    New Partials: ";
//                for (int i = 0; i < nStates; i++) {
//                	s+=", "+partials[i];
//                }
//                System.out.println(s);
//            }
        }
    	return partials;
    }
    
    
    //the partial likelihood of sampling - Nicola
    double sampProb(double p1, int v1, double p2, int v2){
    	double res=1.0,bigP,smallP;
    	int big,small;
    	if (v1>v2) {
    		big=v1;
    		bigP=p1;
    		small=v2;
    		smallP=p2;
    	}else{
    		big=v2;
    		bigP=p2;
    		small=v1;
    		smallP=p1;
    	}
    			
    	for (int i = 1; i <= small; i++) {
    		res=res*(i+big)*smallP*bigP/i;
    	}
    	for (int i = 1; i <= big-small; i++) {
    		res=res*bigP;
    	}
    	return res;
    }
    
    
    

    /**
     * Calculate the log likelihood of the current state.
     *
     * @return the log likelihood.
     */
    double m_fScale = 1.01;
    int m_nScale = 0;
    int X = 100;
    //boolean partialsDirty;

    @Override
    public double calculateLogP() throws IllegalArgumentException {
    	//System.out.println("Start calculate LogP ");
    	final TreePoMo tree = treePoMoInput.get();
    	//substitutionModel.getTransitionProbabilitiesBottleneck(probabilitiesBottleneck,bottle.getValue());
    	bottleneck=bottle.getValue();
    	if (bottleneck!=bottleneckStored){
    		getTransitionProbabilitiesBottleneck(probabilitiesBottleneck,bottleneck);
    	}
    	sErr=seqErr.getValue();
    	if (sErr!=sErrStored){
    		int nTaxa=alignment.getTaxonCount();
            double[] currPartials;//= new double[nStateCount];
            List<String> tNames = alignment.getTaxaNames();
            for (int t=0; t<nTaxa; t++){
            	currPartials=getPartials(alignment.getTaxonIndex(tNames.get(t)), nPatterns);
        		System.arraycopy(currPartials, 0, partialsTaxa[t], 0, nStateCount*nPatterns);
            }
    	}
    	//System.out.println("Got taxa partials ");
    	
    	
    	//NICOLA TODO beagle
//        if (beagle != null) {
//            try {
//				logP = beagle.calculateLogPPartials(partialsDirty, sErr);
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//            partialsDirty = false;
//            //System.out.println("Calculating likelihood, sErrStored="+sErrStored+"   SErr="+sErr+"  logP="+logP);
//            return logP;
//        }
        
        //setCurrentNodeMatrices(tree.getRoot());
        //setCurrentPartials(tree.getRoot(), dataInput.get().getPatternCount());
    	//System.out.println(tree.getRoot().toSortedNewick(new int[1]));
    	double[] rootPartials= getPartialsIterative(tree.getRoot(), dataInput.get().getPatternCount());
    	//System.out.println("Got partialsIterative ");
        double[] frequencies = substitutionModel.getFrequencies();
        double[] proportions = m_siteModel.getCategoryProportions();
        likelihoodCore.calculateIntegratePartials(rootPartials, proportions, m_fRootPartials);
        //System.out.println("Got IntegratePartials ");
        //likelihoodCore.integratePartials(tree.getRoot().getNr(), proportions, m_fRootPartials);
        if (constantPattern != null) {
            proportionInvariant = m_siteModel.getProportionInvariant();
            for (final int i : constantPattern) {
                m_fRootPartials[i] += proportionInvariant;
            }
        }
        likelihoodCore.calculateLogLikelihoods(m_fRootPartials, frequencies, patternLogLikelihoods);
        //System.out.println("Got calculateLikelihoods ");
        try {
			calcLogP();
		} catch (Exception e) {
			e.printStackTrace();
		}
        //System.out.println("Got calcLogP ");
        
        
        m_nScale++;
        if (logP > 0 || (likelihoodCore.getUseScaling() && m_nScale > X)) {
//            System.err.println("Switch off scaling");
//            m_likelihoodCore.setUseScaling(1.0);
//            m_likelihoodCore.unstore();
//            m_nHasDirt = Tree.IS_FILTHY;
//            X *= 2;
//            traverse(tree.getRoot());
//            calcLogP();
//            return logP;
        } else if (logP == Double.NEGATIVE_INFINITY && m_fScale < 10 && !scaling.get().equals(Scaling.none)) { // && !m_likelihoodCore.getUseScaling()) {
        	//final TreePoMo tree = treePoMoInput.get();
        	System.out.println(tree.getRoot().toSortedNewick(new int[1]));
        	//substitutionModel.getTransitionProbabilitiesBottleneck(probabilitiesBottleneck,bottle.getValue());
        	System.out.println("log-LK -infinity.\n bottle parameter: "+bottle.getValue()+"\n Bottle matrix of size "+probabilitiesBottleneck.length);
        	//for(int i=0;i<probabilitiesBottleneck.length;i++) System.out.println(probabilitiesBottleneck[i]);
        	rootPartials= getPartialsIterative(tree.getRoot(), dataInput.get().getPatternCount());
        	//System.out.println("Root partials of size "+rootPartials.length);
        	//for(int i=0;i<rootPartials.length;i++) System.out.println(rootPartials[i]);
            frequencies = substitutionModel.getFrequencies();
            //System.out.println("Root frequencies of size "+frequencies.length);
        	//for(int i=0;i<frequencies.length;i++) System.out.println(frequencies[i]);
        	Double[] nucFreqs=substitutionModel.getNucFrequencies();
        	//System.out.println("Root nuc frequencies of size "+nucFreqs.length);
        	//for(int i=0;i<nucFreqs.length;i++) System.out.println(nucFreqs[i]);
        	Double[] nucMuts=substitutionModel.getNucMuts();
        	//System.out.println("Nuc mutation rates of size "+nucMuts.length);
        	//for(int i=0;i<nucMuts.length;i++) System.out.println(nucMuts[i]);
//        	double[] matrix = new double[likelihoodCore.matrixSize];
//            substitutionModel.getTransitionProbabilities(0.1, 1.0, matrix);
//            System.out.println("PoMo substitution matrix for time 0.1, size "+matrix.length);
//        	for(int i=0;i<matrix.length;i++) System.out.println(matrix[i]);
//        	substitutionModel.getTransitionProbabilities(1.0, 1.0, matrix);
//            System.out.println("PoMo substitution matrix for time 1.0, size "+matrix.length);
//        	for(int i=0;i<matrix.length;i++) System.out.println(matrix[i]);
            proportions = m_siteModel.getCategoryProportions();
            //System.out.println("proportions of size "+proportions.length);
        	//for(int i=0;i<proportions.length;i++) System.out.println(proportions[i]);
            likelihoodCore.calculateIntegratePartials(rootPartials, proportions, m_fRootPartials);
            //System.out.println("m_fRootPartials of size "+m_fRootPartials.length);
        	//for(int i=0;i<m_fRootPartials.length;i++) System.out.println(m_fRootPartials[i]);
            if (constantPattern != null) {
                proportionInvariant = m_siteModel.getProportionInvariant();
                for (final int i : constantPattern) {
                    m_fRootPartials[i] += proportionInvariant;
                }
                System.out.println("constantPattern ");
            }
            likelihoodCore.calculateLogLikelihoods(m_fRootPartials, frequencies, patternLogLikelihoods);
            //System.out.println("patternLogLikelihoods of size "+patternLogLikelihoods.length);
        	//for(int i=0;i<patternLogLikelihoods.length;i++) System.out.println(patternLogLikelihoods[i]);
            try {
    			calcLogP();
    		} catch (Exception e) {
    			e.printStackTrace();
    		}
//            m_nScale = 0;
//            m_fScale *= 1.01;
//            System.err.println("Turning on scaling to prevent numeric instability " + m_fScale);
//            likelihoodCore.setUseScaling(m_fScale);
//            likelihoodCore.unstore();
//            hasDirt = TreePoMo.IS_FILTHY;
//            try {
//            	setCurrentNodeMatrices(tree.getRoot());
//                setCurrentPartials(tree.getRoot(), dataInput.get().getPatternCount());
//                frequencies = substitutionModel.getFrequencies();
//                proportions = m_siteModel.getCategoryProportions();
//                likelihoodCore.integratePartials(tree.getRoot().getNr(), proportions, m_fRootPartials);
//                if (constantPattern != null) {
//                    proportionInvariant = m_siteModel.getProportionInvariant();
//                    for (final int i : constantPattern) {
//                        m_fRootPartials[i] += proportionInvariant;
//                    }
//                }
//                //System.out.println("CalculateLogLikelihoods, root index "+likelihoodCore.getCurrentPartialsIndex(tree.getRoot().getNr()));
//                likelihoodCore.calculateLogLikelihoods(m_fRootPartials, frequencies, patternLogLikelihoods);
//				//substitutionModel.getTransitionProbabilitiesBottleneck(probabilitiesBottleneck);
//				//System.out.println("m_fRootPartials, length  "+m_fRootPartials.length);
//		        //for (int i3=0; i3<m_fRootPartials.length;i3++) System.out.println(m_fRootPartials[i3]); //////////////////////////////////////////
//				calcLogP();
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//            //System.out.println("End calculateLogP() "+logP);
//            return logP;
            System.out.println("-infinity likelihood "+logP);
            //System.exit(0);
        }
        //System.out.println("End calculateLogP() "+logP);
        return logP;
    }

    void calcLogP() throws Exception {
        logP = 0.0;
        if (useAscertainedSitePatterns) {
            final double ascertainmentCorrection = dataInput.get().getAscertainmentCorrection(patternLogLikelihoods);
            for (int i = 0; i < dataInput.get().getPatternCount(); i++) {
                logP += (patternLogLikelihoods[i] - ascertainmentCorrection) * dataInput.get().getPatternWeight(i);
            }
        } else {
            for (int i = 0; i < dataInput.get().getPatternCount(); i++) {
                logP += patternLogLikelihoods[i] * dataInput.get().getPatternWeight(i);
            }
        }
        if (logP>0.0){
        	System.out.println("log-likelihood>0, probably matrix exponentiation problem, rejecting move\n");
        	logP=Double.NEGATIVE_INFINITY;
        }
    }
    
//    void setNodeMatrices(final NodePoMo node){
//    	//System.out.println("Starting set node matrices "+node.getHeight());
//    	final int iNode = node.getNr();
//        final double branchRate = 1.0;
//        int intervals=node.getChildren().size()+node.getSamples().size();
//        double[] branchTimes=new double[intervals];
//        double currentTime=Double.MAX_VALUE;
//        int is=0,ic=0;
//        int nSamples=node.getSamples().size();
//        int nChildren=node.getChildren().size();
//        if (nChildren>0 &&(nSamples==0 || (node.getChild(nChildren-1).getHeight()<node.getSample(nSamples-1).getTime()))){
//        	currentTime=node.getChild(nChildren-1).getHeight();
//        	ic+=1;
//        } else if (nSamples>0){
//        	currentTime=node.getSample(nSamples-1).getTime();
//        	is+=1;
//        }
//        for (;(ic+is)<intervals;){
//        	if (ic<node.getChildren().size() && (is>=nSamples || node.getChild(nChildren-(ic+1)).getHeight()<node.getSample(nSamples-(1+is)).getTime())){
//        		branchTimes[ic+is-1]=node.getChild(nChildren-(ic+1)).getHeight()-currentTime;
//        		currentTime=node.getChild(nChildren-(ic+1)).getHeight();
//        		ic+=1;
//        	}else if (is<nSamples){
//        		branchTimes[ic+is-1]=node.getSample(nSamples-(1+is)).getTime() -currentTime;
//        		currentTime=node.getSample(nSamples-(1+is)).getTime();
//        		is+=1;
//        	}
//        }
//        if(intervals>0){
//        	branchTimes[intervals-1]=node.getHeight()-currentTime;
//        }
//        for (int i=0;i<intervals;i++){
//	            //likelihoodCore.setNodeMatrixForUpdate(iNode);
//	            for (int i2 = 0; i2 < m_siteModel.getCategoryCount(); i2++) {
//	                final double jointBranchRate = m_siteModel.getRateForCategory(i2, null) * branchRate;
//	                substitutionModel.getTransitionProbabilities(branchTimes[i], jointBranchRate, probabilities);
//	                likelihoodCore.setNodeMatrix(iNode, i2, i, intervals, probabilities);
//	            }
//        }
//        //System.out.println("Ending set node matrices "+node.getHeight());
//        for (NodePoMo c :node.getChildren()){
//        	setNodeMatrices(c);
//    	}
//        //m_branchLengths[iNode]=branchTimes;
//        storedBranchLengths[iNode]=branchTimes;
//    }
//    
//    
//    
//    void setCurrentNodeMatrices(final NodePoMo node){
//    	//System.out.println("Starting set current node matrices "+node.getHeight());
//    	//int update = (node.isDirty() | hasDirt);
//    	final int iNode = node.getNr();
//        final double branchRate = 1.0;
//        int intervals=node.getChildren().size()+node.getSamples().size();
//        double[] branchTimes=new double[intervals];
//        double currentTime=Double.MAX_VALUE;
//        int is=0,ic=0;
//        int nSamples=node.getSamples().size();
//        int nChildren=node.getChildren().size();
//        if (nChildren>0 &&(nSamples==0 || (node.getChild(nChildren-1).getHeight()<node.getSample(nSamples-1).getTime()))){
//        	currentTime=node.getChild(nChildren-1).getHeight();
//        	ic+=1;
//        } else if (nSamples>0){
//        	currentTime=node.getSample(nSamples-1).getTime();
//        	is+=1;
//        }
//        for (;(ic+is)<intervals;){
//        	if (ic<node.getChildren().size() && (is>=nSamples || node.getChild(nChildren-(ic+1)).getHeight()<node.getSample(nSamples-(1+is)).getTime())){
//        		branchTimes[ic+is-1]=node.getChild(nChildren-(ic+1)).getHeight()-currentTime;
//        		currentTime=node.getChild(nChildren-(ic+1)).getHeight();
//        		ic+=1;
//        	}else if (is<nSamples){
//        		branchTimes[ic+is-1]=node.getSample(nSamples-(1+is)).getTime() -currentTime;
//        		currentTime=node.getSample(nSamples-(1+is)).getTime();
//        		is+=1;
//        	}
//        }
//        if(intervals>0){
//        	branchTimes[intervals-1]=node.getHeight()-currentTime;
//        }
//        for (int i=0;i<intervals;i++){
//	        if ((node.isDirty() != TreePoMo.IS_CLEAN || branchTimes.length!=m_branchLengths[iNode].length || branchTimes[i] != m_branchLengths[iNode][i] || hasDirt!=TreePoMo.IS_CLEAN)) {
//	            likelihoodCore.setNodeMatrixForUpdate(iNode);
//	            for (int i2 = 0; i2 < m_siteModel.getCategoryCount(); i2++) {	
//	                final double jointBranchRate = m_siteModel.getRateForCategory(i2, null) * branchRate;
//	                substitutionModel.getTransitionProbabilities(branchTimes[i], jointBranchRate, probabilities);
//	                //System.out.println("Substitution probabilities, category "+i2+" interval "+i+" node "+iNode+" length "+probabilities.length);
//	                //for (int i3=0; i3<probabilities.length;i3++) System.out.println(probabilities[i3]); //////////////////////////////////////////
//	                likelihoodCore.setNodeMatrix(iNode, i2, i, intervals, probabilities);
//	            }
//	            //update |= TreePoMo.IS_DIRTY;
//	        }
//        }
//        //System.out.println("Ending set current node matrices "+node.getHeight());
//        for (NodePoMo c :node.getChildren()){
//        	setCurrentNodeMatrices(c);
//        }
//        m_branchLengths[iNode]=branchTimes;
//    }
    
    
    
    
    

    /* Assumes there IS a branch rate model as opposed to traverse() */
//    int traverse(final NodePoMo node) throws Exception {
//        final int iNode = node.getNr();
//        final double branchRate = 1.0; // branchRateModel.getRateForBranch(node);
//
//            if (node.isRoot()) {
//            	System.out.println("traverse- root business");
//                final double[] frequencies = substitutionModel.getFrequencies();
//                final double[] proportions = m_siteModel.getCategoryProportions();
//                likelihoodCore.integratePartials(node.getNr(), proportions, m_fRootPartials);
//                if (constantPattern != null) {
//                    proportionInvariant = m_siteModel.getProportionInvariant();
//                    for (final int i : constantPattern) {
//                        m_fRootPartials[i] += proportionInvariant;
//                    }
//                }
//                likelihoodCore.calculateLogLikelihoods(m_fRootPartials, frequencies, patternLogLikelihoods);
//            }
//    } // traverseWithBRM
    
    

    /** CalculationNode methods **/

    /**
     * check state for changed variables and update temp results if necessary *
     */
    @Override
    protected boolean requiresRecalculation() {
    	return true;
    	//System.out.println("Start requiresRecalculation ");
    	
    	//NICOLA TODO : allow beagle
//        if (beagle != null) {
//            return (beagle.requiresRecalculationFromLike(partialsDirty,seqErr.getValue())||partialsDirty);
//        }
//        hasDirt = TreePoMo.IS_CLEAN;
//        
//        if(seqErr.somethingIsDirty()||bottle.somethingIsDirty()){
//        	//partialsDirty = true;
//        	hasDirt = TreePoMo.IS_FILTHY;
//        	return true;
//        }
//
//        if (dataInput.get().isDirtyCalculation()) {
//            hasDirt = TreePoMo.IS_FILTHY;
//            return true;
//        }
//        if (m_siteModel.isDirtyCalculation()) {
//            hasDirt = TreePoMo.IS_DIRTY;
//            return true;
//        }
////        if (branchRateModel != null && branchRateModel.isDirtyCalculation()) {
////            //m_nHasDirt = Tree.IS_DIRTY;
////            return true;
////        }
//        
////        if(partialsDirty){
////        	return true;
////        }
//        //hasDirt = TreePoMo.IS_FILTHY;
//        //return true;
//        return treePoMoInput.get().somethingIsDirty();
    }

    @Override
    public void store() {
    	//System.out.println("Start store ");
    	if (sErr!=sErrStored){
    		int nTaxa=alignment.getTaxonCount();
            //double[] currPartials;//= new double[nStateCount];
            //List<String> tNames = alignment.getTaxaNames();
            for (int t=0; t<nTaxa; t++){
            	System.arraycopy(partialsTaxa[t], 0, partialsTaxaStored[t], 0, nPatterns*nStateCount);
            }
    	}
    	sErrStored=sErr;
        if (beagle != null) {
            beagle.store();
            super.store();
            return;
        }
        if (likelihoodCore != null) {
            likelihoodCore.store();
        }
        super.store();
//        for (int i=0;i< m_branchLengths.length;i++){
//        	System.arraycopy(m_branchLengths[i], 0, storedBranchLengths[i], 0, m_branchLengths[i].length);
//        }
//        if( eigenDecompositionBottleneck != null ) {
//            storedEigenDecompositionBottleneck = eigenDecompositionBottleneck.copy();
//        }
        if (bottleneck!=bottleneckStored){
        	probabilitiesBottleneckStored=probabilitiesBottleneck.clone();
        	bottleneckStored=bottleneck;
        }
        
        //System.out.println("End store ");
        //System.arraycopy(m_branchLengths, 0, storedBranchLengths, 0, m_branchLengths.length);
    }

    @Override
    public void restore() {
    	//System.out.println("Start restore ");
    	if (sErr!=sErrStored){
    		int nTaxa=alignment.getTaxonCount();
            for (int t=0; t<nTaxa; t++){
            	System.arraycopy(partialsTaxaStored[t], 0, partialsTaxa[t], 0, nPatterns*nStateCount);
            }
    	}
    	sErr=sErrStored;
    	//partialsDirty=false;
        if (beagle != null) {
            beagle.restore();
            super.restore();
            return;
        }
        if (likelihoodCore != null) {
            likelihoodCore.restore();
        }
        super.restore();
//        for (int i=0;i< m_branchLengths.length;i++){
//        	double[] tmp = m_branchLengths[i];
//            m_branchLengths[i] = storedBranchLengths[i];
//            storedBranchLengths[i] = tmp;
//        }
        if (bottleneck!=bottleneckStored){
        	probabilitiesBottleneck=probabilitiesBottleneckStored.clone();
        	//double[] tmp = probabilitiesBottleneck;
            //probabilitiesBottleneck = probabilitiesBottleneckStored;
            //probabilitiesBottleneckStored = tmp;
        	bottleneckStored=bottleneck;
        }
        
        
//        if( storedEigenDecompositionBottleneck != null ) {
//            EigenDecomposition tmp = storedEigenDecompositionBottleneck;
//            storedEigenDecompositionBottleneck = eigenDecompositionBottleneck;
//            eigenDecompositionBottleneck = tmp;
//        }
        //probabilitiesBottleneck=probabilitiesBottleneckStored.clone();
        //double[] tmp = m_branchLengths;
        //m_branchLengths = storedBranchLengths;
        //storedBranchLengths = tmp;
        //System.out.println("End restore ");
    }

    /**
     * @return a list of unique ids for the state nodes that form the argument
     */
    public List<String> getArguments() {
        return Collections.singletonList(dataInput.get().getID());
    }

    /**
     * @return a list of unique ids for the state nodes that make up the conditions
     */
    public List<String> getConditions() {
        return m_siteModel.getConditions();
    }
    
    
    
    
    public void getTransitionProbabilitiesBottleneck(double[] matrix, double bottleSize) {
        //double distance = bottle.getValue();
        
        int i, j, k;
        double temp;

        // this must be synchronized to avoid being called simultaneously by
        // two different likelihood threads - AJD
//        if(bottleSize!=bottleneck){
//        	
//        	setupRateMatrixBottleneck();
//        	eigenDecompositionBottleneck = eigenSystemBottleneck.decomposeMatrix(rateMatrixBottleneck);
//        }
//        synchronized (this) {
//            if (updateMatrixBottleneck) {
//                //setupRelativeRates();
//                setupRateMatrixBottleneck();
//                //eigenDecomposition = eigenSystem.decomposeMatrix(rateMatrix);
//                eigenDecompositionBottleneck = eigenSystemBottleneck.decomposeMatrix(rateMatrixBottleneck);
//                //updateMatrixBottleneck = false;
//            }
//        }

        // is the following really necessary?
        // implemented a pool of iexp matrices to support multiple threads
        // without creating a new matrix each call. - AJD
        // a quick timing experiment shows no difference - RRB
        double[] iexp = new double[nStateCount * nStateCount];
        // Eigen vectors
        double[] Evec = eigenDecompositionBottleneck.getEigenVectors();
        // inverse Eigen vectors
        double[] Ievc = eigenDecompositionBottleneck.getInverseEigenVectors();
        // Eigen values
        double[] Eval = eigenDecompositionBottleneck.getEigenValues();
        for (i = 0; i < nStateCount; i++) {
            temp = Math.exp(bottleSize * Eval[i]);
            for (j = 0; j < nStateCount; j++) {
                iexp[i * nStateCount + j] = Ievc[i * nStateCount + j] * temp;
            }
        }

        int u = 0;
        for (i = 0; i < nStateCount; i++) {
            for (j = 0; j < nStateCount; j++) {
                temp = 0.0;
                for (k = 0; k < nStateCount; k++) {
                    temp += Evec[i * nStateCount + k] * iexp[k * nStateCount + j];
                }

                matrix[u] = Math.abs(temp);
                u++;
            }
        }
        
        //if exponentiation fails, return matrix of 0s to give -infinite log-likelihood
        for (i = 0; i < nStateCount; i++) {
        	temp = 0.0;
            for (j = 0; j < nStateCount; j++) {
                temp +=matrix[i*nStateCount +j];
            }
            if (temp>1.001 || temp<0.999){
            	//System.out.println("Matrix exponentiation has given non-probability matrix: rejecting move\n");
            	for (i = 0; i < nStateCount; i++) {
	            	for (j = 0; j < nStateCount; j++) {
	                    matrix[i*nStateCount +j]=0.0;
	                }
            	}
            	break;
            }	
        }
    } // getTransitionProbabilities
    
    
//    /**
//     * This function returns the Eigen vectors for the bottlenecks.
//     *
//     * @return the array
//     */
//    public EigenDecomposition getEigenDecompositionBottlenecks(Node node) {
//        return getEigenDecompositionBottlenecks();
//    }
    
//    public EigenDecomposition getEigenDecompositionBottlenecks() {
//        synchronized (this) {
//            if (updateMatrixBottleneck) {
//                //setupRelativeRates();
//                setupRateMatrixBottleneck();
//                //eigenDecomposition = eigenSystem.decomposeMatrix(rateMatrix);
//                eigenDecompositionBottleneck = eigenSystem.decomposeMatrix(rateMatrixBottleneck);
//                updateMatrixBottleneck = false;
//            }
//        }
//        return eigenDecompositionBottleneck;
//    }
    
//    /**
//     * sets up rate matrix for the bottleneck*
//     */
//    protected void setupRateMatrixBottleneck() {
//		vPop = virtualPopInput.get();
//	    //Double[] nucFreqs=mutM.getFreqs();
//    	rateMatrixBottleneck=getRatesBottleneck(vPop);   
//    }
    
    /**
     * get the PoMo rate Matrix for a population bottleneck.
     * First 4 rows are fixed states, ordered A,C,G,T. Then are polymorhic state group, ordered A->C, A->G, A->T, C->G, C->T, G->T. 
     * Within each group states are ordered 1A-9C, 2A-8C, etc... 
     */
    public double[][] getRatesBottleneck( int vPop){
    	int nrOfStates=6*(vPop-1) +4;
    	double[][] rates;
    	rates= new double[nrOfStates][nrOfStates];
    	int i,j,k;//,k2;
    	for(i=0;i<nrOfStates;i++){
    		for(j=0;j<nrOfStates;j++) rates[i][j]=0.0;
    	}
    	
    	//fixation rates
    	k=0;
    	//k2=0;
    	for(i=0;i<4;i++){
    		for(j=0;j<4;j++){
    			if (i<j){
    				rates[4+k*(vPop-1)][j]=coeff(0.0,0.0, vPop, 1, 0);
    				rates[4+k*(vPop-1)+(vPop-2)][i]=coeff(0.0,0.0, vPop, 1, 0);
    				k+=1;
    			}
    		}	
    	}
    	
    	//other allele frequency changes
    	for(i=0;i<6;i++){
    		//int[] ind=indeces(i);
    		for(j=0;j<vPop-1;j++){
    			if (j>0) rates[4+i*(vPop-1)+j][4+i*(vPop-1)+j-1]=coeff(0.0,0.0, vPop, j+1, j);
    			if (j<vPop-2) rates[4+i*(vPop-1)+j][4+i*(vPop-1)+j+1]=coeff(0.0,0.0, vPop, j+1, j+2);
    		}
    	}
    	
    	//diagonal elements
    	for(i=0;i<nrOfStates;i++){
    		double sum=0.0;
    		for(j=0;j<nrOfStates;j++){
    			if (i!=j)sum+=rates[i][j];
    		}
    		rates[i][i]=-sum;
    	}
    	return rates;
    }
    
    
  //PoMo allele frequency change rate
    public double coeff(double fit1,double fit2, int vPop, int num1, int num2){
    	double co=0.0;
    	if (num2==num1+1){
    		co=num1*(1.0+fit1-fit2)*(vPop-num1)/(vPop*(num1*(1.0+fit1-fit2) + (vPop-num1)));
    	}else if (num2==num1-1){
    		co=num1*(vPop-num1)/(vPop*(num1*(1.0+fit1-fit2) + (vPop-num1)));
    	}else {
    		System.out.println("Problem with PoMo indeces in calculating the rate matrix");
    		System.exit(0);
    	}
    	return co;
    }
    
    
  //help with indeces
    public Integer opposite(Integer v){
		if (v==2) return 3;
		else if (v==3) return 2;
		else return v;
	}
    
  //return alleles associate with group
    public static int[] indeces(int v){
    	int[] ind;
    	ind = new int[2];
		if (v==0) {ind[0]=0; ind[1]=1;}
		if (v==1) {ind[0]=0; ind[1]=2;}
		if (v==2) {ind[0]=0; ind[1]=3;}
		if (v==3) {ind[0]=1; ind[1]=2;}
		if (v==4) {ind[0]=1; ind[1]=3;}
		if (v==5) {ind[0]=2; ind[1]=3;}
		return ind;
	}
    
//    /**
//     * access to (copy of) rate matrix for the bottleneck *
//     */
//    protected double[][] getRateMatrixBottleneck() {
//        return rateMatrixBottleneck.clone();
//    }

} // class TreeLikelihood
