/*
* File TreeLikelihood.java
*
* Copyright (C) 2010 Remco Bouckaert remco@cs.auckland.ac.nz
*
* This file is part of BEAST2.
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership and licensing.
*
* BEAST is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
*  BEAST is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with BEAST; if not, write to the
* Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
* Boston, MA  02110-1301  USA
*/


package badtrip.evolution.likelihood;

import badtrip.evolution.alignment.AlignmentPoMo;
import badtrip.evolution.datatype.DataTypePoMo;
import badtrip.evolution.datatype.Sample;
import badtrip.evolution.sitemodel.SiteModelInterfacePoMo;
import badtrip.evolution.sitemodel.SiteModelPoMo;
import badtrip.evolution.substitutionmodel.PoMoGeneral;
import badtrip.evolution.tree.NodePoMo;
import badtrip.evolution.tree.TreePoMo;
import beast.base.core.Description;
import beast.base.core.Input;
import beast.base.core.Input.Validate;
import beast.base.core.Log;
import beast.base.evolution.alignment.Taxon;
import beast.base.evolution.sitemodel.SiteModelInterface;
import beast.base.inference.Distribution;
import beast.base.inference.State;
import beast.base.inference.parameter.RealParameter;

import java.util.*;
//import beast.base.evolution.tree.TreeInterface;


@Description("Calculates the probability of deep sequencing data on a PoMo transmission tree given a site and PoMo substitution model using " +
        "a variant of the 'peeling algorithm'. For details, see" +
        "Felsenstein, Joseph (1981). Evolutionary trees from DNA sequences: a maximum likelihood approach. J Mol Evol 17 (6): 368-376.")
public class TreeLikelihoodPoMoTransmission extends Distribution {
    
    final public Input<AlignmentPoMo> dataInput = new Input<>("data", "sequence PoMo data", Validate.REQUIRED);

    final public Input<TreePoMo> treePoMoInput = new Input<>("treePoMo", "phylogenetic transmission PoMo tree with sequence data in the leafs and internal nodes", Validate.REQUIRED);

    final public Input<SiteModelInterface> siteModelInput = new Input<>("siteModel", "site model for leafs in the beast.tree", Validate.REQUIRED);
    
    //final public Input<BranchRateModel.Base> branchRateModelInput = new Input<>("branchRateModel", "A model describing the rates on the branches of the beast.tree.");

	//@Override
	//public List<String> getArguments() {return null;}

	//@Override
	//public List<String> getConditions() {return null;}

	//@Override
	//public void sample(State state, Random random) {}

    public Input<Boolean> m_useAmbiguities = new Input<Boolean>("useAmbiguities", "flag to indicate leafs that sites containing ambigue states should be handled instead of ignored (the default)", false);
    
    public Input<RealParameter> seqErrorInput = new Input<RealParameter>("seqError", " starting sequencing error rate, 0 by default", Validate.REQUIRED);
    
	public Input<RealParameter> bottleneckInput = new Input<RealParameter>("bottleneck", "parameter describing how many Ne generiations the bottleneck corresponds to in terms of drift", Validate.REQUIRED);
    
    enum Scaling {none, always, _default};
    public Input<Scaling> scaling = new Input<TreeLikelihoodPoMoTransmission.Scaling>("scaling", "type of scaling to use, one of " + Arrays.toString(Scaling.values()) + ". If not specified, the -beagle_scaling flag is used.", Scaling._default, Scaling.values());
    
    //public Input<AlignmentPoMo2> dataInput = new Input<AlignmentPoMo2>("data", "sequence data for the beast.tree", Validate.REQUIRED);
    
    //public Input<SiteModelInterfacePoMo> siteModelInput = new Input<SiteModelInterfacePoMo>("siteModel", "site model for leafs in the beast.tree", Validate.REQUIRED);

    /**
     * calculation engine *
     */
    protected BeerLikelihoodCorePoMo likelihoodCore;
    BeagleTreeLikelihoodPoMo beagle;
    
    //TreePoMo treePoMo=treePoMoInput.get();
    
    RealParameter seqErr, bottle;
    //double sErr, sErrStored;

    /**
     * Plugin associated with inputs. Since none of the inputs are StateNodes, it
     * is safe to link to them only once, during initAndValidate.
     */
    PoMoGeneral substitutionModel;
    protected SiteModelPoMo.Base m_siteModel;
    //protected BranchRateModel.Base branchRateModel;

    /**
     * flag to indicate the
     * // when CLEAN=0, nothing needs to be recalculated for the node
     * // when DIRTY=1 indicates a node partial needs to be recalculated
     * // when FILTHY=2 indicates the indices for the node need to be recalculated
     * // (often not necessary while node partial recalculation is required)
     */
    protected int hasDirt;

    /**
     * Lengths of the branches in the tree associated with each of the nodes
     * in the tree through their node  numbers. By comparing whether the
     * current branch length differs from stored branch lengths, it is tested
     * whether a node is dirty and needs to be recomputed (there may be other
     * reasons as well...).
     * These lengths take branch rate models in account.
     */
    protected double[][] m_branchLengths;
    protected double[][] storedBranchLengths;

    /**
     * memory allocation for likelihoods for each of the patterns *
     */
    double[] patternLogLikelihoods;
    /**
     * memory allocation for the root partials *
     */
    double[] m_fRootPartials;
    /**
     * memory allocation for probability tables obtained from the SiteModel *
     */
    double[] probabilities;
    //transition probabilities for bottleneck: just one set for the whole tree
    double[] probabilitiesBottleneck, probabilitiesBottleneckStored;

    int matrixSize;

    /**
     * flag to indicate ascertainment correction should be applied *
     */
    boolean useAscertainedSitePatterns = false;

    /**
     * dealing with proportion of site being invariant *
     */
    double proportionInvariant = 0;
    List<Integer> constantPattern = null;
    

    @Override
    public void initAndValidate() throws IllegalArgumentException {
    	
    	seqErr = seqErrorInput.get();
    	bottle=bottleneckInput.get();
		//sErr = seqErr.getValue();
		//sErrStored = sErr;
		//partialsDirty = true;
    	
        // you might have multiple samples per node and samples at internal nodes, so this is obsolete
//        if (dataInput.get().getTaxonCount() != treeInput.get().getLeafNodeCount()) {
//            throw new IllegalArgumentException("The number of nodes in the tree does not match the number of sequences");
//        }
		//NICOLA TODO : try to see if you can use beagle.
        beagle = null;
//        beagle = new BeagleTreeLikelihoodPoMoTransmission();
//        try {
//	        beagle.initByName(
//                    "data", dataInput.get(), "tree", treeInput.get(), "siteModel", ((SiteModelPoMo) siteModelInput.get()),
//                    "branchRateModel", null , "useAmbiguities", m_useAmbiguities.get(),
//                    "scaling", scaling.get().toString());//branchRateModelInput.get()
//	        if (beagle.beagle != null) {
//	            //a Beagle instance was found, so we use it
//	        	System.out.println("Used beagle\n");
//	            return;
//	        }
//        } catch (Exception e) {
//        	System.out.println("Not using Beagle\n");
//			// ignore
//		}
//        // No Beagle instance was found, so we use the good old java likelihood core
//        beagle = null;
        
        //treePoMo=treePoMoInput.get();
        int nodeCount = treePoMoInput.get().getNodeCount();
        if (!((siteModelInput.get()) instanceof SiteModelPoMo.Base)) {
        	throw new IllegalArgumentException("siteModel input should be of type SiteModel.Base");
        }
        m_siteModel = (SiteModelInterfacePoMo.Base) siteModelInput.get();
        m_siteModel.setDataType((DataTypePoMo)(dataInput.get().getDataType()));
        substitutionModel = m_siteModel.substModelInput.get();

//        if (branchRateModelInput.get() != null) {
//            branchRateModel = branchRateModelInput.get();
//        } else {
        //branchRateModel = new StrictClockModel();
        //}
        m_branchLengths = new double[nodeCount][];
        storedBranchLengths = new double[nodeCount][];

        int nStateCount = dataInput.get().getMaxStateCount();
        nStateCount=substitutionModel.getStateCount();
        int nPatterns = dataInput.get().getPatternCount();
        likelihoodCore = new BeerLikelihoodCorePoMo(nStateCount);

        String className = getClass().getSimpleName();

        AlignmentPoMo alignment = (AlignmentPoMo) dataInput.get();

        Log.info.println(className + "(" + getID() + ") uses " + likelihoodCore.getClass().getSimpleName());
        Log.info.println("  " + alignment.toString(true));
        // print startup messages via Log.print*

        proportionInvariant = m_siteModel.getProportionInvariant();
        m_siteModel.setPropInvariantIsCategory(false);
        if (proportionInvariant > 0) {
            calcConstantPatternIndices(nPatterns, nStateCount);
        }

        patternLogLikelihoods = new double[nPatterns];
        m_fRootPartials = new double[nPatterns * nStateCount];
        //matrixSize = (nStateCount + 1) * (nStateCount + 1);
        matrixSize = (nStateCount ) * (nStateCount );
        //probabilities = new double[(nStateCount + 1) * (nStateCount + 1)];
        probabilities = new double[(nStateCount ) * (nStateCount )];
        Arrays.fill(probabilities, 1.0);
        //probabilitiesBottleneck = new double[(nStateCount + 1) * (nStateCount + 1)];
        probabilitiesBottleneck = new double[(nStateCount ) * (nStateCount )];
        Arrays.fill(probabilitiesBottleneck, 1.0);
        probabilitiesBottleneckStored=probabilitiesBottleneck.clone();

        if (dataInput.get().isAscertained) {
            useAscertainedSitePatterns = true;
        }
        
        initCore();
        
    }


    /**
     * Determine indices of m_fRootProbabilities that need to be updates
     * // due to sites being invariant. If none of the sites are invariant,
     * // the 'site invariant' category does not contribute anything to the
     * // root probability. If the site IS invariant for a certain character,
     * // taking ambiguities in account, there is a contribution of 1 from
     * // the 'site invariant' category.
     */
    void calcConstantPatternIndices(final int nPatterns, final int nStateCount) {
        constantPattern = new ArrayList<Integer>();
        for (int i = 0; i < nPatterns; i++) {
           // final int[][] pattern = dataInput.get().getPatternPoMo(i);
            final boolean[] bIsInvariant = new boolean[nStateCount];
            Arrays.fill(bIsInvariant, true);
            for (int k = 0; k < nStateCount; k++) {
                if (bIsInvariant[k]) {
                    constantPattern.add(i * nStateCount + k);
                }
            }
        }
    }

    void initCore() {
    	System.out.println("InitCore() ");
    	TreePoMo treePoMo=treePoMoInput.get();
        final int nodeCount = treePoMo.getNodeCount();
        likelihoodCore.initialize(
                nodeCount,
                dataInput.get().getPatternCount(),
                m_siteModel.getCategoryCount(),
                true, m_useAmbiguities.get()
        );

        final int extNodeCount = treePoMo.getLeafNodeCount();
        final int intNodeCount = treePoMo.getInternalNodeCount();

        //sErr = seqErr.getValue();
        
        System.out.println("Setting node Matrices ");
        setNodeMatrices(treePoMo.getRoot());
        //setCurrentNodeMatrices(treePoMo.getRoot());
        //traverseMatrices(treePoMo.getRoot());
        
        System.out.println("Creating node partials ");
        for (int i = 0; i < intNodeCount+extNodeCount; i++) {
            likelihoodCore.createNodePartials(i);//extNodeCount +
        }
        
        System.out.println("Setting node Partials");
        setPartials(treePoMo.getRoot(), dataInput.get().getPatternCount());
        //setCurrentPartials(treePoMo.getRoot(), dataInput.get().getPatternCount());
        
        //traverse(treePoMo.getRoot());
        
        hasDirt = TreePoMo.IS_FILTHY;
        System.out.println("End InitCore() ");
    }

    /**
     * This method samples the sequences based on the tree and site model.
     */
    public void sample(State state, Random random) {
        throw new UnsupportedOperationException("Can't sample a fixed alignment!");
    }

    /**
     * set leaf states in likelihood core *
     */
    void setStates(NodePoMo node, int patternCount) {
    	System.out.println("Warning, setting states in PoMo, no reason to.");
    }

    protected int getTaxonNr(NodePoMo node, int sampleIndex, AlignmentPoMo data) {
        int iTaxon = data.getTaxonIndex(node.getSample(sampleIndex).getTaxon().getID());
        if (iTaxon == -1) {
        	if (node.getID().startsWith("'") || node.getID().startsWith("\"")) {
        		iTaxon = data.getTaxonIndex(node.getSample(sampleIndex).getTaxon().getID().substring(1,node.getID().length()-1));
        	}
            if (iTaxon == -1) {
            	throw new RuntimeException("Could not find sequence " + node.getSample(sampleIndex).getTaxon().getID() + " in the alignment");
            }
        }
        return iTaxon;
	}

	/**
     * set leaf partials in likelihood core *
     */
    //most important modified bit - Nicola
    void setPartials(NodePoMo node, int patternCount) {
    	final ArrayList<Integer> childNums = new ArrayList<Integer>();
    	for (NodePoMo c : node.getChildren()){
    		setPartials(c, patternCount);
    		childNums.add(c.getNr());
    	}
        if (node.isLeaf()) {
        	double[] partials=getPartialsList(node.getSamples(), node.getNr(), node.getHeight(), patternCount);
            likelihoodCore.setNodePartials(node.getNr(), partials);
        } else {
        	ArrayList<double[]> partials = new ArrayList<double[]>();
            for (int i=0;i<childNums.size();i++){
            	double[] partial = new double[likelihoodCore.partialsSize];
            	likelihoodCore.getPartials(childNums.get(i), partial);
            	partials.add(partial);
            }
        	double[] newPartials=getPartialsInternal(node, node.getSamples(), partials, node.getNr(), patternCount);
        	likelihoodCore.setNodePartials(node.getNr(), newPartials);
        }
    }
    
    
    /**
     * set leaf partials in likelihood core *
     */
    //most important modified bit - Nicola
    boolean setCurrentPartials(NodePoMo node, int patternCount) {
    	final ArrayList<Integer> childNums = new ArrayList<Integer>();
    	boolean clean=true;
    	for (NodePoMo c : node.getChildren()){
    		if (!setCurrentPartials(c, patternCount)) clean=false;
    		childNums.add(c.getNr());
    	}
    	if (node.isDirty()!=TreePoMo.IS_CLEAN||hasDirt!=TreePoMo.IS_CLEAN) clean=false;
        if (node.isLeaf() && (!clean)) {
        	likelihoodCore.setNodePartialsForUpdate(node.getNr());
            double[] partials=getPartialsList(node.getSamples(), node.getNr(), node.getHeight(), patternCount);
            //System.out.println("Leaf Partials, patternCount "+patternCount+" node "+node.getNr()+" currentPartIndex "+likelihoodCore.getCurrentPartialsIndex(node.getNr()));
            //for (int i3=0; i3<partials.length;i3++) System.out.println(partials[i3]); //////////////////////////////////////////
            likelihoodCore.setCurrentNodePartials(node.getNr(), partials);
        } else if ((!node.isLeaf()) && (!clean)) {
        	likelihoodCore.setNodePartialsForUpdate(node.getNr());
        	ArrayList<double[]> partials = new ArrayList<double[]>();
            for (int i=0;i<childNums.size();i++){
            	double[] partial = new double[likelihoodCore.partialsSize];
            	likelihoodCore.getPartials(childNums.get(i), partial);
            	partials.add(partial);
            }
        	double[] newPartials=getPartialsInternal(node, node.getSamples(), partials, node.getNr(), patternCount);
        	//System.out.println("Internal Partials, patternCount "+patternCount+" node "+node.getNr()+" length "+newPartials.length+" currentPartIndex "+likelihoodCore.getCurrentPartialsIndex(node.getNr()));
            //for (int i3=0; i3<newPartials.length;i3++) System.out.println(newPartials[i3]); //////////////////////////////////////////
        	likelihoodCore.setCurrentNodePartials(node.getNr(), newPartials);
        	//likelihoodCore.getPartials(node.getNr(), newPartials);////just for checking
        	//System.out.println("Internal Partials, retrivieng the same partials node "+node.getNr()+" currentPartIndex "+likelihoodCore.getCurrentPartialsIndex(node.getNr()));
            //for (int i3=0; i3<newPartials.length;i3++) System.out.println(newPartials[i3]); //////////////////////////////////////////
        }
        return clean;
    }

    //travel through samples from a leaf node to get to the partial at its colonization time.
    double[] getPartialsList(List<Sample> samples, int nodeIndex, double height, int patternCount){
    	//System.out.println("getPartialsList "+samples.size()+" "+nodeIndex+" "+height+" "+patternCount);
    	//System.out.println(samples.size()+" "+nodeIndex+" "+height+" "+patternCount);
    	double[] partials=new double[likelihoodCore.partialsSize];
    	double[] ps;
    	//System.out.println("Getting one set of partials");
    	ps=getPartials(samples.get(samples.size()-1).getTaxon(), samples.size()-1, patternCount);
    	//System.out.println("First sample Partials, length "+samples.get(samples.size()-1).getTaxon().getID()+" "+ps.length);
        //for (int i3=0; i3<ps.length;i3++) System.out.println(ps[i3]); //////////////////////////////////////////
    	//System.out.println("Partials lengths "+ps.length +" "+partials.length);
    	System.arraycopy( ps, 0, partials, 0, ps.length );
    	double[] newPartials=new double[likelihoodCore.partialsSize];
    	//double[] matrix = new double[likelihoodCore.matrixSize * likelihoodCore.nrOfPatterns];
    	double[] matrix = new double[likelihoodCore.matrixSize];
    	//System.out.println("Starting for");
    	for (int i=0;i<samples.size()-1;i++){
    		likelihoodCore.getNodeMatrices(nodeIndex, i, matrix);
    		likelihoodCore.calculatePartialsFixedPruning(partials, matrix, getPartials(samples.get(samples.size()-(2+i)).getTaxon(), samples.size()-(2+i), patternCount), newPartials);
    		//partials=getPartialsUpdate(partials,taxa.get(taxa.size()-(2+i)), taxa.size()-(2+i), matrix, patternCount);
    		System.arraycopy( newPartials, 0, partials, 0, newPartials.length );
    	}
    	//System.out.println("Ending for");
    	likelihoodCore.getNodeMatrices(nodeIndex, samples.size()-1, matrix);
    	//System.out.println("Matrix for multiplication "+matrix.length);
        //for (int i3=0; i3<matrix.length;i3++) System.out.println(matrix[i3]); //////////////////////////////////////////
    	//System.out.println("Got matrices");
    	likelihoodCore.calculatePartialsPruning(partials, matrix, newPartials);
    	System.arraycopy( newPartials, 0, partials, 0, newPartials.length );
    	//System.out.println("partials after multiplication "+partials.length);
        //for (int i3=0; i3<partials.length;i3++) System.out.println(partials[i3]); //////////////////////////////////////////
    	likelihoodCore.calculatePartialsPruning(partials, probabilitiesBottleneck ,newPartials);
    	//System.out.println("probabilitiesBottleneck "+probabilitiesBottleneck.length);
        //for (int i3=0; i3<probabilitiesBottleneck.length;i3++) System.out.println(probabilitiesBottleneck[i3]); //////////////////////////////////////////
        //System.out.println("NewPartials after bottleneck "+newPartials.length);
        //for (int i3=0; i3<newPartials.length;i3++) System.out.println(newPartials[i3]); //////////////////////////////////////////
    	//System.out.println("Got partials");
    	return newPartials;
    }
    
    
  //travel through samples from an internal node to get to the final partial at its colonization time.
    double[] getPartialsInternal(NodePoMo node, List<Sample> samples, ArrayList<double[]> partials, int nodeIndex, int patternCount){
    	double[] partial1=new double[likelihoodCore.partialsSize];
    	//double[] matrix = new double[likelihoodCore.matrixSize * likelihoodCore.nrOfPatterns];
    	double[] matrix = new double[likelihoodCore.matrixSize];
    	double[] newPartials=new double[likelihoodCore.partialsSize];
    	double[] ps;
    	int is=0,ic=0;
    	int intervals=partials.size()+samples.size();
        if (samples.size()==0 || (node.getChild(partials.size()-1).getHeight()<samples.get(samples.size()-1).getTime())){
        	ps=partials.get(partials.size()-1);
        	System.arraycopy( ps, 0, partial1, 0, ps.length );
        	ic+=1;
        } else if (node.getSamples().size()>0){
        	ps=getPartials(samples.get(samples.size()-1).getTaxon(), samples.size()-1, patternCount);
        	System.arraycopy( ps, 0, partial1, 0, ps.length );
        	is+=1;
        }
        for (;(ic+is)<intervals;){
        	likelihoodCore.getNodeMatrices(nodeIndex, ic+is-1, matrix);
        	if (ic<partials.size() && (is>=samples.size() || node.getChild(ic).getHeight()<samples.get(is).getTime())){
        		//partial1=getPartialsInternalUpdate(partial1,partials.get(partials.size()-(ic+1)), matrix, patternCount);
        		likelihoodCore.calculatePartialsFixedPruning(partial1, matrix, partials.get(partials.size()-(ic+1)), newPartials);
        		System.arraycopy( newPartials, 0, partial1, 0, newPartials.length );
        		ic+=1;
        	}else if (is<node.getSamples().size()){
        		likelihoodCore.calculatePartialsFixedPruning(partial1, matrix, getPartials(samples.get(samples.size()-(1+is)).getTaxon(), samples.size()-(1+is), patternCount), newPartials);
        		//partials=getPartialsUpdate(partials,taxa.get(taxa.size()-(2+i)), taxa.size()-(2+i), matrix, patternCount);
        		System.arraycopy( newPartials, 0, partial1, 0, newPartials.length );
        		//partial1=getPartialsUpdate(partial1,taxa.get(taxa.size()-(1+is)), taxa.size()-(1+is), matrix, patternCount);
        		is+=1;
        	}
        }
        likelihoodCore.getNodeMatrices(nodeIndex, intervals-1, matrix);
    	likelihoodCore.calculatePartialsPruning(partial1, matrix, newPartials);
    	System.arraycopy( newPartials, 0, partial1, 0, newPartials.length );
    	likelihoodCore.calculatePartialsPruning(partial1, probabilitiesBottleneck ,newPartials);
    	return partial1;
    }
    
    
    
    double[] getPartials(Taxon t, int taxonIndex, int patternCount){
    	
        AlignmentPoMo data = (AlignmentPoMo) dataInput.get();
        int nStates = substitutionModel.getStateCount();
        int vPop=((nStates-4)/6)+1;
        double[] partials = new double[patternCount * nStates];

        int k = 0;
        //int iTaxon = taxonIndex;
        int iTaxon = data.getTaxonIndex(t.getID());
        //int iTaxon = getTaxonNr(t, data);
        int[] nState, maxNucs, maxVals;
        for (int iPattern = 0; iPattern < patternCount; iPattern++) {
            nState = data.getPatternPoMo(iTaxon, iPattern);
            maxNucs = new int[]{-1, -1};
            maxVals = new int[]{0, 0};
            for (int iNuc = 0; iNuc < 4; iNuc++) {
            	if (nState[iNuc]>maxVals[0]){
            		maxNucs[1]=maxNucs[0];
            		maxVals[1]=maxVals[0];
            		maxNucs[0]=iNuc;
            		maxVals[0]=nState[iNuc];
            	}else if(nState[iNuc]>maxVals[1]){
            		maxNucs[1]=iNuc;
            		maxVals[1]=nState[iNuc];
            	}
            }
            if(maxNucs[0]==-1){
            	for (int iState = 0; iState < nStates; iState++) {
                    partials[k++] = 1.0;
                }
            }else {
            	//fixed states
            	double sErr=seqErr.getValue();
            	for (int iState = 0; iState < 4; iState++) {
            		if (iState==maxNucs[0]){
            			partials[k++] = sampProb(1.0-sErr,maxVals[0],sErr/3.0,maxVals[1]);
            		} else if (iState==maxNucs[1]){
            			partials[k++] = sampProb(sErr/3.0,maxVals[0],1.0-sErr,maxVals[1]);
            		} else{
            			partials[k++] = sampProb(sErr/3.0,maxVals[0]+maxVals[1],1.0-sErr,0);
            		}
            	}
            	//groups of polymorphisms
            	for (int iGroup = 0; iGroup < 6; iGroup++) {
            		int[] ind=PoMoGeneral.indeces(iGroup);
            		for (int iState = 0; iState < vPop-1; iState++) {
                		double[] probs=new double[2];
                		double freq;
                		if (maxNucs[0]==ind[0]) {
                			freq=(iState+1.0)/((double)(vPop));
                			//probs[0]=freq*(1.0-sErr) + (1.0-freq)*sErr/3.0;
                		}else if(maxNucs[0]==ind[1]) {
                			freq=(vPop - (iState+1.0))/((double)(vPop));
                			//probs[0]=(vPop - (iState+1.0))/((double)(vPop));
                		}else{
                			freq=0.0;
                			//probs[0]=sErr;
                		}
                		probs[0]=freq*(1.0-sErr) + (1.0-freq)*sErr/3.0;
                		if (maxNucs[1]==ind[0]) {
                			freq=(iState+1.0)/((double)(vPop));
                			//probs[1]=(iState+1.0)/((double)(vPop));
                		}else if(maxNucs[1]==ind[1]) {
                			freq=(vPop - (iState+1.0))/((double)(vPop));
                			//probs[1]=(vPop - (iState+1.0))/((double)(vPop));
                		}else{
                			freq=0.0;
                			//probs[1]=sErr;
                		}
                		probs[1]=freq*(1.0-sErr) + (1.0-freq)*sErr/3.0;
                		partials[k++] = sampProb(probs[0],maxVals[0],probs[1],maxVals[1]);
            		}
            	}
            }
//            if (iPattern==0){
//            	String s="Pattern: "+maxNucs[0]+"-"+maxVals[0]+", "+maxNucs[1]+"-"+maxVals[1]+" \n    New Partials: ";
//                for (int i = 0; i < nStates; i++) {
//                	s+=", "+partials[i];
//                }
//                System.out.println(s);
//            }
        }
    	return partials;
    }
    
    
    //the partial likelihood of sampling - Nicola
    double sampProb(double p1, int v1, double p2, int v2){
    	double res=1.0,bigP,smallP;
    	int big,small;
    	if (v1>v2) {
    		big=v1;
    		bigP=p1;
    		small=v2;
    		smallP=p2;
    	}else{
    		big=v2;
    		bigP=p2;
    		small=v1;
    		smallP=p1;
    	}
    			
    	for (int i = 1; i <= small; i++) {
    		res=res*(i+big)*smallP*bigP/i;
    	}
    	for (int i = 1; i <= big-small; i++) {
    		res=res*bigP;
    	}
    	return res;
    }
    
    
    

    /**
     * Calculate the log likelihood of the current state.
     *
     * @return the log likelihood.
     */
    double m_fScale = 1.01;
    int m_nScale = 0;
    int X = 100;
    //boolean partialsDirty;

    @Override
    public double calculateLogP() throws IllegalArgumentException {
    	
    	//System.out.println("Start calculateLogP() ");
    	//if(seqErr.somethingIsDirty()){
        //	partialsDirty = true;
        	//System.out.println("PartialsDirty is true");
        //}
    	//treePoMo = treePoMoInput.get();
    	final TreePoMo tree = treePoMoInput.get();
    	//sErr = seqErr.getValue();
//    	if(sErr!=sErrStored){
//    		partialsDirty=true;
//    	}
    	
    	//if(partialsDirty){
    	//	sErr = seqErr.getValue();
    	//}
    	//System.out.println("Get bottleneck probabilities ");
    	//TODO: bottleneck probabilities like in the slow version 
    	//substitutionModel.getTransitionProbabilitiesBottleneck(probabilitiesBottleneck,bottle.getValue());
    	
    	//NICOLA TODO beagle
//        if (beagle != null) {
//            try {
//				logP = beagle.calculateLogPPartials(partialsDirty, sErr);
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//            partialsDirty = false;
//            //System.out.println("Calculating likelihood, sErrStored="+sErrStored+"   SErr="+sErr+"  logP="+logP);
//            return logP;
//        }
        
        //final TreePoMo tree = treePoMo;
        
        //System.out.println("Set matrices ");
        setCurrentNodeMatrices(tree.getRoot());
        //System.out.println("Set partials ");
        setCurrentPartials(tree.getRoot(), dataInput.get().getPatternCount());
        double[] frequencies = substitutionModel.getFrequencies();
        double[] proportions = m_siteModel.getCategoryProportions();
        //System.out.println("Integrate partials ");
        likelihoodCore.integratePartials(tree.getRoot().getNr(), proportions, m_fRootPartials);
        if (constantPattern != null) {
            proportionInvariant = m_siteModel.getProportionInvariant();
            for (final int i : constantPattern) {
                m_fRootPartials[i] += proportionInvariant;
            }
        }
        //System.out.println("CalculateLogLikelihoods, root index "+likelihoodCore.getCurrentPartialsIndex(tree.getRoot().getNr()));
        likelihoodCore.calculateLogLikelihoods(m_fRootPartials, frequencies, patternLogLikelihoods);
        //System.out.println("Pattern LogLK root, length  "+patternLogLikelihoods.length);
        //for (int i3=0; i3<patternLogLikelihoods.length;i3++) System.out.println(patternLogLikelihoods[i3]); //////////////////////////////////////////
        //System.out.println("frequencies, length  "+frequencies.length);
        //for (int i3=0; i3<frequencies.length;i3++) System.out.println(frequencies[i3]); //////////////////////////////////////////
        //System.out.println("m_fRootPartials, length  "+m_fRootPartials.length);
        //for (int i3=0; i3<m_fRootPartials.length;i3++) System.out.println(m_fRootPartials[i3]); //////////////////////////////////////////
        //System.out.println("bottleneck size: "+bottle.getValue());
        //System.out.println("calcLogP() ");
        try {
			calcLogP();
		} catch (Exception e) {
			e.printStackTrace();
		}
        
        //System.out.println("End first part of calculateLogP() "+logP);
        
        //partialsDirty=false;
        
        m_nScale++;
        if (logP > 0 || (likelihoodCore.getUseScaling() && m_nScale > X)) {
//            System.err.println("Switch off scaling");
//            m_likelihoodCore.setUseScaling(1.0);
//            m_likelihoodCore.unstore();
//            m_nHasDirt = Tree.IS_FILTHY;
//            X *= 2;
//            traverse(tree.getRoot());
//            calcLogP();
//            return logP;
        } else if (logP == Double.NEGATIVE_INFINITY && m_fScale < 10 && !scaling.get().equals(Scaling.none)) { // && !m_likelihoodCore.getUseScaling()) {
            m_nScale = 0;
            m_fScale *= 1.01;
            System.err.println("Turning on scaling to prevent numeric instability " + m_fScale);
            likelihoodCore.setUseScaling(m_fScale);
            likelihoodCore.unstore();
            hasDirt = TreePoMo.IS_FILTHY;
            try {
            	setCurrentNodeMatrices(tree.getRoot());
                setCurrentPartials(tree.getRoot(), dataInput.get().getPatternCount());
                frequencies = substitutionModel.getFrequencies();
                proportions = m_siteModel.getCategoryProportions();
                likelihoodCore.integratePartials(tree.getRoot().getNr(), proportions, m_fRootPartials);
                if (constantPattern != null) {
                    proportionInvariant = m_siteModel.getProportionInvariant();
                    for (final int i : constantPattern) {
                        m_fRootPartials[i] += proportionInvariant;
                    }
                }
                //System.out.println("CalculateLogLikelihoods, root index "+likelihoodCore.getCurrentPartialsIndex(tree.getRoot().getNr()));
                likelihoodCore.calculateLogLikelihoods(m_fRootPartials, frequencies, patternLogLikelihoods);
				//substitutionModel.getTransitionProbabilitiesBottleneck(probabilitiesBottleneck);
				//System.out.println("m_fRootPartials, length  "+m_fRootPartials.length);
		        //for (int i3=0; i3<m_fRootPartials.length;i3++) System.out.println(m_fRootPartials[i3]); //////////////////////////////////////////
				calcLogP();
			} catch (Exception e) {
				e.printStackTrace();
			}
            //System.out.println("End calculateLogP() "+logP);
            return logP;
        }
        //System.out.println("End calculateLogP() "+logP);
        return logP;
    }

    void calcLogP() throws Exception {
        logP = 0.0;
        if (useAscertainedSitePatterns) {
            final double ascertainmentCorrection = dataInput.get().getAscertainmentCorrection(patternLogLikelihoods);
            for (int i = 0; i < dataInput.get().getPatternCount(); i++) {
                logP += (patternLogLikelihoods[i] - ascertainmentCorrection) * dataInput.get().getPatternWeight(i);
            }
        } else {
            for (int i = 0; i < dataInput.get().getPatternCount(); i++) {
                logP += patternLogLikelihoods[i] * dataInput.get().getPatternWeight(i);
            }
        }
        if (logP>0.0){
        	System.out.println("log-likelihood>0, probably matrix exponentiation problem, rejecting move\n");
        	logP=Double.NEGATIVE_INFINITY;
        }
    }
    
    void setNodeMatrices(final NodePoMo node){
    	//System.out.println("Starting set node matrices "+node.getHeight());
    	final int iNode = node.getNr();
        final double branchRate = 1.0;
        int intervals=node.getChildren().size()+node.getSamples().size();
        double[] branchTimes=new double[intervals];
        double currentTime=Double.MAX_VALUE;
        int is=0,ic=0;
        int nSamples=node.getSamples().size();
        int nChildren=node.getChildren().size();
        if (nChildren>0 &&(nSamples==0 || (node.getChild(nChildren-1).getHeight()<node.getSample(nSamples-1).getTime()))){
        	currentTime=node.getChild(nChildren-1).getHeight();
        	ic+=1;
        } else if (nSamples>0){
        	currentTime=node.getSample(nSamples-1).getTime();
        	is+=1;
        }
        for (;(ic+is)<intervals;){
        	if (ic<node.getChildren().size() && (is>=nSamples || node.getChild(nChildren-(ic+1)).getHeight()<node.getSample(nSamples-(1+is)).getTime())){
        		branchTimes[ic+is-1]=node.getChild(nChildren-(ic+1)).getHeight()-currentTime;
        		currentTime=node.getChild(nChildren-(ic+1)).getHeight();
        		ic+=1;
        	}else if (is<nSamples){
        		branchTimes[ic+is-1]=node.getSample(nSamples-(1+is)).getTime() -currentTime;
        		currentTime=node.getSample(nSamples-(1+is)).getTime();
        		is+=1;
        	}
        }
        if(intervals>0){
        	branchTimes[intervals-1]=node.getHeight()-currentTime;
        }
        for (int i=0;i<intervals;i++){
	            //likelihoodCore.setNodeMatrixForUpdate(iNode);
	            for (int i2 = 0; i2 < m_siteModel.getCategoryCount(); i2++) {
	                final double jointBranchRate = m_siteModel.getRateForCategory(i2, null) * branchRate;
	                substitutionModel.getTransitionProbabilities(branchTimes[i], jointBranchRate, probabilities);
	                likelihoodCore.setNodeMatrix(iNode, i2, i, intervals, probabilities);
	            }
        }
        //System.out.println("Ending set node matrices "+node.getHeight());
        for (NodePoMo c :node.getChildren()){
        	setNodeMatrices(c);
    	}
        //m_branchLengths[iNode]=branchTimes;
        storedBranchLengths[iNode]=branchTimes;
    }
    
    
    
    void setCurrentNodeMatrices(final NodePoMo node){
    	//System.out.println("Starting set current node matrices "+node.getHeight());
    	//int update = (node.isDirty() | hasDirt);
    	final int iNode = node.getNr();
        final double branchRate = 1.0;
        int intervals=node.getChildren().size()+node.getSamples().size();
        double[] branchTimes=new double[intervals];
        double currentTime=Double.MAX_VALUE;
        int is=0,ic=0;
        int nSamples=node.getSamples().size();
        int nChildren=node.getChildren().size();
        if (nChildren>0 &&(nSamples==0 || (node.getChild(nChildren-1).getHeight()<node.getSample(nSamples-1).getTime()))){
        	currentTime=node.getChild(nChildren-1).getHeight();
        	ic+=1;
        } else if (nSamples>0){
        	currentTime=node.getSample(nSamples-1).getTime();
        	is+=1;
        }
        for (;(ic+is)<intervals;){
        	if (ic<node.getChildren().size() && (is>=nSamples || node.getChild(nChildren-(ic+1)).getHeight()<node.getSample(nSamples-(1+is)).getTime())){
        		branchTimes[ic+is-1]=node.getChild(nChildren-(ic+1)).getHeight()-currentTime;
        		currentTime=node.getChild(nChildren-(ic+1)).getHeight();
        		ic+=1;
        	}else if (is<nSamples){
        		branchTimes[ic+is-1]=node.getSample(nSamples-(1+is)).getTime() -currentTime;
        		currentTime=node.getSample(nSamples-(1+is)).getTime();
        		is+=1;
        	}
        }
        if(intervals>0){
        	branchTimes[intervals-1]=node.getHeight()-currentTime;
        }
        for (int i=0;i<intervals;i++){
	        if ((node.isDirty() != TreePoMo.IS_CLEAN || branchTimes.length!=m_branchLengths[iNode].length || branchTimes[i] != m_branchLengths[iNode][i] || hasDirt!=TreePoMo.IS_CLEAN)) {
	            likelihoodCore.setNodeMatrixForUpdate(iNode);
	            for (int i2 = 0; i2 < m_siteModel.getCategoryCount(); i2++) {	
	                final double jointBranchRate = m_siteModel.getRateForCategory(i2, null) * branchRate;
	                substitutionModel.getTransitionProbabilities(branchTimes[i], jointBranchRate, probabilities);
	                //System.out.println("Substitution probabilities, category "+i2+" interval "+i+" node "+iNode+" length "+probabilities.length);
	                //for (int i3=0; i3<probabilities.length;i3++) System.out.println(probabilities[i3]); //////////////////////////////////////////
	                likelihoodCore.setNodeMatrix(iNode, i2, i, intervals, probabilities);
	            }
	            //update |= TreePoMo.IS_DIRTY;
	        }
        }
        //System.out.println("Ending set current node matrices "+node.getHeight());
        for (NodePoMo c :node.getChildren()){
        	setCurrentNodeMatrices(c);
        }
        m_branchLengths[iNode]=branchTimes;
    }
    
    
    
    
    

    /* Assumes there IS a branch rate model as opposed to traverse() */
//    int traverse(final NodePoMo node) throws Exception {
//        final int iNode = node.getNr();
//        final double branchRate = 1.0; // branchRateModel.getRateForBranch(node);
//
//            if (node.isRoot()) {
//            	System.out.println("traverse- root business");
//                final double[] frequencies = substitutionModel.getFrequencies();
//                final double[] proportions = m_siteModel.getCategoryProportions();
//                likelihoodCore.integratePartials(node.getNr(), proportions, m_fRootPartials);
//                if (constantPattern != null) {
//                    proportionInvariant = m_siteModel.getProportionInvariant();
//                    for (final int i : constantPattern) {
//                        m_fRootPartials[i] += proportionInvariant;
//                    }
//                }
//                likelihoodCore.calculateLogLikelihoods(m_fRootPartials, frequencies, patternLogLikelihoods);
//            }
//    } // traverseWithBRM
    
    

    /** CalculationNode methods **/

    /**
     * check state for changed variables and update temp results if necessary *
     */
    @Override
    protected boolean requiresRecalculation() {
    	//System.out.println("Start requiresRecalculation ");
    	
    	//NICOLA TODO : allow beagle
//        if (beagle != null) {
//            return (beagle.requiresRecalculationFromLike(partialsDirty,seqErr.getValue())||partialsDirty);
//        }
        hasDirt = TreePoMo.IS_CLEAN;
        
        if(seqErr.somethingIsDirty()||bottle.somethingIsDirty()){
        	//partialsDirty = true;
        	hasDirt = TreePoMo.IS_FILTHY;
        	return true;
        }

        if (dataInput.get().isDirtyCalculation()) {
            hasDirt = TreePoMo.IS_FILTHY;
            return true;
        }
        if (m_siteModel.isDirtyCalculation()) {
            hasDirt = TreePoMo.IS_DIRTY;
            return true;
        }
//        if (branchRateModel != null && branchRateModel.isDirtyCalculation()) {
//            //m_nHasDirt = Tree.IS_DIRTY;
//            return true;
//        }
        
//        if(partialsDirty){
//        	return true;
//        }
        //hasDirt = TreePoMo.IS_FILTHY;
        //return true;
        return treePoMoInput.get().somethingIsDirty();
    }

    @Override
    public void store() {
    	//System.out.println("Start store ");
    	//sErrStored=sErr;
        if (beagle != null) {
            beagle.store();
            super.store();
            return;
        }
        if (likelihoodCore != null) {
            likelihoodCore.store();
        }
        super.store();
        for (int i=0;i< m_branchLengths.length;i++){
        	System.arraycopy(m_branchLengths[i], 0, storedBranchLengths[i], 0, m_branchLengths[i].length);
        }
        probabilitiesBottleneckStored=probabilitiesBottleneck.clone();
        //System.out.println("End store ");
        //System.arraycopy(m_branchLengths, 0, storedBranchLengths, 0, m_branchLengths.length);
    }

    @Override
    public void restore() {
    	//System.out.println("Start restore ");
    	//sErr=sErrStored;
    	//partialsDirty=false;
        if (beagle != null) {
            beagle.restore();
            super.restore();
            return;
        }
        if (likelihoodCore != null) {
            likelihoodCore.restore();
        }
        super.restore();
        for (int i=0;i< m_branchLengths.length;i++){
        	double[] tmp = m_branchLengths[i];
            m_branchLengths[i] = storedBranchLengths[i];
            storedBranchLengths[i] = tmp;
        }
        double[] tmp = probabilitiesBottleneck;
        probabilitiesBottleneck = probabilitiesBottleneckStored;
        probabilitiesBottleneckStored = tmp;
        //probabilitiesBottleneck=probabilitiesBottleneckStored.clone();
        //double[] tmp = m_branchLengths;
        //m_branchLengths = storedBranchLengths;
        //storedBranchLengths = tmp;
        //System.out.println("End restore ");
    }

    /**
     * @return a list of unique ids for the state nodes that form the argument
     */
    public List<String> getArguments() {
        return Collections.singletonList(dataInput.get().getID());
    }

    /**
     * @return a list of unique ids for the state nodes that make up the conditions
     */
    public List<String> getConditions() {
        return m_siteModel.getConditions();
    }

} // class TreeLikelihood
