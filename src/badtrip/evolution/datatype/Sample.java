package badtrip.evolution.datatype;

//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;


//import beast.base.core.BEASTObject;
//import beast.base.core.Description;
import beast.base.evolution.alignment.Taxon;


//NICOLA a combo of sampling time and sampled Taxon.
public class Sample {
	public Taxon taxon;
	public double time;

    public Sample(Taxon t, double tt) {
    	this.taxon=t;
    	this.time=tt;
    }
    
    public Taxon getTaxon(){
    	return this.taxon;
    }
    
    public double getTime(){
    	return this.time;
    }
    


}
