![Scheme](figures/BADTRIP.jpg)

BADTRIP
=============

BADTRIP is a BEAST2 package to infer who infected whom using within- and between-sample genetic variation data and epidemiological data.

The manuscript describing BADTRIP is https://doi.org/10.1371/journal.pcbi.1006117

You can find the documentation describing how to use the software in the /doc/ folder.

Also, you can find some python scripts to help you create an input xml file and to make a graphical output in the /scripts/ folder.

An example xml file is in the /examples/ folder.


Creator: Nicola De Maio


License
-------

This software is free (as in freedom).  With the exception of the
libraries on which it depends, it is made available under the terms of
the GNU General Public Licence version 3, which is contained in this
directory in the file named COPYING.
